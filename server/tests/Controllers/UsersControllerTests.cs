using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Unpub.Server.Controllers;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Tests.Controllers
{
  [TestFixture]
  public class UsersControllerTests
  {
    private MongoDbClient _client = default!;
    private Mock<IFileService> _mockFileService = default!;

    [SetUp]
    public async Task SetUp()
    {
      MongoConnectionInfo info = new()
      {
        Host = $"localhost:{TestOverhead.Instance.MongoProcess.Port}",
        Database = "unpub_test"
      };
      _client = await MongoDbClient.Create(info);
      _mockFileService = new Mock<IFileService>();
    }

    [TearDown]
    public void TearDown()
    {
      var mongoProcess = TestOverhead.Instance.MongoProcess;
      mongoProcess.ClearTable("unpub_test", "UnpubUsers");
      mongoProcess.ClearTable("unpub_test", "Games");
    }

    [Test]
    public async Task MeReturnsNotAuthorizedForAnonymousUser()
    {
      var controller = new UsersController(_client, _mockFileService.Object, TestUtils.DefaultUploadSettings);
      TestUtils.PrepareController(controller);

      var result = await controller.FetchMe();

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(ForbidResult), result.Result);
    }

    [Test]
    public async Task MeReturnsUserDetailForAuthedUser()
    {
      var user = await TestUtils.CreateTestUser(_client);

      var controller = new UsersController(_client, _mockFileService.Object, TestUtils.DefaultUploadSettings);
      TestUtils.PrepareController(controller, user);

      var result = await controller.FetchMe();
      Assert.AreEqual(user.Id.ToString(), result.Value.Id);
      Assert.AreEqual(user.Email, result.Value.Email);
      Assert.AreEqual(user.DisplayName, result.Value.DisplayName);
      Assert.AreEqual(user.AvatarPath, result.Value.AvatarUrl);
    }

    [Test]
    public async Task MeReturnsGamesByUser()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var games = new List<Game>();
      for (int i = 0; i < 5; ++i)
      {
        games.Add(await TestUtils.CreateTestGame(_client, user, $"Test Game {i}"));
      }

      var controller = new UsersController(_client, _mockFileService.Object, TestUtils.DefaultUploadSettings);
      TestUtils.PrepareController(controller, user);

      var result = await controller.FetchMe();
      Assert.AreEqual(5, result.Value.Games.Count);
      foreach (var game in result.Value.Games)
      {
        Assert.IsTrue(games.Any(g => TestUtils.GameSummaryMatchesGame(g, user, game)));
      }
    }

    [Test]
    public async Task MeReturnsRecentFeedbackByUser()
    {
      var designerUser = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, designerUser, "My Fake Game");

      var user = await TestUtils.CreateTestUser(_client);
      var feedback = new List<Feedback>();
      for (int i = 0; i < 5; ++i)
      {
        feedback.Add(await TestUtils.CreateFakeFeedback(_client, game, user, DateTime.UtcNow));
      }

      var controller = new UsersController(_client, _mockFileService.Object, TestUtils.DefaultUploadSettings);
      TestUtils.PrepareController(controller, user);

      var result = await controller.FetchMe();
      Assert.AreEqual(5, result.Value.Feedback.Count);
      foreach (var feedbackItem in result.Value.Feedback)
      {
        Assert.IsTrue(feedback.Any(f =>
        {
          return TestUtils.FeedbackSummaryMatchesFeedback(f, game, feedbackItem);
        }));
      }
    }

    [Test]
    public async Task AnonymousPostAvatarReturnsForbidden()
    {
      var controller = new UsersController(_client, _mockFileService.Object, TestUtils.DefaultUploadSettings);
      TestUtils.PrepareController(controller);

      var createData = new CreateAvatarData()
      {
        Top = 0,
        Left = 0,
        Width = 200,
        Height = 200
      };
      var mockFile = new Mock<IFormFile>();

      var result = await controller.PostAvatar(createData, mockFile.Object);

      Assert.IsInstanceOf(typeof(ForbidResult), result);
    }

    [Test]
    public async Task LargeFilesReturnError()
    {
      var user = await TestUtils.CreateTestUser(_client);

      var controller = new UsersController(_client, _mockFileService.Object, TestUtils.DefaultUploadSettings);
      TestUtils.PrepareController(controller, user);

      var createData = new CreateAvatarData()
      {
        Top = 0,
        Left = 0,
        Width = 200,
        Height = 200
      };
      var mockFile = new Mock<IFormFile>();
      mockFile.SetupGet(x => x.Length).Returns(100 * 1024 * 1024);

      var result = await controller.PostAvatar(createData, mockFile.Object);

      Assert.IsInstanceOf(typeof(JsonResult), result);
      var jsonResult = result as JsonResult;
      Assert.AreEqual(400, jsonResult?.StatusCode);
      Assert.AreEqual(1, (jsonResult?.Value as Dictionary<string, string[]>)?["errors"].Length);
    }

    [Test]
    public async Task SavesAvatarImage()
    {
      var user = await TestUtils.CreateTestUser(_client);

      var controller = new UsersController(_client, _mockFileService.Object, TestUtils.DefaultUploadSettings);
      TestUtils.PrepareController(controller, user);

      var createData = new CreateAvatarData()
      {
        Top = 0,
        Left = 0,
        Width = 200,
        Height = 200
      };
      var mockFile = new Mock<IFormFile>();
      var mockStream = new Mock<Stream>();
      mockFile.SetupGet(x => x.Length).Returns(5 * 1024);
      mockFile.Setup(x => x.OpenReadStream()).Returns(mockStream.Object);

      var srcRect = new Rectangle(0, 0, 200, 200);
      _mockFileService.Setup(x => x.ReplaceAvatar(It.IsNotNull<UnpubUser>(), mockStream.Object, srcRect)).Returns("url");
      _mockFileService.Setup(x => x.UrlForUserContent(It.IsNotNull<string>())).Returns("url");

      var result = await controller.PostAvatar(createData, mockFile.Object);

      _mockFileService.VerifyAll();
      Assert.IsInstanceOf(typeof(CreatedResult), result);
      Assert.AreEqual("url", (result as CreatedResult)?.Location);
    }
  }
}