using System;
using System.Threading.Tasks;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Unpub.Server.Controllers;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Tests.Controllers
{
  [TestFixture]
  public class FeedbackControllerTests
  {
    private MongoDbClient _client = default!;

    // Test vars
    private UnpubUser _designerUser = default!;
    private UnpubUser _feedbackUser = default!;
    private Game _game = default!;
    private DateTime _feedbackCreatedTime;

    private Mock<IFileService> _mockFileService = default!;

    [SetUp]
    public async Task SetUp()
    {
      MongoConnectionInfo info = new()
      {
        Host = $"localhost:{TestOverhead.Instance.MongoProcess.Port}",
        Database = "unpub_test"
      };
      _client = await MongoDbClient.Create(info);

      _designerUser = await TestUtils.CreateTestUser(_client);
      _feedbackUser = await TestUtils.CreateTestUser(_client);
      _game = await TestUtils.CreateTestGame(_client, _designerUser, "Test Game 0");
      _feedbackCreatedTime = DateTime.UtcNow;

      _mockFileService = new Mock<IFileService>();
      _mockFileService.Setup(x => x.UrlForUserContent(It.IsAny<string>())).Returns("url");
    }

    [TearDown]
    public void TearDown()
    {
      var mongoProcess = TestOverhead.Instance.MongoProcess;
      mongoProcess.ClearTable("unpub_test", "UnpubUsers");
      mongoProcess.ClearTable("unpub_test", "Games");
      mongoProcess.ClearTable("unpub_test", "Feedbacks");
      mongoProcess.ClearTable("unpub_test", "Feedbacks_Trash");
    }

    [Test]
    public async Task RequestForNonExistantFeedbackReturnsNotFound()
    {
      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _designerUser);

      var result = await controller.FetchFeedback("12345abcde");
      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }

    [Test]
    public async Task AnonymousRequestForFeedbackForbidden()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, true);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller);

      var result = await controller.FetchFeedback(feedback.Id.ToString());
      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(ForbidResult), result.Result);
    }

    [Test]
    public async Task FeedbackRequestFromCreatorReturnsFeedback()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, true);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _feedbackUser);

      var result = await controller.FetchFeedback(feedback.Id.ToString());
      Assert.NotNull(result);
      result.Value.WithDeepEqual(feedback);
    }

    [Test]
    public async Task FeedbackRequestFromDesignerReturnsFeedbackWithAnonymousDataRemoved()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, true);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _designerUser);

      var result = await controller.FetchFeedback(feedback.Id.ToString());
      Assert.NotNull(result);
      result.Value.WithDeepEqual(feedback)
        .IgnoreSourceProperty(x => x.Id)
        .IgnoreDestinationProperty(x => x.Id.ToString())
        .IgnoreSourceProperty(x => x.FeedbackMeta);
      Assert.IsNull(result.Value.FeedbackMeta);
      Assert.AreEqual(feedback.Id.ToString(), result.Value.Id);
    }

    [Test]
    public async Task FeedbackRequestFromDesignerReturnsFeedbackWithNonAnonymousDataIntact()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _designerUser);

      var result = await controller.FetchFeedback(feedback.Id.ToString());
      Assert.NotNull(result);
      result.Value.WithDeepEqual(feedback);
    }

    [Test]
    public async Task FeedbackRequestReturnsGameSummary()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _designerUser);

      var result = await controller.FetchFeedback(feedback.Id.ToString());
      Assert.AreEqual(_game.Id.ToString(), result.Value.Game.Id);
      Assert.AreEqual(_game.Name, result.Value.Game.Name);
      Assert.AreEqual(_designerUser.Id.ToString(), result.Value.Game.Owner.Id);
      Assert.AreEqual(_designerUser.DisplayName, result.Value.Game.Owner.DisplayName);
      Assert.AreEqual("url", result.Value.Game.Owner.AvatarUrl);
    }

    [Test]
    public async Task FeedbackRequestFrom3rdPartyReturnsNotFound()
    {
      var thirdParty = await TestUtils.CreateTestUser(_client);
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, thirdParty);

      var result = await controller.FetchFeedback(feedback.Id.ToString());
      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }

    [Test]
    public async Task DeleteFeedbackReturnsForbiddenForAnonymousUser()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller);

      var result = await controller.DeleteFeedback(feedback.Id.ToString());
      Assert.IsInstanceOf(typeof(ForbidResult), result);
    }

    [Test]
    public async Task DeleteUnknownFeedbackReturnsNotFound()
    {
      var _ = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _designerUser);

      var result = await controller.DeleteFeedback("12345abcde");
      Assert.IsInstanceOf(typeof(NotFoundResult), result);
    }

    [Test]
    public async Task DeleteFromUserReturnsForbidden()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _feedbackUser);

      var result = await controller.DeleteFeedback(feedback.Id.ToString());
      Assert.IsInstanceOf(typeof(ForbidResult), result);
    }

    [Test]
    public async Task DeleteFromThirdPartyReturnsNotFound()
    {
      var thirdParty = await TestUtils.CreateTestUser(_client);
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, thirdParty);

      var result = await controller.DeleteFeedback(feedback.Id.ToString());
      Assert.IsInstanceOf(typeof(NotFoundResult), result);
    }

    [Test]
    public async Task FeedbackDeletionFromDesignerSendsToTrash()
    {
      var feedback = await TestUtils.CreateFakeFeedback(_client, _game, _feedbackUser, _feedbackCreatedTime, false);

      var controller = new FeedbackController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, _designerUser);

      var result = await controller.DeleteFeedback(feedback.Id.ToString());
      Assert.IsInstanceOf(typeof(NoContentResult), result);

      var trashFeedback = await _client.GetAllFromTrash<Feedback>();
      Assert.NotNull(trashFeedback);
      Assert.AreEqual(1, trashFeedback.Count);
      trashFeedback[0].WithDeepEqual(feedback)
        .IgnoreSourceProperty(x => x.Created)
        .Assert();
    }
  }
}