using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using NUnit.Framework;
using Unpub.Server.Controllers;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Tests.Controllers
{
  class MockFileService : IFileService
  {
    public string ReplaceAvatar(UnpubUser user, Stream rawAvatar, Rectangle srcRect)
    {
      return "";
    }

    public string ReplaceGameAvatar(Game user, Stream rawAvatar, Rectangle srcRect)
    {
      return "";
    }

    public void RemoveGameAvatar(Game game)
    {

    }

    public string? UrlForUserContent(string? filePath)
    {
      return filePath;
    }
  }

  class FakeTimeProviderService : ITimeProviderService
  {
    public DateTime FakeTime { get; set; }

    public FakeTimeProviderService()
    {
      FakeTime = DateTime.UtcNow;
    }

    public DateTime GetNow()
    {
      return FakeTime;
    }
  }


  [TestFixture]
  public class GamesControllerTests
  {
    private MongoDbClient _client = default!;

    [SetUp]
    public async Task SetUp()
    {
      MongoConnectionInfo info = new()
      {
        Host = $"localhost:{TestOverhead.Instance.MongoProcess.Port}",
        Database = "unpub_test"
      };
      _client = await MongoDbClient.Create(info);
    }

    [TearDown]
    public void TearDown()
    {
      var mongoProcess = TestOverhead.Instance.MongoProcess;
      mongoProcess.ClearTable("unpub_test", "UnpubUsers");
      mongoProcess.ClearTable("unpub_test", "Games");
      mongoProcess.ClearTable("unpub_test", "Feedback");
    }

    [Test]
    public async Task CanGetAllGames()
    {
      var user1 = await TestUtils.CreateTestUser(_client);
      var user2 = await TestUtils.CreateTestUser(_client);

      var games = new List<Game>();
      for (int i = 0; i < 5; ++i)
      {
        games.Add(await TestUtils.CreateTestGame(_client, user1, $"Test Game {i}"));
      }

      for (int i = 0; i < 5; ++i)
      {
        games.Add(await TestUtils.CreateTestGame(_client, user2, $"Test Game {i}"));
      }

      var service = new GamesController(_client, new FakeTimeProviderService(), new MockFileService());
      var fetchedGames = await service.FetchGames();
      Assert.AreEqual(10, fetchedGames.Value.Count);
      foreach (var game in games)
      {
        var user = game.OwnerId == user1.Id ? user1 : user2;
        Assert.IsTrue(fetchedGames.Value.Any(g => TestUtils.GameSummaryMatchesGame(game, user, g)));
      }
    }

    [Test]
    public async Task GetGamesDoesNotReturnDeletedGames()
    {
      var user1 = await TestUtils.CreateTestUser(_client);
      var user2 = await TestUtils.CreateTestUser(_client);

      var games = new List<Game>();
      for (int i = 0; i < 5; ++i)
      {
        games.Add(await TestUtils.CreateTestGame(_client, user1, $"Test Game {i}"));
      }

      for (int i = 0; i < 5; ++i)
      {
        games.Add(await TestUtils.CreateTestGame(_client, user2, $"Test Game {i}"));
      }

      await _client.TrashById<Game>(games[3].Id);
      await _client.TrashById<Game>(games[8].Id);

      var service = new GamesController(_client, new FakeTimeProviderService(), new MockFileService());
      var fetchedGames = await service.FetchGames();
      Assert.AreEqual(8, fetchedGames.Value.Count);
      foreach (var fetchedGame in fetchedGames.Value)
      {
        var user = fetchedGame.Owner.Id == user1.Id.ToString() ? user1 : user2;
        var matchingGame = games.FirstOrDefault(g => g.Id.ToString() == fetchedGame.Id);
        Assert.NotNull(matchingGame);
        Assert.IsTrue(TestUtils.GameSummaryMatchesGame(matchingGame!, user, fetchedGame));
      }
    }

    [Test]
    public async Task CanGetGamesByOwner()
    {
      var user1 = await TestUtils.CreateTestUser(_client);
      var user2 = await TestUtils.CreateTestUser(_client);

      var user1Games = new List<Game>();
      for (int i = 0; i < 5; ++i)
      {
        user1Games.Add(await TestUtils.CreateTestGame(_client, user1, $"Test Game {i}"));
      }

      for (int i = 0; i < 5; ++i)
      {
        await TestUtils.CreateTestGame(_client, user2, $"Test Game {i}");
      }

      var service = new GamesController(_client, new FakeTimeProviderService(), new MockFileService());
      var fetchedGames = await service.FetchGames(user1.Id.ToString());
      Assert.AreEqual(5, fetchedGames.Value.Count);
      foreach (var game in user1Games)
      {
        Assert.IsTrue(fetchedGames.Value.Any(g => TestUtils.GameSummaryMatchesGame(game, user1, g)));
      }
    }

    [Test]
    public async Task CanGetGameById()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var games = new List<Game>();
      for (int i = 0; i < 5; ++i)
      {
        games.Add(await TestUtils.CreateTestGame(_client, user, $"Test Game {i}"));
      }

      var service = new GamesController(_client, new FakeTimeProviderService(), new MockFileService());
      var fetchedGame = await service.FetchGame(games[2].Id.ToString());
      Assert.IsNotNull(fetchedGame.Value);
      games[2].WithDeepEqual(fetchedGame.Value)
        .IgnoreSourceProperty(x => x.AvatarPath)
        .IgnoreSourceProperty(x => x.OwnerId)
        .IgnoreDestinationProperty(x => x.Avatar)
        .IgnoreDestinationProperty(x => x.Owner)
        .Assert();
      Assert.AreEqual(user.Id.ToString(), fetchedGame.Value.Owner?.Id);
      Assert.AreEqual(user.AvatarPath, fetchedGame.Value.Owner?.AvatarUrl);
      Assert.AreEqual(user.DisplayName, fetchedGame.Value.Owner?.DisplayName);
    }

    [Test]
    public async Task GetInvalidGameReturnsNotFound()
    {
      var service = new GamesController(_client, new FakeTimeProviderService(), new MockFileService());
      var result = await service.FetchGame("1234abcde");

      Assert.IsNotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }

    [Test]
    public async Task SendAnonymousFeedbackToGame()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user, $"Test Game 0");

      var timeService = new FakeTimeProviderService();
      var service = new GamesController(_client, timeService, new MockFileService());
      var feedback = TestUtils.GenerateFakeFeedback();

      var result = await service.SendFeedback(game.Id.ToString(), feedback);
      Assert.NotNull(result);
      Assert.NotNull(result.Value.Id);

      var dbFeedback = await _client.GetById<Feedback>(ObjectId.Parse(result.Value.Id));
      Assert.NotNull(dbFeedback);
      Assert.IsNull(dbFeedback.FeedbackMeta?.FromRegisteredUserId);
      Assert.That(timeService.FakeTime, Is.EqualTo(dbFeedback.Created).Within(1).Seconds);
      // TODO: Check loging IP address and / or some other session cookie?
      feedback.WithDeepEqual(dbFeedback)
        .IgnoreUnmatchedProperties();
    }

    [Test]
    public async Task SendFeedbackFromLoggedInUser()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user, $"Test Game 0");

      var user2 = await TestUtils.CreateTestUser(_client);

      var controller = new GamesController(_client, new TimeProviderService(), new MockFileService());
      TestUtils.PrepareController(controller, user2);

      var feedback = TestUtils.GenerateFakeFeedback();
      var result = await controller.SendFeedback(game.Id.ToString(), feedback);

      Assert.NotNull(result);

      var dbFeedback = await _client.GetById<Feedback>(ObjectId.Parse(result.Value.Id));
      Assert.AreEqual(user2.Id, dbFeedback.FeedbackMeta?.FromRegisteredUserId);
    }

    [Test]
    public async Task SendFeedbackToNonexistantGameReturnsNotFound()
    {
      var service = new GamesController(_client, new FakeTimeProviderService(), new MockFileService());
      var feedback = TestUtils.GenerateFakeFeedback();

      var result = await service.SendFeedback("12345abcde", feedback);
      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }
  }
}