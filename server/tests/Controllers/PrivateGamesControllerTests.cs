using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Net;
using Unpub.Server.Controllers;
using Unpub.Server.Database;
using Unpub.Server.Models;
using DeepEqual.Syntax;
using System;
using Moq;
using Unpub.Server.Services;
using System.Drawing;
using System.IO;
using MongoDB.Bson;
using System.Collections.Generic;

namespace Unpub.Server.Tests.Controllers
{
  [TestFixture]
  public class PrivateGamesControllerTests
  {
    private MongoDbClient _client = default!;

    private Mock<IFileService> _mockFileService = default!;

    [SetUp]
    public async Task Setup()
    {
      MongoConnectionInfo info = new()
      {
        Host = $"localhost:{TestOverhead.Instance.MongoProcess.Port}",
        Database = "unpub_test"
      };
      _client = await MongoDbClient.Create(info);

      _mockFileService = new Mock<IFileService>(MockBehavior.Default);
      _mockFileService.Setup(x => x.UrlForUserContent(It.IsAny<string>()))
        .Returns((string r) => { return r != null ? $"/{r}" : null; });
    }

    [TearDown]
    public void TearDown()
    {
      var mongoProcess = TestOverhead.Instance.MongoProcess;
      mongoProcess.ClearTable("unpub_test", "UnpubUsers");
      mongoProcess.ClearTable("unpub_test", "Games");
      mongoProcess.ClearTable("unpub_test", "Games_Trash");
    }

    [Test]
    public async Task GetDetailsReturnsNotAuthorizedForAnonymousUser()
    {
      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller);

      var result = await controller.FetchGame("fake-id");

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(ForbidResult), result.Result);
    }

    [Test]
    public async Task CreateGameForSelf()
    {
      var ownerUser = await TestUtils.CreateTestUser(_client);

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, ownerUser);

      var createInfo = new GameCreateInfo(
        name: "Test Game Name",
        time: "20-30",
        minAge: 10,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: new List<string>(),
        description: "My Game Description"
      );
      var result = await controller.CreateGame(createInfo);

      Assert.NotNull(result);
      Assert.NotNull(result.Value.Id);
      Assert.AreEqual("Test Game Name", result.Value.Name);
      Assert.AreEqual("20-30", result.Value.Time);
      Assert.AreEqual(10, result.Value.MinAge);
      Assert.AreEqual(2, result.Value.MinPlayers);
      Assert.AreEqual(10, result.Value.MaxPlayers);
      Assert.AreEqual("My Game Description", result.Value.Description);

      // Also in database
      var dbGame = await _client.GetById<Game>(ObjectId.Parse(result.Value.Id));
      Assert.NotNull(dbGame);
      dbGame.WithDeepEqual(result.Value)
        .IgnoreSourceProperty(x => x.OwnerId)
        .IgnoreSourceProperty(x => x.AvatarPath)
        .IgnoreDestinationProperty(x => x.Avatar)
        .IgnoreDestinationProperty(x => x.Owner)
        .Assert();
    }

    [Test]
    public async Task CreateGameWithAvatar()
    {
      var ownerUser = await TestUtils.CreateTestUser(_client);
      var srcRect = new Rectangle(0, 0, 400, 400);

      Func<Stream, string, bool> verifyStream = (Stream s, string value) =>
      {
        var contents = new StreamReader(s).ReadToEnd();
        return contents == value;
      };
      _mockFileService.Setup(x => x.ReplaceGameAvatar(
          It.IsNotNull<Game>(),
          It.Is<Stream>(x => verifyStream(x, "FAKE_IMAGE_BASE64_STRING")),
          srcRect)
        ).Returns("game_avatar_url")
        .Verifiable();

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, ownerUser);

      var createInfo = new GameCreateInfo(
        name: "Test Game Name",
        avatar: "RkFLRV9JTUFHRV9CQVNFNjRfU1RSSU5H",
        avatarSrcRect: new Rectangle(0, 0, 400, 400),
        time: "20-30",
        minAge: 10,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: new List<string>(),
        description: "My Game Description"
      );
      var result = await controller.CreateGame(createInfo);

      Assert.NotNull(result);
      Assert.AreEqual("/game_avatar_url", result.Value.Avatar);

      // Also in database
      var dbGame = await _client.GetById<Game>(ObjectId.Parse(result.Value.Id));
      Assert.NotNull(dbGame);
      dbGame.WithDeepEqual(result.Value)
        .IgnoreSourceProperty(x => x.OwnerId)
        .IgnoreSourceProperty(x => x.AvatarPath)
        .IgnoreDestinationProperty(x => x.Avatar)
        .IgnoreDestinationProperty(x => x.Owner)
        .Assert();
    }

    [Test]
    public async Task AnonymousUserCannotCreateGames()
    {
      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller);

      var createInfo = new GameCreateInfo(
        name: "Test Game Name",
        time: "20-30",
        minAge: 10,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: new List<string>(),
        description: "My Game Description"
      );
      var result = await controller.CreateGame(createInfo);

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(ForbidResult), result.Result);
    }

    [Test]
    public async Task GetDetailsForMissingGameReturnsNotFound()
    {
      var user = await TestUtils.CreateTestUser(_client);

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user);

      var result = await controller.FetchGame("12345abcde");

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }

    [Test]
    public async Task GetDetailsForUnownedGameReturnsNotFound()
    {
      var user1 = await TestUtils.CreateTestUser(_client);
      var user2 = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user2, "Test Game");

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user1);

      var result = await controller.FetchGame(game.Id.ToString());

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }

    [Test]
    public async Task GetDetailsForGameReturnsPrivateDetails()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user, "Test Game");

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user);

      var result = await controller.FetchGame(game.Id.ToString());

      Assert.NotNull(result);
      Assert.AreEqual(game.Id.ToString(), result.Value.Id);
      Assert.AreEqual("Test Game", result.Value.Name);
    }

    [Test]
    public async Task GetDetailsForGameReturnsFeedbackSummary()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user, "Test Game");

      var feedbackCreatedTime = DateTime.UtcNow;
      var feedback = await TestUtils.CreateFakeFeedback(_client, game, null, feedbackCreatedTime);

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user);

      var result = await controller.FetchGame(game.Id.ToString());

      Assert.AreEqual(1, result.Value.Feedback.Count);
      var actualFeedback = result.Value.Feedback[0];
      Assert.IsTrue(TestUtils.FeedbackSummaryMatchesFeedback(feedback, game, actualFeedback));
    }

    [Test]
    public async Task DeleteGameReturnsForbiddenForAnonymousUser()
    {
      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller);

      var result = await controller.DeleteGame("fake-id");

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(ForbidResult), result);
    }

    [Test]
    public async Task DeleteGameForMissingGameReturnsNotFound()
    {
      var user = await TestUtils.CreateTestUser(_client);

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user);

      var result = await controller.DeleteGame("12345abcde");

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result);
    }

    [Test]
    public async Task DeletingAGameThatIsNotYoursReturnsNotFound()
    {
      var user1 = await TestUtils.CreateTestUser(_client);
      var user2 = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user2, "Test Game");

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user1);

      var result = await controller.DeleteGame(game.Id.ToString());

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result);
    }

    [Test]
    public async Task DeletingGamePutsInTrash()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user, "Test Game");

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user);

      var result = await controller.DeleteGame(game.Id.ToString());

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NoContentResult), result);

      var fetchedGames = await _client.GetAllFromTrash<Game>();
      Assert.NotNull(fetchedGames);
      Assert.AreEqual(1, fetchedGames.Count);
      Assert.AreEqual(game.Id, fetchedGames[0].Id);
    }

    [Test]
    public async Task UpdateReturnsForbiddenForAnonymous()
    {
      var user = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user, "Test Game");

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller);

      var createInfo = new GameCreateInfo(
        name: "Test Game Name",
        time: "20-30",
        minAge: 10,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: new List<string>(),
        description: "My Game Description"
      );
      var result = await controller.UpdateGame(game.Id.ToString(), createInfo);

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(ForbidResult), result.Result);
    }

    [Test]
    public async Task UpdateGameForMissingGameReturnsNotFound()
    {
      var user = await TestUtils.CreateTestUser(_client);

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user);

      var createInfo = new GameCreateInfo(
        name: "Test Game Name",
        time: "20-30",
        minAge: 10,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: new List<string>(),
        description: "My Game Description"
      );
      var result = await controller.UpdateGame("1234abcde", createInfo);

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }

    [Test]
    public async Task UpdateAGameThatIsNotYoursReturnsNotFound()
    {
      var user1 = await TestUtils.CreateTestUser(_client);
      var user2 = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, user2, "Test Game");

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, user1);

      var createInfo = new GameCreateInfo(
        name: "Modified Name",
        time: game.Time,
        minAge: 5,
        minPlayers: 1,
        maxPlayers: 100,
        mechanisms: new List<string>(),
        description: game.Description
      );
      var result = await controller.UpdateGame(game.Id.ToString(), createInfo);

      Assert.NotNull(result);
      Assert.IsInstanceOf(typeof(NotFoundResult), result.Result);
    }

    [Test]
    public async Task UpdateGameUpdatesGame()
    {
      var ownerUser = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, ownerUser, "Test Game");

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, ownerUser);

      var createInfo = new GameCreateInfo(
        name: "Modified Game Name",
        time: "100",
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 100,
        mechanisms: new List<string>(),
        description: "My New Game Description"
      );
      var result = await controller.UpdateGame(game.Id.ToString(), createInfo);

      Assert.NotNull(result);
      Assert.AreEqual(result.Value.Id, game.Id.ToString());
      Assert.AreEqual("Modified Game Name", result.Value.Name);
      Assert.AreEqual("100", result.Value.Time);
      Assert.AreEqual(8, result.Value.MinAge);
      Assert.AreEqual(2, result.Value.MinPlayers);
      Assert.AreEqual(100, result.Value.MaxPlayers);
      Assert.AreEqual("My New Game Description", result.Value.Description);

      // Also in database
      var dbGame = await _client.GetById<Game>(ObjectId.Parse(result.Value.Id));
      Assert.NotNull(dbGame);
      dbGame.WithDeepEqual(result.Value)
        .IgnoreSourceProperty(x => x.OwnerId)
        .IgnoreSourceProperty(x => x.AvatarPath)
        .IgnoreDestinationProperty(x => x.Avatar)
        .IgnoreDestinationProperty(x => x.Owner)
        .Assert();
    }

    [Test]
    public async Task UpdateGameWithAvatarAddsAvatar()
    {
      var ownerUser = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, ownerUser, "Test Game");
      var srcRect = new Rectangle(0, 0, 400, 400);

      Func<Stream, string, bool> verifyStream = (Stream s, string value) =>
      {
        var contents = new StreamReader(s).ReadToEnd();
        return contents == value;
      };
      _mockFileService.Setup(x => x.ReplaceGameAvatar(
          It.IsNotNull<Game>(),
          It.Is<Stream>(x => verifyStream(x, "FAKE_IMAGE_BASE64_STRING")),
          srcRect)
        ).Returns("game_avatar_url")
        .Verifiable();

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, ownerUser);

      var createInfo = new GameCreateInfo(
        name: "Modified Game Name",
        avatar: "RkFLRV9JTUFHRV9CQVNFNjRfU1RSSU5H",
        avatarSrcRect: new Rectangle(0, 0, 400, 400),
        time: "100",
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 100,
        mechanisms: new List<string>(),
        description: "My New Game Description"
      );
      var result = await controller.UpdateGame(game.Id.ToString(), createInfo);

      _mockFileService.Verify();
      Assert.NotNull(result);
      Assert.AreEqual("/game_avatar_url", result.Value.Avatar);
      var dbGame = await _client.GetById<Game>(game.Id);
      Assert.AreEqual("game_avatar_url", dbGame.AvatarPath);
    }

    [Test]
    public async Task UpdateGameWithNullAvatarRemovesAvatar()
    {
      var ownerUser = await TestUtils.CreateTestUser(_client);
      var game = await TestUtils.CreateTestGame(_client, ownerUser, "Test Game", "fake_image_path");

      _mockFileService.Setup(x => x.RemoveGameAvatar(
          It.IsNotNull<Game>()
        )).Verifiable();

      var controller = new PrivateGamesController(_client, _mockFileService.Object);
      TestUtils.PrepareController(controller, ownerUser);

      var createInfo = new GameCreateInfo(
        name: "Modified Game Name",
        avatar: "",
        time: "100",
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 100,
        mechanisms: new List<string>(),
        description: "My New Game Description"
      );
      var result = await controller.UpdateGame(game.Id.ToString(), createInfo);

      _mockFileService.Verify();
      Assert.NotNull(result);
      Assert.IsNull(result.Value.Avatar);
      var dbGame = await _client.GetById<Game>(game.Id);
      Assert.IsNull(dbGame.AvatarPath);
    }
  }
}