// TODO: Really should figure out hot to fix nullable stuff here
#nullable disable

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using NUnit.Framework;

namespace Unpub.Server.Tests
{
  public class MongoDbProcess : IDisposable
  {
    private static int s_count = 0;

    private static readonly int BasePort = 18847;
    public static readonly string Host = "localhost";

    private readonly int _instanceNumber;
    private string _mongoFolder;
    private Process _process;

    public int Port { get { return BasePort + _instanceNumber; } }

    private static string OsDir
    {
      get
      {
        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
        {
          return "macos";
        }
        else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
          return "win";
        }
        else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
        {
          return "ubuntu";
        }
        return "unknown";
      }
    }

    private static string ExecutableSuffix
    {
      get
      {
        return RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? ".exe" : "";
      }
    }

    public MongoDbProcess()
    {
      s_count++;
      _instanceNumber = s_count;
    }

    public void Start()
    {
      var assemblyFolder = Path.GetDirectoryName(new Uri(typeof(MongoDbProcess).Assembly.Location).LocalPath);
      _mongoFolder = Path.Combine(assemblyFolder, "mongodb");
      var dbFolder = Path.Combine(_mongoFolder, "unpub_tests");

      // re-create db folder if it exists
      if (Directory.Exists(dbFolder))
        Directory.Delete(dbFolder, true);
      Directory.CreateDirectory(dbFolder);

      _process = new Process();
      _process.StartInfo.FileName = Path.Combine(_mongoFolder, OsDir, "mongod" + ExecutableSuffix);
      _process.StartInfo.Arguments = $"--dbpath {dbFolder} --port {Port} --storageEngine ephemeralForTest";
      _process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
      _process.StartInfo.UseShellExecute = false;
      _process.StartInfo.RedirectStandardOutput = true;
      _process.OutputDataReceived += (sender, e) => { };
      _process.Start();
      _process.BeginOutputReadLine();
    }

    public string RunQuery(string query)
    {
      var output = new StringBuilder();

      var procQuery = new Process
      {
        StartInfo = new ProcessStartInfo
        {
          FileName = Path.Combine(_mongoFolder, OsDir, "mongo" + ExecutableSuffix),
          Arguments = $"--host {Host} --port {Port} --quiet --eval \"{query}\"",
          UseShellExecute = false,
          RedirectStandardOutput = true,
          CreateNoWindow = true
        },
      };

      procQuery.Start();

      // read query output
      while (!procQuery.StandardOutput.EndOfStream)
      {
        output.AppendLine(procQuery.StandardOutput.ReadLine());
      }

      // wait 2 seconds max before killing it
      if (!procQuery.WaitForExit(2000))
      {
        procQuery.Kill();
      }

      return output.ToString();
    }

    public void ClearTable(string dbName, string tableName)
    {
      RunQuery($"db=db.getSiblingDB('{dbName}');db.{tableName}.remove({{}})");
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
      TestContext.Out.WriteLine($"Disposing instance {_instanceNumber}");
      if (_process != null && !_process.HasExited)
      {
        RunQuery("db=db.getSiblingDB('admin');db.shutdownServer({timeoutSecs: 60});");
        _process.WaitForExit();
        _process.CancelOutputRead();
        _process = null;
      }
    }
  }
}