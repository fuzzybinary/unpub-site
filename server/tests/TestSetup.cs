using NUnit.Framework;
using Unpub.Server.Database;

namespace Unpub.Server.Tests
{
  [SetUpFixture]
  public class TestOverhead
  {
    private static TestOverhead? _instance;
    public static TestOverhead Instance
    {
      get
      {
        return _instance!;
      }
    }

    public MongoDbProcess MongoProcess { get; set; } = default!;

    [OneTimeSetUp]
    public void FixtureSetUp()
    {
      _instance = this;

      MongoDbClient.RegisterModelClasses();
      MongoProcess = new MongoDbProcess();
      MongoProcess.Start();
    }

    [OneTimeTearDown]
    public void FixtureTearDown()
    {
      MongoProcess.Dispose();
    }
  }
}