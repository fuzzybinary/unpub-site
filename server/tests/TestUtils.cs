using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Unpub.Server.Controllers;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Tests
{
  public static class TestUtils
  {
    public static void PrepareController(Controller controller, UnpubUser? loggedInUser = null)
    {
      var user = loggedInUser == null ? TestUtils.MockAnonymousUser() : TestUtils.MockLoginForUser(loggedInUser);
      controller.ControllerContext = new ControllerContext()
      {
        HttpContext = new DefaultHttpContext()
        {
          User = user
        }
      };
    }
    public static readonly UploadSettings DefaultUploadSettings = new()
    {
      MaxAvatarSizeBytes = 5 * 1024 * 1024,
      MaxPictureSizeBytes = 10 * 1024 * 1024
    };

    public static async Task<UnpubUser> CreateTestUser(IDbClient client)
    {
      // Create a user for testing
      var ownerUser = new UnpubUser(
        displayName: "Test User",
        email: "test@tester.com"
      );
      ownerUser.Id = await client.CreateItemAsync(ownerUser);
      return ownerUser;
    }

    public static async Task<Game> CreateTestGame(IDbClient client, UnpubUser owner, string name, string? avatarPath = null)
    {
      var game = new Game(
        ownerId: owner.Id,
        avatarPath: avatarPath,
        name: name,
        time: "30m",
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: new List<string>(),
        description: "My Game Description"
      );
      game.Id = await client.CreateItemAsync(game);
      return game;
    }

    public static async Task<Feedback> CreateFakeFeedback(IDbClient client, Game game, UnpubUser? user, DateTime createTime, bool anonymous = false)
    {
      var feedback = new Feedback()
      {
        Created = createTime,
        GameId = game.Id,
        Time = 30,
        Players = 5,
        FirstPlayerScore = 100,
        LastPlayerScore = 10,
        IsWinner = true,
        GameLength = 4,
        EaseOfLearning = 3,
        DownTime = 3,
        Decisions = 5,
        Interactivity = 4,
        Originality = 3,
        Fun = 4,
        Predictable = DiscreetAnswer.No,
        PredictableWhy = "It wasn't",
        PlayAgain = DiscreetAnswer.Yes,
        Buy = DiscreetAnswer.Maybe,
        ChangeOneThing = "",
        FavoritePart = "Everything",
        Comments = "No comments",
        IsAnonymous = anonymous,
        FeedbackMeta = new FeedbackMeta(
          fromRegisteredUserId: user?.Id,
          fromName: user == null ? null : "From Name",
          fromEmail: user == null ? null : "fake@email.com"
        )
      };

      feedback.Id = await client.CreateItemAsync(feedback);
      return feedback;
    }

    public static ClaimsPrincipal MockAnonymousUser()
    {
#nullable disable
      var mockIdentity = new Mock<IIdentity>();
      mockIdentity.Setup(m => m.IsAuthenticated).Returns(false);

      var mockUser = new Mock<ClaimsPrincipal>();
      mockUser.Setup(m => m.FindFirst(ClaimTypes.NameIdentifier))
        .Returns((Claim)null);
      mockUser.Setup(m => m.Identity).Returns(mockIdentity.Object);

      return mockUser.Object;
#nullable enable
    }

    public static ClaimsPrincipal MockLoginForUser(UnpubUser user)
    {
      var mockIdentity = new Mock<IIdentity>();
      mockIdentity.Setup(m => m.IsAuthenticated).Returns(true);

      var mockUser = new Mock<ClaimsPrincipal>();
      mockUser.Setup(m => m.FindFirst(ClaimTypes.NameIdentifier))
        .Returns(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
      mockUser.Setup(m => m.Identity).Returns(mockIdentity.Object);

      return mockUser.Object;
    }

    public static bool GameSummaryMatchesGame(Game game, UnpubUser owner, GameSummary gameSummary)
    {
      if (game == null || gameSummary == null)
        return false;

      return game.Id.ToString() == gameSummary.Id &&
        game.Name == gameSummary.Name &&
        (gameSummary.Owner == null || (
          owner.Id.ToString() == gameSummary.Owner.Id &&
          owner.DisplayName == gameSummary.Owner.DisplayName &&
          owner.AvatarPath == gameSummary.Owner.AvatarUrl
        ));
    }

    public static bool FeedbackSummaryMatchesFeedback(Feedback feedback, Game game, FeedbackItemSummary feedbackSummary)
    {
      return feedback.Id.ToString() == feedbackSummary.Id
        && game.Id.ToString() == feedbackSummary.GameId
        && game.Name == feedbackSummary.GameName
        && feedback.GameLength == feedbackSummary.GameLength
        && feedback.EaseOfLearning == feedbackSummary.EaseOfLearning
        && feedback.DownTime == feedbackSummary.DownTime
        && feedback.Decisions == feedbackSummary.Decisions
        && feedback.Interactivity == feedbackSummary.Interactivity
        && feedback.Originality == feedbackSummary.Originality
        && feedback.Fun == feedbackSummary.Fun;
    }

    public static FeedbackSubmission GenerateFakeFeedback()
    {
      return new FeedbackSubmission()
      {
        Time = 30,
        Players = 5,
        FirstPlayerScore = 100,
        LastPlayerScore = 10,
        IsWinner = true,
        GameLength = 4,
        EaseOfLearning = 3,
        DownTime = 3,
        Decisions = 5,
        Interactivity = 4,
        Originality = 3,
        Fun = 4,
        Predictable = DiscreetAnswer.No,
        PredictableWhy = "It wasn't",
        PlayAgain = DiscreetAnswer.Yes,
        Buy = DiscreetAnswer.Maybe,
        ChangeOneThing = "",
        FavoritePart = "Everything",
        Comments = "No comments",
        IsAnonymous = false,
      };
    }
  }
}