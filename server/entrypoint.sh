#! /bin/bash
set -x

file_env() {
	local var="$1"
	local fileVar="${var}__File"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}

pid=0

# SIGTERM-handler
term_handler() {
  if [ $pid -ne 0 ]; then
    kill -SIGTERM "$pid"
    wait "$pid"
  fi
  exit 143; # 128 + 15 -- SIGTERM
}

# setup handlers
# on callback, kill the last background process, which is `tail -f /dev/null` and execute the specified handler
trap 'kill ${!};' SIGUSR1
trap 'kill ${!}; term_handler' SIGTERM

file_env "MongoConnectionInfo__Password"
file_env "JWTSettings__SecretKey"
file_env "MailSenderConfiguration__Password"

dotnet "Unpub.Server.dll" &
pid="$!"

tail -f /dev/null & wait ${!}