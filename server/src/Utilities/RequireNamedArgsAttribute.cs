using System;

namespace Unpub.Server.Utilities
{
  [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor)]
  public class RequireNamedArgsAttribute : Attribute { }

}