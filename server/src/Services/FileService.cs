using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using Unpub.Server.Models;

namespace Unpub.Server.Services
{
  public interface IFileService
  {
    string? UrlForUserContent(string? filePath);

    // Replace an avatar for the selected user, return the saved path
    string ReplaceAvatar(UnpubUser user, Stream rawAvatar, Rectangle srcRect);

    string ReplaceGameAvatar(Game game, Stream rawAvatr, Rectangle srcRect);

    void RemoveGameAvatar(Game game);
  }

  public class FileService : IFileService
  {
    private const int AvatarSize = 200;
    private const string RootContentDir = "/srv";
    private const string UserContentDir = "usercontent";
    private const string ProfilesDir = "profiles";
    private const string GamesDir = "games";

    public string? UrlForUserContent(string? filePath)
    {
      if (filePath == null)
        return null;
      return "/" + Path.Join(UserContentDir, filePath);
    }

    public string ReplaceGameAvatar(Game game, Stream rawAvatar, Rectangle srcRect)
    {
      var newPath = SaveResizedImage(rawAvatar, srcRect, GamesDir);

      var oldPath = Path.Join(RootContentDir, UserContentDir, game.AvatarPath);
      if (oldPath != null && File.Exists(oldPath))
      {
        File.Delete(oldPath);
      }

      return newPath;
    }

    public void RemoveGameAvatar(Game game)
    {
      var oldPath = Path.Join(RootContentDir, UserContentDir, game.AvatarPath);
      if (oldPath != null && File.Exists(oldPath))
      {
        File.Delete(oldPath);
      }
    }

    public string ReplaceAvatar(UnpubUser user, Stream rawAvatar, Rectangle srcRect)
    {
      var url = SaveResizedImage(rawAvatar, srcRect, ProfilesDir);

      var oldPath = Path.Join(RootContentDir, UserContentDir, user.AvatarPath);
      if (oldPath != null && File.Exists(oldPath))
      {
        File.Delete(oldPath);
      }

      return url;
    }

    private static string SaveResizedImage(Stream raw, Rectangle srcRect, string destPath)
    {
      string newPath;
      using (var image = Image.FromStream(raw))
      {
        if (srcRect.Left + srcRect.Width > image.Size.Width ||
          srcRect.Top + srcRect.Height > image.Size.Height)
        {
          throw new ArgumentException("Supplied creation data is too big for provided image.");
        }

        using var bitmap = new Bitmap(AvatarSize, AvatarSize);
        using var graphics = Graphics.FromImage(bitmap);

        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
        graphics.SmoothingMode = SmoothingMode.HighQuality;
        graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
        graphics.CompositingQuality = CompositingQuality.HighQuality;
        graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceOver;

        var destRect = new Rectangle(0, 0, AvatarSize, AvatarSize);
        graphics.DrawImage(image, destRect, srcRect, GraphicsUnit.Pixel);

        var guid = Guid.NewGuid();
        newPath = Path.Join(destPath, $"{guid}.jpg");

        using var fs = new FileStream(Path.Join(RootContentDir, UserContentDir, newPath), FileMode.Create, FileAccess.Write);
        bitmap.Save(fs, ImageFormat.Jpeg);
      }

      return newPath;
    }
  }
}