using System;

namespace Unpub.Server.Services
{
  public interface ITimeProviderService
  {
    DateTime GetNow();
  }

  public class TimeProviderService : ITimeProviderService
  {
    public DateTime GetNow()
    {
      return DateTime.UtcNow;
    }
  }
}