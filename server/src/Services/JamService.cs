using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Unpub.Server.Services
{
  public class JamService : BackgroundService
  {
    private readonly ILogger _logger;

    private bool _regenerating = false;
    private readonly SemaphoreSlim _semaphore = new(0, 1);

    public JamService(ILogger<JamService> logger)
    {
      _logger = logger;
    }

    public bool StartRegen()
    {
      if (_regenerating)
        return false;

      _regenerating = true;
      _semaphore.Release();

      return true;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
      _logger.Log(LogLevel.Information, "Started JamService Task");
      while (!stoppingToken.IsCancellationRequested)
      {
        await _semaphore.WaitAsync(stoppingToken);
        _logger.Log(LogLevel.Information, "Starting regen");
        if (_regenerating)
        {
          try
          {
            var process = new Process()
            {
              StartInfo = new ProcessStartInfo
              {
                FileName = "yarn",
                Arguments = "build",
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true,
                WorkingDirectory = "/srv/eleventy"
              }
            };
            process.Start();
            string _ = process.StandardOutput.ReadToEnd();
            _ = process.StandardError.ReadToEnd();
            process.WaitForExit();
          }
          finally
          {
            _regenerating = false;
          }
        }
      }
    }
  }
}