namespace Unpub.Server.Services
{
  public class UploadSettings
  {
    public int MaxAvatarSizeBytes { get; set; }

    public int MaxPictureSizeBytes { get; set; }
  }
}