using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace Unpub.Server.Services
{
  public class MailSenderConfiguration
  {
    public string From { get; set; } = default!;
    public string Server { get; set; } = default!;
    public int Port { get; set; }
    public string UserName { get; set; } = default!;
    public string Password { get; set; } = default!;
  }

  public class Message
  {
    public List<MailboxAddress> To { get; private set; }
    public string Subject { get; private set; }
    public string Content { get; private set; }

    public Message(IEnumerable<string> to, string subject, string content)
    {
      To = new List<MailboxAddress>();

      To.AddRange(to.Select(x => new MailboxAddress(x)));
      Subject = subject;
      Content = content;
    }
  }

  public interface IMailSenderService
  {
    Task SendMail(Message message);
  }

  public class MailSenderService : IMailSenderService
  {
    private readonly MailSenderConfiguration _configuration;

    public MailSenderService(MailSenderConfiguration configuration)
    {
      _configuration = configuration;
    }

    public async Task SendMail(Message message)
    {
      var mimeMessage = CreateEmailMessage(message);
      using var client = new SmtpClient();
      try
      {
        await client.ConnectAsync(_configuration.Server, _configuration.Port, SecureSocketOptions.SslOnConnect);
        client.Authenticate(_configuration.UserName, _configuration.Password);
        await client.SendAsync(mimeMessage);
      }
      catch (Exception e)
      {
        Console.WriteLine($"Failed to send mail {e}");
      }
      finally
      {
        client.Disconnect(true);
      }
    }

    private MimeMessage CreateEmailMessage(Message message)
    {
      var emailMessage = new MimeMessage();
      emailMessage.From.Add(new MailboxAddress(_configuration.From));
      emailMessage.To.AddRange(message.To);
      emailMessage.Subject = message.Subject;
      emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Text) { Text = message.Content };

      return emailMessage;
    }
  }
}