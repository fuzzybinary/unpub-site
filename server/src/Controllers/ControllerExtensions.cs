using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Unpub.Server.Controllers
{
  public static class ControllerExtensions
  {
    public static JsonResult ModelStateError(this Controller controller)
    {
      var items = controller.ModelState.Values.SelectMany(x => x.Errors)
        .Select(x => x.ErrorMessage)
        .ToArray();
      return new JsonResult(new Dictionary<string, string[]> {
        { "errors", items }
      })
      { StatusCode = 400 };
    }

    public static ObjectId? AuthedUserId(this Controller controller)
    {
      var user = controller.HttpContext?.User;
      if (user?.Identity == null || !user.Identity.IsAuthenticated)
        return null;

      var userIdString = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
      if (userIdString == null)
        return null;

      if (!ObjectId.TryParse(userIdString, out var userId))
        return null;
      return userId;
    }

    public static bool IsAuthenticated(this Controller controller)
    {
      var user = controller.HttpContext.User;
      return !user.Identity?.IsAuthenticated ?? false;
    }
  }
}