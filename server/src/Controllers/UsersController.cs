using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BrunoZell.ModelBinding;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Controllers
{
  [Route("api/[controller]")]
  public class UsersController : Controller
  {
    private readonly IDbClient _dbClient;
    private readonly IFileService _fileService;

    private readonly UploadSettings _uploadSettings;

    public UsersController(IDbClient dbClient, IFileService fileService, UploadSettings uploadSettings)
    {
      _dbClient = dbClient;
      _fileService = fileService;
      _uploadSettings = uploadSettings;
    }

    [HttpGet("me")]
    public async Task<ActionResult<UnpubUserProfileDetail>> FetchMe()
    {
      if (this.IsAuthenticated())
        return Forbid();

      var userId = this.AuthedUserId();
      if (!userId.HasValue)
        return Forbid();

      var dbUser = await _dbClient.GetById<UnpubUser>(userId.Value);
      if (dbUser == null)
        return NotFound();

      var detail = new UnpubUserProfileDetail(
        id: dbUser.Id.ToString(),
        displayName: dbUser.DisplayName,
        email: dbUser.Email,
        avatarUrl: _fileService.UrlForUserContent(dbUser.AvatarPath),
        games: new List<GameSummary>(),
        feedback: new List<FeedbackItemSummary>()
      );
      var games = await _dbClient.GetAll<Game>();
      detail.Games = games.Where(g => g.OwnerId == userId)
        .Select(g => GameSummary.FromGame(g, dbUser, _fileService))
        .ToList();

      var feedback = (await _dbClient
        .GetWhere<Feedback>(fb => fb.FeedbackMeta != null && fb.FeedbackMeta.FromRegisteredUserId == userId, fb => fb.Created))
        .Select(fb =>
        {
          var game = games.Where(g => fb.GameId == g.Id).FirstOrDefault()!;
          if (game != null)
          {
            return FeedbackItemSummary.FromFeedback(fb, game);
          }
          return null;
        })
        .OfType<FeedbackItemSummary>()
        .ToList();
      detail.Feedback = feedback;

      return new ActionResult<UnpubUserProfileDetail>(detail);
    }

    [HttpPost("me/avatar")]
    public async Task<ActionResult> PostAvatar(
      [ModelBinder(typeof(JsonModelBinder))] CreateAvatarData createData,
      IFormFile postedFile)
    {
      if (this.IsAuthenticated())
        return Forbid();

      var userId = this.AuthedUserId();
      if (!userId.HasValue)
        return Forbid();

      if (!ModelState.IsValid)
      {
        return this.ModelStateError();
      }

      if (postedFile.Length > _uploadSettings.MaxAvatarSizeBytes)
      {
        return new JsonResult(new Dictionary<string, string[]>() {
          { "errors", new string[] { $"File size too large. Max size is {_uploadSettings.MaxAvatarSizeBytes} bytes" } }
        })
        {
          StatusCode = 400
        };
      }


      var dbUser = await _dbClient.GetById<UnpubUser>(userId.Value);

      try
      {
        using var stream = postedFile.OpenReadStream();
        var srcRect = new Rectangle(createData.Left, createData.Top, createData.Width, createData.Height);
        dbUser.AvatarPath = _fileService.ReplaceAvatar(dbUser, stream, srcRect);

        await _dbClient.Replace(dbUser);
      }
      catch (ArgumentException e)
      {
        return new JsonResult(new Dictionary<string, string[]>() {
          { "errors", new string[] { $"Error validating image: {e.Message}" } }
        })
        {
          StatusCode = 400
        };
      }
      catch (Exception)
      {
        return StatusCode(500);
      }

      var avatarUrl = _fileService.UrlForUserContent(dbUser.AvatarPath);
      return Created(avatarUrl, null);
    }
  }
}