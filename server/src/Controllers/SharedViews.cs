using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Unpub.Server.Models;
using Unpub.Server.Utilities;

namespace Unpub.Server.Controllers
{
  public class FeedbackItemSummary
  {
    public string Id { get; set; }
    public string GameId { get; set; }
    public string GameName { get; set; }
    public DateTime Created { get; set; }
    public int GameLength { get; set; }
    public int EaseOfLearning { get; set; }
    public int DownTime { get; set; }
    public int Decisions { get; set; }
    public int Interactivity { get; set; }
    public int Originality { get; set; }
    public int Fun { get; set; }
    public DiscreetAnswer PlayAgain { get; set; }
    public DiscreetAnswer Buy { get; set; }
    public string Comments { get; set; }

    public FeedbackItemSummary(
      string id,
      string gameId,
      string gameName,
      DateTime created,
      int gameLength,
      int easeOfLearning,
      int downTime,
      int decisions,
      int interactivity,
      int originality,
      int fun,
      DiscreetAnswer playAgain,
      DiscreetAnswer buy,
      string comments
    )
    {
      Id = id;
      GameId = gameId;
      GameName = gameName;
      Created = created;
      GameLength = gameLength;
      EaseOfLearning = easeOfLearning;
      DownTime = downTime;
      Decisions = decisions;
      Interactivity = interactivity;
      Originality = originality;
      Fun = fun;
      PlayAgain = playAgain;
      Buy = buy;
      Comments = comments;
    }

    public static FeedbackItemSummary FromFeedback(Feedback feedback, Game forGame)
    {
      return new FeedbackItemSummary(
        id: feedback.Id.ToString(),
        gameId: forGame.Id.ToString(),
        gameName: forGame.Name,
        created: feedback.Created,
        gameLength: feedback.GameLength,
        easeOfLearning: feedback.EaseOfLearning,
        downTime: feedback.DownTime,
        decisions: feedback.Decisions,
        interactivity: feedback.Interactivity,
        originality: feedback.Originality,
        fun: feedback.Fun,
        playAgain: feedback.PlayAgain,
        buy: feedback.Buy,
        comments: feedback.Comments
      );
    }
  }
}