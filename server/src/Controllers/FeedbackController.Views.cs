using System;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Controllers
{
  public class FeedbackDetailsView
  {
    public string Id { get; }
    public GameSummary Game { get; }
    public DateTime Created { get; }
    public int Time { get; }
    public int Players { get; }
    public int FirstPlayerScore { get; }
    public int LastPlayerScore { get; }
    public bool IsWinner { get; }
    public int GameLength { get; }
    public int EaseOfLearning { get; }
    public int DownTime { get; }
    public int Decisions { get; }
    public int Interactivity { get; }
    public int Originality { get; }
    public int Fun { get; }
    public DiscreetAnswer Predictable { get; }
    public string PredictableWhy { get; }
    public DiscreetAnswer PlayAgain { get; }
    public DiscreetAnswer Buy { get; }
    public string ChangeOneThing { get; }
    public string FavoritePart { get; }
    public string Comments { get; }
    public bool IsAnonymous { get; }
    public FeedbackMeta? FeedbackMeta { get; set; }

    public FeedbackDetailsView(
      string id,
      GameSummary game,
      DateTime created,
      int time,
      int players,
      int firstPlayerScore,
      int lastPlayerScore,
      bool isWinner,
      int gameLength,
      int easeOfLearning,
      int downTime,
      int decisions,
      int interactivity,
      int originality,
      int fun,
      DiscreetAnswer predictable,
      string predictableWhy,
      DiscreetAnswer playAgain,
      DiscreetAnswer buy,
      string changeOneThing,
      string favoritePart,
      string comments,
      bool isAnonymous,
      FeedbackMeta? feedbackMeta = null
    )
    {
      Id = id;
      Game = game;
      Created = created;
      Time = time;
      Players = players;
      FirstPlayerScore = firstPlayerScore;
      LastPlayerScore = lastPlayerScore;
      IsWinner = isWinner;
      GameLength = gameLength;
      EaseOfLearning = easeOfLearning;
      DownTime = downTime;
      Decisions = decisions;
      Interactivity = interactivity;
      Originality = originality;
      Fun = fun;
      Predictable = predictable;
      PredictableWhy = predictableWhy;
      PlayAgain = playAgain;
      Buy = buy;
      ChangeOneThing = changeOneThing;
      FavoritePart = favoritePart;
      Comments = comments;
      IsAnonymous = isAnonymous;
      FeedbackMeta = feedbackMeta;
    }

    public static FeedbackDetailsView FromFeedback(Feedback feedback, Game game, UnpubUser owner, IFileService fileService)
    {
      return new FeedbackDetailsView(
        id: feedback.Id.ToString(),
        game: GameSummary.FromGame(game, owner, fileService),
        created: feedback.Created,
        time: feedback.Time,
        players: feedback.Players,
        firstPlayerScore: feedback.FirstPlayerScore,
        lastPlayerScore: feedback.LastPlayerScore,
        isWinner: feedback.IsWinner,
        gameLength: feedback.GameLength,
        easeOfLearning: feedback.EaseOfLearning,
        downTime: feedback.DownTime,
        decisions: feedback.Decisions,
        interactivity: feedback.Interactivity,
        originality: feedback.Originality,
        fun: feedback.Fun,
        predictable: feedback.Predictable,
        predictableWhy: feedback.PredictableWhy,
        playAgain: feedback.PlayAgain,
        buy: feedback.Buy,
        changeOneThing: feedback.ChangeOneThing,
        favoritePart: feedback.FavoritePart,
        comments: feedback.Comments,
        isAnonymous: feedback.IsAnonymous,
        feedbackMeta: feedback.FeedbackMeta
      );
    }
  }
}