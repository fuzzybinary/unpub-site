using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;
using MongoDB.Bson;

namespace Unpub.Server.Controllers
{
  [Route("api/[controller]")]
  public class GamesController : Controller
  {
    private readonly IDbClient _dbClient;
    private readonly ITimeProviderService _timeProvider;
    private readonly IFileService _fileService;

    public GamesController(IDbClient dbClient, ITimeProviderService timeProvider, IFileService fileService)
    {
      _dbClient = dbClient;
      _timeProvider = timeProvider;
      _fileService = fileService;
    }

    [HttpGet]
    public async Task<ActionResult<List<GameSummary>>> FetchGames([FromQuery] string? ownerId = null)
    {
      IEnumerable<Game> games = await _dbClient.GetAll<Game>();
      var owners = await _dbClient.GetAll<UnpubUser>();

      if (ownerId != null)
      {
        if (ObjectId.TryParse(ownerId, out var ownerObjectId))
        {
          var dbOwner = owners.FirstOrDefault(o => o.Id == ownerObjectId);
          if (dbOwner == null)
            return new List<GameSummary>();
          games = games.Where(g => g.OwnerId == dbOwner.Id);
        }
      }

      var gameList = games.Select(g =>
      {
        var owner = owners.FirstOrDefault(o => o.Id == g.OwnerId)!;
        return GameSummary.FromGame(g, owner, _fileService);
      }).ToList();

      return gameList;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<GameDetailsPublic>> FetchGame([FromRoute] string id)
    {
      if (!ObjectId.TryParse(id, out var gameId))
        return NotFound();

      var game = await _dbClient.GetById<Game>(gameId);
      if (game == null)
        return NotFound();

      var owner = await _dbClient.GetById<UnpubUser>(game.OwnerId);

      return GameDetailsPublic.FromGame(game, owner, _fileService);
    }

    [HttpPost("{id}/feedback")]
    public async Task<ActionResult<FeedbackSubmissionResult>> SendFeedback([FromRoute] string id, [FromBody] FeedbackSubmission feedback)
    {
      if (!ModelState.IsValid)
      {
        return this.ModelStateError();
      }

      ObjectId? feedbackUserId = this.AuthedUserId();
      if (!ObjectId.TryParse(id, out var gameId))
        return NotFound();

      var game = await _dbClient.GetById<Game>(gameId);
      if (game == null)
        return NotFound();

      var feedbackHolder = new Feedback()
      {
        GameId = game.Id,
        Created = _timeProvider.GetNow(),
        Time = feedback.Time,
        Players = feedback.Players,
        FirstPlayerScore = feedback.FirstPlayerScore,
        LastPlayerScore = feedback.LastPlayerScore,
        IsWinner = feedback.IsWinner,
        GameLength = feedback.GameLength,
        EaseOfLearning = feedback.EaseOfLearning,
        DownTime = feedback.DownTime,
        Decisions = feedback.Decisions,
        Interactivity = feedback.Interactivity,
        Originality = feedback.Originality,
        Fun = feedback.Fun,
        Predictable = feedback.Predictable,
        PredictableWhy = feedback.PredictableWhy,
        PlayAgain = feedback.PlayAgain,
        Buy = feedback.Buy,
        ChangeOneThing = feedback.ChangeOneThing,
        FavoritePart = feedback.FavoritePart,
        Comments = feedback.Comments,
        IsAnonymous = feedback.IsAnonymous,
        FeedbackMeta = new FeedbackMeta(
          fromRegisteredUserId: feedbackUserId,
          fromName: feedback.FromName,
          fromEmail: feedback.FromEmail
        )
      };

      feedbackHolder.Id = await _dbClient.CreateItemAsync<Feedback>(feedbackHolder);
      return new FeedbackSubmissionResult(
        id: feedbackHolder.Id.ToString(),
        gameId: game.Id.ToString(),
        created: feedbackHolder.Created
      );
    }
  }
}