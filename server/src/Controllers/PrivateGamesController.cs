using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Controllers
{
  [Route("/api/me/games")]
  public class PrivateGamesController : Controller
  {
    private readonly IDbClient _dbClient;
    private readonly IFileService _fileService;

    public PrivateGamesController(IDbClient dbClient, IFileService fileService)
    {
      _dbClient = dbClient;
      _fileService = fileService;
    }

    [HttpPost]
    public async Task<ActionResult<GameDetailsPublic>> CreateGame([FromBody] GameCreateInfo info)
    {
      if (this.IsAuthenticated())
        return Forbid();

      if (!ModelState.IsValid)
      {
        return this.ModelStateError();
      }

      var userId = this.AuthedUserId();
      if (!userId.HasValue)
        return Forbid();

      var game = new Game(
        ownerId: userId.Value,
        name: info.Name,
        time: info.Time,
        minAge: info.MinAge,
        minPlayers: info.MinPlayers,
        maxPlayers: info.MaxPlayers,
        mechanisms: info.Mechanisms,
        description: info.Description
      );

      if (info.Avatar != null && info.AvatarSrcRect != null)
      {
        using var stream = new MemoryStream(Convert.FromBase64String(info.Avatar));
        game.AvatarPath = _fileService.ReplaceGameAvatar(game, stream, info.AvatarSrcRect.Value);
      }

      game.Id = await _dbClient.CreateItemAsync(game);

      var details = GameDetailsPublic.FromGame(game, null, _fileService);
      return new ActionResult<GameDetailsPublic>(details);
    }

    [HttpPost("{id}")]
    public async Task<ActionResult<GameDetailsPublic>> UpdateGame([FromRoute] string id, [FromBody] GameCreateInfo info)
    {
      if (this.IsAuthenticated())
        return Forbid();

      if (!ModelState.IsValid)
      {
        return this.ModelStateError();
      }

      if (!ObjectId.TryParse(id, out var gameId))
        return NotFound();

      var game = await _dbClient.GetById<Game>(gameId);
      if (game == null)
        return NotFound();

      var userId = this.AuthedUserId();
      if (game.OwnerId != userId)
        return NotFound();

      if (info.Avatar != null && info.Avatar == string.Empty)
      {
        _fileService.RemoveGameAvatar(game);
        game.AvatarPath = null;
      }
      else if (info.Avatar != null && info.AvatarSrcRect != null)
      {
        using var stream = new MemoryStream(Convert.FromBase64String(info.Avatar));
        game.AvatarPath = _fileService.ReplaceGameAvatar(game, stream, info.AvatarSrcRect.Value);
      }

      game.Name = info.Name;
      game.Time = info.Time;
      game.MinAge = info.MinAge;
      game.MinPlayers = info.MinPlayers;
      game.MaxPlayers = info.MaxPlayers;
      game.Description = info.Description;
      game.Mechanisms = info.Mechanisms;
      await _dbClient.Replace(game);

      var details = GameDetailsPublic.FromGame(game, null, _fileService);
      return new ActionResult<GameDetailsPublic>(details);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<PrivateGameDetails>> FetchGame([FromRoute] string id)
    {
      if (this.IsAuthenticated())
        return Forbid();

      if (!ObjectId.TryParse(id, out var gameId))
        return NotFound();

      var game = await _dbClient.GetById<Game>(gameId);
      if (game == null)
        return NotFound();

      var userId = this.AuthedUserId();
      if (game.OwnerId != userId)
        return NotFound();

      var feedbackItems = (await _dbClient.GetWhere<Feedback>(f => f.GameId == game.Id, f => f.Created))
        .Select(fb => FeedbackItemSummary.FromFeedback(fb, game));

      return new PrivateGameDetails(
        id: game.Id.ToString(),
        name: game.Name,
        avatar: _fileService.UrlForUserContent(game.AvatarPath),
        time: game.Time,
        minAge: game.MinAge,
        minPlayers: game.MinPlayers,
        maxPlayers: game.MaxPlayers,
        mechanisms: game.Mechanisms,
        description: game.Description,
        feedback: feedbackItems.ToList()
      );
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteGame([FromRoute] string id)
    {
      if (this.IsAuthenticated())
        return Forbid();

      if (!ObjectId.TryParse(id, out var gameId))
        return NotFound();

      var game = await _dbClient.GetById<Game>(gameId);
      if (game == null)
        return NotFound();

      var userId = this.AuthedUserId();
      if (game.OwnerId != userId)
        return NotFound();

      if (await _dbClient.TrashById<Game>(game.Id))
        return NoContent();

      return StatusCode(StatusCodes.Status500InternalServerError);
    }
  }
}