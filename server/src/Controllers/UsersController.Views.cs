using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Unpub.Server.Utilities;

namespace Unpub.Server.Controllers
{
  // Details about a specific user 
  public class UnpubUserProfileDetail
  {
    public string Id { get; set; }
    public string DisplayName { get; set; }
    public string Email { get; set; }
    public string? AvatarUrl { get; set; }
    public List<GameSummary> Games { get; set; }
    public List<FeedbackItemSummary> Feedback { get; set; }

    [RequireNamedArgs]
    public UnpubUserProfileDetail(string id, string displayName, string email, string? avatarUrl,
      List<GameSummary> games, List<FeedbackItemSummary> feedback)
    {
      Id = id;
      DisplayName = displayName;
      Email = email;
      AvatarUrl = avatarUrl;
      Games = games;
      Feedback = feedback;
    }
  }

  public class UnpubUserSummary
  {
    public string Id { get; }

    public string DisplayName { get; }

    public string? AvatarUrl { get; }

    [RequireNamedArgs]
    public UnpubUserSummary(string id, string displayName, string? avatarUrl)
    {
      Id = id;
      DisplayName = displayName;
      AvatarUrl = avatarUrl;
    }
  }

  public class CreateAvatarData
  {
    [Required]
    [JsonProperty("top")]
    [Range(0, int.MaxValue, ErrorMessage = "Top must be greater than 0")]
    public int Top { get; set; }

    [Required]
    [JsonProperty("left")]
    [Range(0, int.MaxValue, ErrorMessage = "Left must be greater than 0")]
    public int Left { get; set; }

    [Required]
    [JsonProperty("width")]
    [Range(0, int.MaxValue, ErrorMessage = "Width must be greater than 0")]
    public int Width { get; set; }

    [Required]
    [JsonProperty("height")]
    [Range(0, int.MaxValue, ErrorMessage = "Height must be greater than 0")]
    public int Height { get; set; }
  }
}