﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Unpub.Server.JsonConverters;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Controllers
{
  public class OwnerSummary
  {
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("displayName")]
    public string DisplayName { get; set; }

    [JsonProperty("avatarUrl")]
    public string? AvatarUrl { get; set; }

    public OwnerSummary(string id, string displayName, string? avatarUrl)
    {
      Id = id;
      DisplayName = displayName;
      AvatarUrl = avatarUrl;
    }

    public static OwnerSummary FromUser(UnpubUser user, IFileService fileService)
    {
      return new OwnerSummary(
        id: user.Id.ToString(),
        displayName: user.DisplayName,
        avatarUrl: fileService.UrlForUserContent(user.AvatarPath)
      );
    }
  }

  public class GameSummary
  {
    public string Id { get; set; }

    public string? Avatar { get; set; }

    public string Name { get; set; }

    public OwnerSummary Owner { get; set; }

    public GameSummary(string id, string? avatar, string name, OwnerSummary owner)
    {
      Id = id;
      Avatar = avatar;
      Name = name;
      Owner = owner;
    }

    public static GameSummary FromGame(Game game, UnpubUser owner, IFileService fileService)
    {
      return new GameSummary(
        id: game.Id.ToString(),
        avatar: fileService.UrlForUserContent(game.AvatarPath),
        name: game.Name,
        owner: OwnerSummary.FromUser(owner, fileService)
      );
    }
  }

  public class GameDetailsPublic
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public string? Avatar { get; set; }

    public string Time { get; set; }

    public int MinAge { get; set; }

    public int MinPlayers { get; set; }

    public int MaxPlayers { get; set; }

    public List<string> Mechanisms { get; set; }

    public string Description { get; set; }

    public OwnerSummary? Owner { get; set; }

    public GameDetailsPublic(string id, string name, string? avatar, string time,
      int minAge, int minPlayers, int maxPlayers, List<string> mechanisms,
      string description, OwnerSummary? owner)
    {
      Id = id;
      Name = name;
      Avatar = avatar;
      Time = time;
      MinAge = minAge;
      MinPlayers = minPlayers;
      MaxPlayers = maxPlayers;
      Mechanisms = mechanisms;
      Description = description;
      Owner = owner;
    }

    public static GameDetailsPublic FromGame(Game game, UnpubUser? owner, IFileService fileService)
    {
      return new GameDetailsPublic(
        id: game.Id.ToString(),
        name: game.Name,
        avatar: fileService.UrlForUserContent(game.AvatarPath),
        time: game.Time,
        minAge: game.MinAge,
        minPlayers: game.MinPlayers,
        maxPlayers: game.MaxPlayers,
        mechanisms: game.Mechanisms,
        description: game.Description,
        owner: owner != null ? OwnerSummary.FromUser(owner, fileService) : null
      );
    }
  }

  public class GameCreateInfo
  {
    [Required]
    public string Name { get; set; }

    /// If this property is null, perform no operation on the avatar
    /// If it is set to an empty string, remove the avatar
    /// Otherwise interpret as a Base64 encoded image
    public string? Avatar { get; set; }

    [JsonConverter(typeof(RectangleJsonConverter))]
    public Rectangle? AvatarSrcRect { get; set; }

    [Required]
    public string Time { get; set; }

    [Required]
    public int MinAge { get; set; }

    [Required]
    public int MinPlayers { get; set; }

    [Required]
    public int MaxPlayers { get; set; }

    [Required]
    public List<string> Mechanisms { get; set; }

    [Required]
    public string Description { get; set; }

    public GameCreateInfo(string name, string time,
      int minAge, int minPlayers, int maxPlayers, List<string> mechanisms, string description,
      string? avatar = null, Rectangle? avatarSrcRect = null)
    {
      Name = name;
      Avatar = avatar;
      AvatarSrcRect = avatarSrcRect;
      Time = time;
      MinAge = minAge;
      MinPlayers = minPlayers;
      MaxPlayers = maxPlayers;
      Mechanisms = mechanisms;
      Description = description;
    }
  }

  public class FeedbackSubmission
  {
    [Required]
    public int Time { get; set; }

    [Required]
    public int Players { get; set; }

    [Required]
    public int FirstPlayerScore { get; set; }

    [Required]
    public int LastPlayerScore { get; set; }

    [Required]
    public bool IsWinner { get; set; }

    [Required]
    [Range(1, 5)]
    public int GameLength { get; set; }

    [Required]
    [Range(1, 5)]
    public int EaseOfLearning { get; set; }

    [Required]
    [Range(1, 5)]
    public int DownTime { get; set; }

    [Required]
    [Range(1, 5)]
    public int Decisions { get; set; }

    [Required]
    [Range(1, 5)]
    public int Interactivity { get; set; }

    [Required]
    [Range(1, 5)]
    public int Originality { get; set; }

    [Required]
    [Range(1, 5)]
    public int Fun { get; set; }

    [Required]
    public DiscreetAnswer Predictable { get; set; }

    [Required(AllowEmptyStrings = true)]
    public string PredictableWhy { get; set; } = "";

    [Required]
    public DiscreetAnswer PlayAgain { get; set; }

    [Required]
    public DiscreetAnswer Buy { get; set; }

    [Required(AllowEmptyStrings = true)]
    public string ChangeOneThing { get; set; } = "";

    [Required(AllowEmptyStrings = true)]
    public string FavoritePart { get; set; } = "";

    [Required(AllowEmptyStrings = true)]
    public string Comments { get; set; } = "";

    public string? FromName { get; set; }

    public string? FromEmail { get; set; }

    [Required]
    public bool IsAnonymous { get; set; }

    [Required]
    public bool ShareEmailOptIn { get; set; }
  }

  public class FeedbackSubmissionResult
  {
    public string Id { get; set; }

    public string GameId { get; set; }

    public DateTime Created { get; set; }

    public FeedbackSubmissionResult(string id, string gameId, DateTime created)
    {
      Id = id;
      GameId = gameId;
      Created = created;
    }
  }
}