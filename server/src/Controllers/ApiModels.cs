using System.Collections.Generic;

namespace Unpub.Server.Controllers
{
  public class ApiResponse
  {
    public string Status { get; set; }
    public List<string> Errors { get; set; }

    public ApiResponse(string status, List<string> errors)
    {
      Status = status;
      Errors = errors;
    }
  }
}