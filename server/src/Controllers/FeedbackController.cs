using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Controllers
{
  [Route("api/[controller]")]
  public class FeedbackController : Controller
  {
    private readonly IDbClient _dbClient;
    private readonly IFileService _fileService;

    public FeedbackController(IDbClient dbClient, IFileService fileService)
    {
      _dbClient = dbClient;
      _fileService = fileService;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<FeedbackDetailsView>> FetchFeedback([FromRoute] string id)
    {
      var user = HttpContext.User;
      if (!user.Identity?.IsAuthenticated ?? false)
        return Forbid();

      if (!ObjectId.TryParse(id, out var feedbackId))
        return NotFound();

      var feedback = await _dbClient.GetById<Feedback>(feedbackId);
      if (feedback == null)
        return NotFound();

      var game = await _dbClient.GetById<Game>(feedback.GameId);
      var gameOwner = await _dbClient.GetById<UnpubUser>(game.OwnerId);
      var userId = this.AuthedUserId();

      if (userId != feedback.FeedbackMeta?.FromRegisteredUserId)
      {
        // This isn't the feedback giver, check to see if it's the designer
        if (game.OwnerId != userId)
          return NotFound();
      }

      var feedbackDetails = FeedbackDetailsView.FromFeedback(feedback, game, gameOwner, _fileService);

      if (feedback.IsAnonymous && feedback.FeedbackMeta?.FromRegisteredUserId != userId)
      {
        feedbackDetails.FeedbackMeta = null;
      }

      return feedbackDetails;
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteFeedback([FromRoute] string id)
    {
      var userId = this.AuthedUserId();
      if (userId == null)
        return Forbid();

      if (!ObjectId.TryParse(id, out var feedbackId))
        return NotFound();

      var feedback = await _dbClient.GetById<Feedback>(feedbackId);
      if (feedback == null)
        return NotFound();

      var game = await _dbClient.GetById<Game>(feedback.GameId);
      if (game.OwnerId == userId)
      {
        if (await _dbClient.TrashById<Feedback>(feedbackId))
          return NoContent();
        else
          return NotFound();
      }

      // Double check. This user knows the feedback exists, but only
      // the reciever of the feedback can delete it. This happens below
      // the owner check so that an owner can delete feedback they left for
      // themselves
      if (feedback.FeedbackMeta?.FromRegisteredUserId == userId)
        return Forbid();

      return NotFound();
    }
  }
}