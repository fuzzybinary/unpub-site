using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Unpub.Server.Services;

namespace Unpub.Server.Controllers
{
  public class ConfigView
  {
    public int MaxAvatarSizeBytes { get; set; }

    public int MaxPictureSizeBytes { get; set; }
  }

  [Route("api/[controller]")]
  public class ConfigController : Controller
  {
    private readonly UploadSettings _uploadSettings;

    public ConfigController(UploadSettings uploadSettings)
    {
      _uploadSettings = uploadSettings;
    }

    [HttpGet]
    public ActionResult<ConfigView> FetchConfig()
    {
      var configView = new ConfigView()
      {
        MaxAvatarSizeBytes = _uploadSettings.MaxAvatarSizeBytes,
        MaxPictureSizeBytes = _uploadSettings.MaxPictureSizeBytes
      };

      return configView;
    }
  }
}