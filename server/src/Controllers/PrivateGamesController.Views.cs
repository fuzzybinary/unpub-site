using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Unpub.Server.Models;

namespace Unpub.Server.Controllers
{
  public class PrivateGameDetails
  {
    public string Id { get; }
    public string Name { get; }
    public string? Avatar { get; }
    public string Time { get; }
    public int MinAge { get; }
    public int MinPlayers { get; }
    public int MaxPlayers { get; }
    public List<string> Mechanisms { get; }
    public string Description { get; }
    public List<FeedbackItemSummary> Feedback { get; }

    public PrivateGameDetails(string id, string name, string? avatar, string time,
      int minAge, int minPlayers, int maxPlayers, List<string> mechanisms, string description,
      List<FeedbackItemSummary> feedback)
    {
      Id = id;
      Name = name;
      Avatar = avatar;
      Time = time;
      MinAge = minAge;
      MinPlayers = minPlayers;
      MaxPlayers = maxPlayers;
      Mechanisms = mechanisms;
      Description = description;
      Feedback = feedback;
    }
  }
}