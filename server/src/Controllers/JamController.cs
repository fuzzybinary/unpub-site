using System.Diagnostics;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using Unpub.Server.Services;

namespace Unpub.Server.Controllers
{
  /// <summary>Referring to JAMStack</summary>
  [Route("api/[controller]")]
  public class JamController : Controller
  {
    private readonly JamService _regenService;

    public JamController(JamService regenService)
    {
      _regenService = regenService;
    }

    [HttpPost]
    public ActionResult RegenSite()
    {
      if (_regenService.StartRegen())
        return Ok();
      else
        return StatusCode(425); // Too early
    }
  }
}