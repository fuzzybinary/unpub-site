﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using Unpub.Server.Database;
using AspNetCore.Identity.Mongo;
using Unpub.Server.Models;
using Unpub.Server.Auth;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Threading.Tasks;
using Unpub.Server.Services;
using Microsoft.AspNetCore.Identity;
using System;
using Newtonsoft.Json.Serialization;
using Unpub.Server.Controllers;
using Microsoft.Extensions.Hosting;
using Unpub.Server.JsonConverters;

namespace Unpub.Server
{
  public class Startup
  {
    private readonly IWebHostEnvironment _env;
    private readonly IConfiguration _configuration;
    private readonly ILoggerFactory _loggerFactory;

    public Startup(IWebHostEnvironment env, IConfiguration config, ILoggerFactory loggerFactory)
    {
      _env = env;
      _configuration = config;
      _loggerFactory = loggerFactory;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      JsonConvert.DefaultSettings = () =>
      {
        return new JsonSerializerSettings()
        {
          NullValueHandling = NullValueHandling.Ignore,
          ContractResolver = new CamelCasePropertyNamesContractResolver(),
          Converters = { new RectangleJsonConverter() }
        };
      };

      services.AddSingleton<ITimeProviderService>(new TimeProviderService());
      services.AddSingleton<IFileService>(new FileService());
      services.AddSingleton<JamService>();
      services.AddHostedService<JamService>(p => p.GetService<JamService>()!);

      var mailConfiguration = GetConfiguration<MailSenderConfiguration>();
      services.AddSingleton<IMailSenderService>(new MailSenderService(mailConfiguration));

      var uploadConfiguration = GetConfiguration<UploadSettings>();
      services.AddSingleton<UploadSettings>(uploadConfiguration);

      var mongoConfig = SetupDb(services);
      SetupAuthentication(services, mongoConfig);

      services.AddAntiforgery(o =>
      {
        o.Cookie.Name = "X-CSRF-TOKEN";
      });

      services.AddMvc(options =>
      {
        options.EnableEndpointRouting = false;
      }).AddNewtonsoftJson();
    }

    private T GetConfiguration<T>()
    {
      var type = typeof(T);
      var section = _configuration.GetSection(type.Name);
      var settings = section.Get<T>();
      return settings;
    }

    private void SetupAuthentication(IServiceCollection services, MongoConnectionInfo mongoConfig)
    {
      var jwtSettings = GetConfiguration<JWTSettings>();
      var jwtConfigService = new JWTService(jwtSettings);
      services.AddSingleton<JWTService>(jwtConfigService);

      var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.SecretKey));
      var tokenValidationParameters = new TokenValidationParameters
      {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = signingKey,

        // Validate the JWT Issuer (iss) claim
        ValidateIssuer = true,
        ValidIssuer = jwtSettings.Issuer,

        // Validate the JWT Audience (aud) claim
        ValidateAudience = true,
        ValidAudience = jwtSettings.Audience
      };

      services.AddIdentityMongoDbProvider<UnpubUser, UnpubRole>(identityOptions =>
      {
        identityOptions.Password.RequiredLength = 6;
        identityOptions.Password.RequireLowercase = false;
        identityOptions.Password.RequireUppercase = false;
        identityOptions.Password.RequireNonAlphanumeric = false;
        identityOptions.Password.RequireDigit = false;
      }, mongoIdentityOptions =>
      {
        mongoIdentityOptions.UsersCollection = "UnpubUsers";
        mongoIdentityOptions.RolesCollection = "UnpubRoles";
        mongoIdentityOptions.ConnectionString = mongoConfig.FullConnectionString;
      }).AddDefaultTokenProviders();

      services.Configure<DataProtectionTokenProviderOptions>(opt =>
      {
        opt.TokenLifespan = TimeSpan.FromHours(2);
      });

      services.AddAuthentication(options =>
      {
        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
      }).AddJwtBearer(options =>
      {
        options.TokenValidationParameters = tokenValidationParameters;
        options.SaveToken = true;
      }).AddCookie(options =>
      {
        options.Events.OnRedirectToLogin = context =>
        {
          context.Response.StatusCode = 401;
          return Task.CompletedTask;
        };
      });
    }

    private MongoConnectionInfo SetupDb(IServiceCollection services)
    {
      MongoDbClient.RegisterModelClasses();
      var mongoSection = _configuration.GetSection(nameof(MongoConnectionInfo));
      var mongoConfig = mongoSection.Get<MongoConnectionInfo>();
      services.Configure<MongoConnectionInfo>(mongoSection);
      IDbClient dbClient = MongoDbClient.Create(mongoConfig).Result;

      services.AddSingleton<IDbClient>(dbClient);
      return mongoConfig;
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.EnvironmentName == "Development")
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Home/Error");
      }

      app.UseAuthentication();

      app.UseMvc();
    }
  }
}
