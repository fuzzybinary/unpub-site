using System;
using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Unpub.Server.JsonConverters
{
  public class RectangleJsonConverter : JsonConverter<Rectangle>
  {
    public override Rectangle ReadJson(JsonReader reader, Type objectType, Rectangle existingValue, bool hasExistingValue, JsonSerializer serializer)
    {
      JObject jo = JObject.Load(reader);
      return new Rectangle((int)jo["left"], (int)jo["top"], (int)jo["width"], (int)jo["height"]);
    }

    public override void WriteJson(JsonWriter writer, Rectangle value, JsonSerializer serializer)
    {
      Rectangle rect = (Rectangle)value;
      JObject jo = new()
      {
        { "top", rect.Top },
        { "left", rect.Left },
        { "width", rect.Width },
        { "height", rect.Height }
      };
      jo.WriteTo(writer);
    }
  }
}