using AspNetCore.Identity.Mongo.Model;
using MongoDB.Bson;

namespace Unpub.Server.Models
{
  public class UnpubUser : MongoUser, Identifyable
  {
    public override ObjectId Id { get; set; }

    public string DisplayName { get; set; }

    public override string Email { get; set; }

    public string? AvatarPath { get; set; }

    public UnpubUser(string displayName, string email, string? avatarPath = null)
    {
      DisplayName = displayName;
      Email = email;
      UserName = email;
      AvatarPath = avatarPath;
    }
  }
}