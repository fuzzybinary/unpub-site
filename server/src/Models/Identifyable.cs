using MongoDB.Bson;

namespace Unpub.Server.Models
{
#pragma warning disable IDE1006
  public interface Identifyable
  {
    ObjectId Id { get; set; }
  }
}