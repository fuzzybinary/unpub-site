using System;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Unpub.Server.Database;

namespace Unpub.Server.Models
{
  [JsonConverter(typeof(StringEnumConverter))]
  public enum DiscreetAnswer
  {
    Yes = 0,
    Maybe = 1,
    No = 2
  }

  [DbAutoRegister]
  public class Feedback : Identifyable
  {
    public ObjectId Id { get; set; }

    public ObjectId GameId { get; set; }

    public DateTime Created { get; set; }

    public int Time { get; set; }

    public int Players { get; set; }

    public int FirstPlayerScore { get; set; }

    public int LastPlayerScore { get; set; }

    public bool IsWinner { get; set; }

    public int GameLength { get; set; }

    public int EaseOfLearning { get; set; }

    public int DownTime { get; set; }

    public int Decisions { get; set; }

    public int Interactivity { get; set; }

    public int Originality { get; set; }

    public int Fun { get; set; }

    public DiscreetAnswer Predictable { get; set; }

    public string PredictableWhy { get; set; } = "";

    public DiscreetAnswer PlayAgain { get; set; }

    public DiscreetAnswer Buy { get; set; }

    public string ChangeOneThing { get; set; } = "";

    public string FavoritePart { get; set; } = "";

    public string Comments { get; set; } = "";

    public bool IsAnonymous { get; set; }

    public FeedbackMeta? FeedbackMeta { get; set; }
  }

  [DbAutoRegister]
  public class FeedbackMeta
  {
    public ObjectId? FromRegisteredUserId { get; set; }

    public string? FromName { get; set; }

    public string? FromEmail { get; set; }

    public FeedbackMeta(ObjectId? fromRegisteredUserId, string? fromName, string? fromEmail)
    {
      FromRegisteredUserId = fromRegisteredUserId;
      FromName = fromName;
      FromEmail = fromEmail;
    }
  }
}