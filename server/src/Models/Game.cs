using System.Collections.Generic;
using MongoDB.Bson;
using Unpub.Server.Database;

namespace Unpub.Server.Models
{
  [DbAutoRegister]
  public class Game : Identifyable
  {
    public ObjectId Id { get; set; }

    public ObjectId OwnerId { get; set; }

    public string? AvatarPath { get; set; }

    public string Name { get; set; }

    public string Time { get; set; }

    public int MinAge { get; set; }

    public int MinPlayers { get; set; }

    public int MaxPlayers { get; set; }

    public List<string> Mechanisms { get; set; }

    public string Description { get; set; }

    public Game(ObjectId ownerId, string name, string time,
      int minAge, int minPlayers, int maxPlayers, List<string> mechanisms, string description,
      ObjectId? id = null, string? avatarPath = null)
    {
      Id = id ?? new ObjectId();
      OwnerId = ownerId;
      AvatarPath = avatarPath;
      Name = name;
      Time = time;
      MinAge = minAge;
      MinPlayers = minPlayers;
      MaxPlayers = maxPlayers;
      Mechanisms = mechanisms;
      Description = description;
    }
  }
}