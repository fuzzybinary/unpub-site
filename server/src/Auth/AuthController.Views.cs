using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Unpub.Server.Controllers;

namespace Unpub.Server.Auth
{
  public class ForgotPasswordParams
  {
    public ForgotPasswordParams(string email)
    {
      Email = email;
    }

    [Required]
    [EmailAddress]
    [Display(Name = "Email")]
    public string Email { get; }
  }

  public class RegisterCredentials
  {
    public RegisterCredentials(string displayName, string email, string password)
    {
      DisplayName = displayName;
      Email = email;
      Password = password;
    }

    [Required]
    [Display(Name = "Display Name")]
    public string DisplayName { get; }

    [Required]
    [EmailAddress]
    [Display(Name = "Email")]
    public string Email { get; }

    [Required]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; }
  }

  public class Credentials
  {
    public Credentials(string email, string password)
    {
      Email = email;
      Password = password;
    }

    [Required]
    [EmailAddress]
    [Display(Name = "Email")]
    public string Email { get; }

    [Required]
    [DataType(DataType.Password)]
    [Display(Name = "Password")]
    public string Password { get; }
  }

  public class AuthResponse
  {
    public AuthResponse(string accessToken, UnpubUserSummary userInfo)
    {
      AccessToken = accessToken;
      UserInfo = userInfo;
    }

    public string AccessToken { get; }

    public UnpubUserSummary UserInfo { get; }
  }

  public class ResetPasswordToken
  {
    public ResetPasswordToken(string resetToken, string email)
    {
      ResetToken = resetToken;
      Email = email;
    }

    public string ResetToken { get; }

    public string Email { get; }
  }

  public class ResetPasswordModel
  {
    public ResetPasswordModel(string email, string password, string token)
    {
      Email = email;
      Password = password;
      Token = token;
    }

    [Required]
    [EmailAddress]
    [Display(Name = "Email")]
    public string Email { get; }

    [Required]
    public string Password { get; }

    [Required]
    public string Token { get; }
  }
}