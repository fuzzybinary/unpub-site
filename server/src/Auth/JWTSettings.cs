namespace Unpub.Server.Auth
{
  public class JWTSettings
  {
    public string SecretKey { get; set; } = default!;
    public string Issuer { get; set; } = default!;
    public string Audience { get; set; } = default!;
  }
}