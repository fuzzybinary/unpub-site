using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Unpub.Server.Controllers;
using Unpub.Server.Database;
using Unpub.Server.Models;
using Unpub.Server.Services;

namespace Unpub.Server.Auth
{
  [Route("api/[controller]")]
  public class AuthController : Controller
  {
    private readonly UserManager<UnpubUser> _userManager;
    private readonly RoleManager<UnpubRole> _roleManager;
    private readonly SignInManager<UnpubUser> _signInManager;
    private readonly JWTService _jwt;
    private readonly IMailSenderService _mailSender;
    private readonly IFileService _fileService;

    public AuthController(UserManager<UnpubUser> userManager,
      RoleManager<UnpubRole> roleManager, SignInManager<UnpubUser> signInManager,
      JWTService jwt, IMailSenderService mailSender,
      IFileService fileService)
    {
      _userManager = userManager;
      _roleManager = roleManager;
      _signInManager = signInManager;
      _jwt = jwt;
      _mailSender = mailSender;
      _fileService = fileService;
    }

    [HttpPost("register")]
    public async Task<ActionResult<AuthResponse>> Register([FromBody] RegisterCredentials credentials)
    {
      if (ModelState.IsValid)
      {
        var user = new UnpubUser(
          displayName: credentials.DisplayName,
          email: credentials.Email
        );
        var result = await _userManager.CreateAsync(user, credentials.Password);
        if (result.Succeeded)
        {
          await _signInManager.SignInAsync(user, isPersistent: false);
          var additionalClaims = await GetPermissionClaims(user);
          return new AuthResponse(
            accessToken: _jwt.GetToken(user.Id.ToString(), credentials.Email, additionalClaims),
            userInfo: new UnpubUserSummary(
              id: user.Id.ToString(),
              displayName: user.DisplayName,
              avatarUrl: _fileService.UrlForUserContent(user.AvatarPath)
            )
          );
        }

        return Error(result);
      }

      return ModelStateError();
    }

    [HttpPost("login")]
    public async Task<ActionResult<AuthResponse>> Login([FromBody] Credentials credentials)
    {
      if (ModelState.IsValid)
      {
        var result = await _signInManager.PasswordSignInAsync(credentials.Email, credentials.Password, false, false);
        if (result.Succeeded)
        {
          var user = await _userManager.FindByEmailAsync(credentials.Email);
          var additionalClaims = await GetPermissionClaims(user);
          var userSummary = new UnpubUserSummary(
            user.Id.ToString(),
            user.DisplayName,
            _fileService.UrlForUserContent(user.AvatarPath)
          );
          return new AuthResponse(
            accessToken: _jwt.GetToken(user.Id.ToString(), credentials.Email, additionalClaims),
            userInfo: userSummary
          );
        }

        return new JsonResult(new string[] { "Invalid username or password" }) { StatusCode = 401 };
      }

      return ModelStateError();
    }

    [HttpGet("refresh")]
    public async Task<ActionResult<AuthResponse>> Refresh()
    {
      var user = HttpContext.User;
      if (!user.Identity?.IsAuthenticated ?? false)
        return Forbid();

      var userId = user.FindFirst(ClaimTypes.NameIdentifier)?.Value;
      var dbUser = await _userManager.FindByIdAsync(userId);
      if (dbUser == null)
        return Forbid();

      var additionalClaims = await GetPermissionClaims(dbUser);

      var userSummary = new UnpubUserSummary(
        id: dbUser.Id.ToString(),
        displayName: dbUser.DisplayName,
        avatarUrl: _fileService.UrlForUserContent(dbUser.AvatarPath)
      );
      return new AuthResponse(
        accessToken: _jwt.GetToken(dbUser.Id.ToString(), dbUser.Email, additionalClaims),
        userInfo: userSummary
      );
    }

    [HttpPost("forgotPassword")]
    //[ValidateAntiForgeryToken]
    public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordParams parameters)
    {
      if (!ModelState.IsValid)
      {
        return this.ModelStateError();
      }

      var user = await _userManager.FindByEmailAsync(parameters.Email);
      if (user == null)
        return Ok();

      var token = await _userManager.GeneratePasswordResetTokenAsync(user);
      var resetToken = new ResetPasswordToken(
        resetToken: token,
        email: user.Email
      );
      var responseJson = JsonConvert.SerializeObject(resetToken, Formatting.None);
      var responseString = Convert.ToBase64String(Encoding.UTF8.GetBytes(responseJson));

      var callbackUrl = new Uri($"{Request.Scheme}://{Request.Host}/resetPassword?token={responseString}");

      var emailMessage = new Message(new string[] { user.Email }, "Unpub: Password Reset Request",
        $@"You, or someone pretending to be you, has requested a password reset for your Unpub account.
        
If this was you and you'd like to reset your password, please visit the link below to reset your password.

{callbackUrl}");

      await _mailSender.SendMail(emailMessage);

      return Ok();
    }

    [HttpPost("resetPassword")]
    //[ValidateAntiForgeryToken]
    public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel resetPassword)
    {
      if (!ModelState.IsValid)
        return this.ModelStateError();

      var user = await _userManager.FindByEmailAsync(resetPassword.Email);
      if (user == null)
        return NotFound();

      var result = await _userManager.ResetPasswordAsync(user, resetPassword.Token, resetPassword.Password);
      if (!result.Succeeded)
      {
        foreach (var error in result.Errors)
        {
          ModelState.TryAddModelError(error.Code, error.Description);
        }
        return this.ModelStateError();
      }

      return Ok();
    }

    private JsonResult ModelStateError()
    {
      var items = ModelState.Values.SelectMany(x => x.Errors)
        .Select(x => x.ErrorMessage)
        .ToArray();
      return new JsonResult(new Dictionary<string, string[]> {
        { "errors", items }
      })
      { StatusCode = 400 };
    }

    private static JsonResult Error(IdentityResult result)
    {
      var items = result.Errors
          .Select(x => x.Description)
          .ToArray();
      return new JsonResult(items) { StatusCode = 400 };
    }

    private async Task<List<Claim>> GetPermissionClaims(UnpubUser user)
    {
      var claims = new List<Claim>();
      var userClaims = await _userManager.GetClaimsAsync(user);
      var userRoles = await _userManager.GetRolesAsync(user);
      claims.AddRange(userClaims);
      foreach (var userRole in userRoles)
      {
        claims.Add(new Claim(ClaimTypes.Role, userRole));
        var role = await _roleManager.FindByNameAsync(userRole);
        if (role != null)
        {
          var roleClaims = await _roleManager.GetClaimsAsync(role);
          foreach (var roleClaim in roleClaims)
          {
            claims.Add(roleClaim);
          }
        }
      }
      return claims;
    }
  }
}