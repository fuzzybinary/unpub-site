using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Unpub.Server.Auth
{
  public class JWTService
  {
    private readonly JWTSettings _settings;

    public JWTService(JWTSettings settings)
    {
      _settings = settings;
    }

    public string GetToken(string userId, string email, List<Claim> additionalClaims)
    {
      var claims = new List<Claim> {
        new Claim(JwtRegisteredClaimNames.Sub, userId),
        new Claim(JwtRegisteredClaimNames.Email, email),
        new Claim(ClaimTypes.Email, email),
        new Claim(ClaimTypes.NameIdentifier, userId)
      };

      if (additionalClaims != null)
      {
        claims.AddRange(additionalClaims);
      }

      var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_settings.SecretKey));
      var credentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
      JwtSecurityToken token = new(
        issuer: _settings.Issuer,
        audience: _settings.Audience,
        signingCredentials: credentials,
        claims: claims,
        notBefore: DateTime.UtcNow,
        expires: DateTime.UtcNow.AddDays(7)
      );

      return new JwtSecurityTokenHandler().WriteToken(token);
    }
  }
}