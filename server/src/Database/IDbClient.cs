using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Unpub.Server.Models;

namespace Unpub.Server.Database
{
  public interface IDbClient
  {
    Task<ObjectId> CreateItemAsync<T>(T item)
      where T : Identifyable;

    Task<List<T>> GetAll<T>();

    Task<T> GetById<T>(ObjectId id) where T : Identifyable;

    Task<List<T>> GetWhere<T>(Expression<Func<T, bool>> expression, Expression<Func<T, object>>? sortField = null);

    Task<bool> DeleteById<T>(ObjectId id) where T : Identifyable;

    Task<bool> TrashById<T>(ObjectId id) where T : Identifyable;

    Task<List<T>> GetAllFromTrash<T>();


    Task<bool> Replace<T>(T item) where T : Identifyable;

    [return: MaybeNull]
    Task<T> AddChildItem<Parent, T>(ObjectId parentId, Expression<Func<Parent, IEnumerable<T>>> field, T newChild)
      where Parent : Identifyable
      where T : class?;
  }
}