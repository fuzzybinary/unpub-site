using System;

namespace Unpub.Server.Database
{
  [AttributeUsage(AttributeTargets.Class)]
  public class CollectionNameAttribute : Attribute
  {
    public string Name { get; set; }

    public CollectionNameAttribute(string name)
    {
      Name = name;
    }
  }
}