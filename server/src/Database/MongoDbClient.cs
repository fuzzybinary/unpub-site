using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using Unpub.Server.Models;

namespace Unpub.Server.Database
{
  public class MongoConnectionInfo
  {
    public string Host { get; set; } = default!;
    public string Database { get; set; } = default!;
    public string Username { get; set; } = default!;
    public string Password { get; set; } = default!;

    public string ConnectionString
    {
      get
      {
        string userPartial = "";
        if (Username != null && Password != null)
        {
          var password = HttpUtility.UrlEncode(Password);
          userPartial = $"{Username}:{password}@";
        }
        return $"mongodb://{userPartial}{Host}";
      }
    }

    /// <summary>Connection string including database name</summary>
    public string FullConnectionString
    {
      get { return $"{ConnectionString}/{Database}"; }
    }
  }

  public class MongoDbClient : IDbClient
  {
    private static bool ModelClassesRegistered = false;

    private readonly MongoClient _client;
    private readonly IMongoDatabase _database;

    private MongoDbClient(MongoClient client, IMongoDatabase database)
    {
      _client = client;
      _database = database;
    }

    public async Task<ObjectId> CreateItemAsync<T>(T item)
      where T : Identifyable
    {
      var collection = CollectionForClass<T>();
      await collection.InsertOneAsync(item);

      return item.Id;
    }

    public async Task<List<T>> GetAll<T>()
    {
      var collection = CollectionForClass<T>();
      return await collection.Find(_ => true).ToListAsync();
    }

    public async Task<T> GetById<T>(ObjectId id) where T : Identifyable
    {
      var query = Builders<T>.Filter.Eq("_id", id);
      var collection = CollectionForClass<T>();
      return await collection.Find(query).FirstOrDefaultAsync();
    }

    public async Task<List<T>> GetWhere<T>(Expression<Func<T, bool>> expression, Expression<Func<T, object>>? sortField = null)
    {
      var query = Builders<T>.Filter.Where(expression);
      var sort = Builders<T>.Sort.Descending(sortField);
      var collection = CollectionForClass<T>();
      return await collection.Find(query).Sort(sort).ToListAsync();
    }

    public async Task<bool> Replace<T>(T item) where T : Identifyable
    {
      var collection = CollectionForClass<T>();
      var query = Builders<T>.Filter.Eq("_id", item.Id);
      var result = await collection.ReplaceOneAsync(query, item);
      return result.ModifiedCount == 1;
    }

    public async Task<bool> DeleteById<T>(ObjectId id) where T : Identifyable
    {
      var query = Builders<T>.Filter.Eq("_id", id);
      var collection = CollectionForClass<T>();
      var result = await collection.DeleteOneAsync(query);
      return result.DeletedCount == 1;
    }

    public async Task<bool> TrashById<T>(ObjectId id) where T : Identifyable
    {
      var collection = CollectionForClass<T>();
      var trashCollection = TrashCollectionForClass<T>();
      var query = Builders<T>.Filter.Eq("_id", id);
      var item = await collection.Find(query).FirstOrDefaultAsync();
      await trashCollection.InsertOneAsync(item);

      var result = await collection.DeleteOneAsync(query);

      return result.DeletedCount == 1;
    }

    public async Task<List<T>> GetAllFromTrash<T>()
    {
      var collection = TrashCollectionForClass<T>();
      return await collection.Find(_ => true).ToListAsync();
    }

    [return: MaybeNull]
    public async Task<T> AddChildItem<Parent, T>(ObjectId parentId, Expression<Func<Parent, IEnumerable<T>>> field, T newChild)
      where Parent : Identifyable
      where T : class?
    {
      var collectionName = CollectionNameForClass<Parent>();
      var query = Builders<Parent>.Update.AddToSet(field, newChild);
      var collection = _database.GetCollection<Parent>(collectionName);
      var result = await collection.FindOneAndUpdateAsync<Parent>(x => x.Id == parentId, query,
        new FindOneAndUpdateOptions<Parent>() { ReturnDocument = ReturnDocument.After });
      if (result != null)
      {
        return field.Compile().Invoke(result).Last();
      }
      return default!;
    }

    private static string CollectionNameForClass<T>()
    {
      var collectionNameAttr = typeof(T).GetCustomAttribute<CollectionNameAttribute>();
      string collectionName = collectionNameAttr != null ? collectionNameAttr.Name : (typeof(T).Name + "s");
      return collectionName;
    }

    private IMongoCollection<T> CollectionForClass<T>()
    {
      var collectionName = CollectionNameForClass<T>();
      return _database.GetCollection<T>(collectionName);
    }

    private IMongoCollection<T> TrashCollectionForClass<T>()
    {
      var collectionName = CollectionNameForClass<T>() + "_Trash";
      return _database.GetCollection<T>(collectionName);
    }

    public static Task<MongoDbClient> Create(MongoConnectionInfo connectionInfo)
    {
      var url = new MongoUrl(connectionInfo.FullConnectionString);
      var mongoClient = new MongoClient(connectionInfo.FullConnectionString);

      var client = new MongoDbClient(mongoClient, mongoClient.GetDatabase(url.DatabaseName));

      return Task.FromResult(client);
    }

    public static void RegisterModelClasses()
    {
      if (!ModelClassesRegistered)
      {
        var assembly = Assembly.GetExecutingAssembly();
        var types = assembly
          .GetTypes()
          .Where(type => type.GetCustomAttribute(typeof(DbAutoRegisterAttribute)) != null);
        foreach (var type in types)
        {
          RegisterClass(type);
        }
        ModelClassesRegistered = true;
      }
    }

    private static void RegisterClass(Type type)
    {
      var bm = new BsonClassMap(type);
      var properties = type.GetProperties();
      foreach (var property in properties)
      {
        if (property.Name == "Id")
        {
          bm.MapIdProperty(property.Name)
            .SetElementName("id")
            .SetIdGenerator(new ObjectIdGenerator());
        }
        else
        {
          var fieldName = property.Name;
          var propertyName = char.ToLower(fieldName[0]) + fieldName[1..];
          bm.MapProperty(fieldName).SetElementName(propertyName);
        }
      }

      BsonClassMap.RegisterClassMap(bm);
    }
  }
}