using System;

namespace Unpub.Server.Database
{
  [AttributeUsage(AttributeTargets.Property)]
  public class DbIgnoreAttribute : Attribute
  {

  }
}