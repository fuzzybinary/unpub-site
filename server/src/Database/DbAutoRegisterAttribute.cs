using System;

namespace Unpub.Server.Database
{
  [AttributeUsage(AttributeTargets.Class)]
  public class DbAutoRegisterAttribute : Attribute
  {

  }
}