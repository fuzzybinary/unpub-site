require("dotenv").config();

const ghostContentAPI = require("@tryghost/content-api");

let ghostUrl = process.env.GHOST_API_URL;
let key = process.env.GHOST_CONTENT_API_KEY;

// Init Ghost API
const api = new ghostContentAPI({
  url: `${ghostUrl}${process.env.GHOST_SUBDIR}`,
  key: key,
  version: "v2"
});

// Get all site information
module.exports = async function() {
  const siteData = await api.settings
    .browse({
      include: "icon,url"
    })
    .catch(err => {
      console.error(err);
    });

  if (process.env.SITE_URL) siteData.url = process.env.SITE_URL;

  return siteData;
};
