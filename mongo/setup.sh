set -e

file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}

file_env "MONGO_INITDB_PASSWORD" 

mongo <<EOF
use $MONGO_INITDB_DATABASE 
db.createUser({
  user: '$MONGO_INITDB_USER',
  pwd: '$MONGO_INITDB_PASSWORD',
  roles: [ "readWrite" ]
});
EOF