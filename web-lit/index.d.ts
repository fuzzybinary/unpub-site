declare module 'layout' {
  export class Cluster extends HTMLElement {}
  export class Stack extends HTMLElement {}
}

declare module '*.scss' {
  import { CSSResultGroup } from 'lit';

  const content: CSSResultGroup;
  export default content;
}

declare module '*.css' {
  import { CSSResultGroup } from 'lit';

  const content: CSSResultGroup;
  export default content;
}
