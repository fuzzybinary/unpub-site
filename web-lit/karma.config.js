var webpackConfig = require('./webpack.config.js');

module.exports = function(config) {
  config.set({
    basePath: './src',
    frameworks: ['mocha', 'chai', 'sinon'],
    files: [
      'test-init.js',
      'index.ts',
      {
        pattern: '**/*.test.ts'
      }
    ],
    exclude: [],
    preprocessors: {
      '**/*.ts': ['webpack', 'sourcemap']
    },
    webpack: webpackConfig({
      MODE: 'development'
    }),
    reporters: ['mocha'],
    mochaReporter: {
      showDiff: true
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    browsers: ['ChromeHeadless'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },
    concurrency: Infinity
  });
};
