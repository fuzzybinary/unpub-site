const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PnpWebpackPlugin = require(`pnp-webpack-plugin`);
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = env => {
  return {
    mode: env.MODE,
    entry: ['./src/sass/styles.scss', './src/index.ts'],
    devtool: env.MODE === 'development' ? 'inline-source-map' : false,
    devServer: {
      static: [
        {
          directory: path.resolve(__dirname, './dist'),
          publicPath: '/'
        },
        {
          directory: path.resolve(__dirname, './usercontent'),
          publicPath: '/usercontent'
        }
      ],
      historyApiFallback: true
    },
    module: {
      rules: [
        {
          test: [/\.scss$/],
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
          include: [path.resolve('./src/sass/styles.scss')]
        },
        {
          test: [/\.scss$/, /\.css$/],
          use: [
            {
              loader: 'lit-scss-loader',
              options: {
                minify: env.MODE == 'production'
              }
            },
            'extract-loader',
            'css-loader',
            'sass-loader'
          ],
          exclude: path.resolve('./src/sass/styles.scss')
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'fonts/'
              }
            }
          ]
        },
        {
          test: /\.tsx?$/,
          loader: require.resolve('ts-loader'),
          options: PnpWebpackPlugin.tsLoaderOptions({
            // ... regular options go there ...
          }),
          exclude: [/node_modules/]
        },
        {
          test: /\.(html)$/,
          use: {
            loader: 'html-loader',
            options: {
              attrs: false
            }
          }
        }
      ]
    },
    resolve: {
      plugins: [new TsconfigPathsPlugin()],
      extensions: ['.tsx', '.ts', '.js'],
      fallback: {
        '/__web-dev-server__web-socket.js': false,
        buffer: require.resolve('buffer'),
        stream: require.resolve('stream-browserify'),
        crypto: require.resolve('crypto-browserify')
      }
    },
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/'
    },
    plugins: [
      new webpack.IgnorePlugin({
        resourceRegExp: /^data\:text.*/
      }),
      new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer']
      }),
      new MiniCssExtractPlugin(),
      new CleanWebpackPlugin(),
      new CopyPlugin([
        {
          from: 'node_modules/@shoelace-style/shoelace/dist/assets',
          to: 'assets'
        },
        {
          from: 'node_modules/@webcomponents/webcomponentsjs',
          to: 'webcomponentsjs'
        },
        {
          from: 'public/images',
          to: 'images'
        }
      ]),
      new HtmlWebpackPlugin({
        inject: 'head',
        template: 'public/index.html'
      })
    ]
  };
};
