import { AppConfig } from 'models/Config';
import {
  FeedbackDetails,
  FeedbackSubmissionResult,
  GameCreateInfo,
  GameDetails,
  GameDetailsPrivate,
  GameDetailsPublic,
  GameFeedbackSubmission,
  GameSummary
} from 'models/Game';
import { UserProfileDetail } from 'models/UserModels';

import { AuthService } from './AuthService';

export interface ApiResult<T> {
  status: 'ok' | 'failed' | 'forbidden';
  data: T | null;
  messages: string[];
}

// TODO: Temp, move this once I know the service works
interface CreateAvatarData {
  top: number;
  left: number;
  width: number;
  height: number;
}

export class UnpubService {
  public _authService: AuthService;
  public _config!: AppConfig;

  public get config() {
    return this._config;
  }

  constructor(authService: AuthService) {
    this._authService = authService;
  }

  public async initConfig(): Promise<AppConfig> {
    const config = await (await this.performApiQuery<AppConfig>('/api/config', 'GET')).data;
    if (config) {
      this._config = config;
    } else {
      // Sensible default?
      this._config = {
        maxAvatarSizeBytes: 1 * 1024 * 1024,
        maxPictureSizeBytes: 5 * 1024 * 1024
      };
    }
    return this._config;
  }

  public async fetchMe(): Promise<UserProfileDetail | null> {
    if (!this._authService.isAuthenticated()) {
      return null;
    }

    const me = (await this.performApiQuery<UserProfileDetail>('/api/users/me', 'GET')).data;
    if (me) {
      if (me.avatarUrl && this._authService.userInfo?.avatarUrl !== me.avatarUrl) {
        this._authService.avatarChanged(me.avatarUrl);
      }
    }
    return me;
  }

  public async postAvatar(createData: CreateAvatarData, file: File): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const formData = new FormData();
      const createBody = JSON.stringify(createData);

      formData.append('createData', createBody);
      formData.append('postedFile', file);

      const request = new XMLHttpRequest();
      request.onreadystatechange = () => {
        if (request.readyState !== 4) {
          return;
        }
        if (request.status >= 200 && request.status <= 300) {
          const newAvatar = request.getResponseHeader('Location')!;
          this._authService.avatarChanged(newAvatar);
          resolve(newAvatar);
        } else {
          reject(request.statusText);
        }
      };
      request.open('POST', '/api/users/me/avatar');
      request.setRequestHeader('Accept', 'application/json');
      request.setRequestHeader('Authorization', `Bearer ${this._authService.token}`);
      request.send(formData);
    });
  }

  public async fetchGames(): Promise<GameSummary[] | null> {
    return (await this.performApiQuery<GameSummary[]>('/api/games', 'GET')).data;
  }

  public async fetchGame(id: string): Promise<GameDetailsPublic | null> {
    return (await this.performApiQuery<GameDetailsPublic>(`/api/games/${id}`, 'GET')).data;
  }

  public async submitFeedback(
    id: string,
    feedback: GameFeedbackSubmission
  ): Promise<FeedbackSubmissionResult | null> {
    return (
      await this.performApiQuery<FeedbackSubmissionResult>(
        `/api/games/${id}/feedback`,
        'POST',
        feedback
      )
    ).data;
  }

  public async fetchFeedbackDetails(id: string): Promise<ApiResult<FeedbackDetails>> {
    return await this.performApiQuery<FeedbackDetails>(`/api/feedback/${id}`, 'GET');
  }

  public async deleteFeedback(id: string): Promise<ApiResult<void>> {
    return await this.performApiQuery(`/api/feedback/${id}`, 'DELETE');
  }

  public async createGame(info: GameCreateInfo): Promise<ApiResult<GameDetails>> {
    return await this.performApiQuery<GameDetails>('/api/me/games/', 'POST', info);
  }

  public async updateGame(id: string, info: GameCreateInfo): Promise<ApiResult<GameDetails>> {
    return this.performApiQuery<GameDetails>(`/api/me/games/${id}`, 'POST', info);
  }

  public async fetchPrivateGame(id: string): Promise<ApiResult<GameDetailsPrivate>> {
    return await this.performApiQuery<GameDetailsPrivate>(`/api/me/games/${id}`, 'GET');
  }

  public async deleteGame(id: string): Promise<ApiResult<void>> {
    return await this.performApiQuery(`/api/me/games/${id}`, 'DELETE');
  }

  private async performApiQuery<T>(
    path: string,
    method: string,
    body: any = null
  ): Promise<ApiResult<T>> {
    const headers: any = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };
    if (this._authService.isAuthenticated()) {
      headers.Authorization = `Bearer ${this._authService.token}`;
    }

    const postBody = body ? JSON.stringify(body) : undefined;
    const response = await fetch(path, {
      body: postBody,
      headers,
      method
    });
    if (response.ok) {
      let responseBody: T | null = null;
      if (response.status !== 204) {
        responseBody = (await response.json()) as T;
      }
      return {
        status: 'ok',
        data: responseBody,
        messages: []
      };
    } else {
      const errorText = await response.text();
      console.log(errorText);
    }
    return {
      status: response.status === 403 ? 'forbidden' : 'failed',
      data: null,
      messages: []
    };
  }
}
