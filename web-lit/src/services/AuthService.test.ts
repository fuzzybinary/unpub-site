import { expect } from 'chai';
import fetchMock from 'fetch-mock';
import * as jwt from 'jwt-simple';
import { UserSummary } from 'models/UserModels';
import sinon, { SinonStub } from 'sinon';
import { AuthService } from './AuthService';

describe('AuthService', () => {
  let getItemStub: SinonStub<[string], string | null>;
  let setItemStub: SinonStub<[string, string], void>;

  beforeEach(() => {
    getItemStub = sinon.stub(localStorage, 'getItem');
    setItemStub = sinon.stub(localStorage, 'setItem');
  });

  afterEach(() => {
    sinon.restore();
    fetchMock.restore();
  });

  describe('initialization', () => {
    it('initially is not authenticated', () => {
      getItemStub.withArgs(AuthService.TokenStorageKey).returns(null);

      const auth = new AuthService();

      expect(auth.token).to.be.null;
      expect(auth.isAuthenticated()).to.equal(false);
    });

    it('reads a token from local storage', () => {
      const auth = new AuthService();

      expect(getItemStub.calledWith(AuthService.TokenStorageKey)).to.be.ok;
    });

    it('authenticated if the token is valid', () => {
      const fakeToken = jwt.encode(
        {
          exp: Date.now() / 1000 + 5000
        },
        'xxx'
      );
      getItemStub.withArgs(AuthService.TokenStorageKey).returns(fakeToken);

      const auth = new AuthService();

      expect(auth.token).to.equal(fakeToken);
      expect(auth.isAuthenticated()).to.equal(true);
    });

    it('is not authenticated if token is expired', () => {
      const fakeToken = jwt.encode(
        {
          exp: Date.now() / 1000 - 1000
        },
        'xxx'
      );
      getItemStub.withArgs(AuthService.TokenStorageKey).returns(fakeToken);

      const auth = new AuthService();

      expect(auth.token).to.equal(fakeToken);
      expect(auth.isAuthenticated()).to.equal(false);
    });

    it('reads user info from local storage', () => {
      const userInfo: UserSummary = {
        id: 'fake.id',
        displayName: 'fake name',
        avatarUrl: 'fake://url'
      };
      getItemStub.withArgs(AuthService.UserInfoStorageKey).returns(JSON.stringify(userInfo));

      const auth = new AuthService();

      expect(getItemStub.calledWith(AuthService.UserInfoStorageKey)).to.be.ok;
      expect(auth.userInfo).to.deep.equal(userInfo);
    });
  });

  describe('register', () => {
    const RegisterEndpoint = '/api/auth/register';

    it('posts to register endpoint', async () => {
      fetchMock.mock(RegisterEndpoint, {});

      const auth = new AuthService();
      const result = await auth.register('name', 'email@email.com', 'password');

      const call = fetchMock.lastCall(RegisterEndpoint);
      expect(call).to.be.ok;

      expect(call?.[1]?.method).to.equal('POST');
      const expectedBody = {
        displayName: 'name',
        email: 'email@email.com',
        password: 'password'
      };
      expect(JSON.parse(call?.[1]?.body?.toString() || '')).to.deep.equal(expectedBody);
    });

    it('responds with success on success', async () => {
      const registerResponse = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(RegisterEndpoint, registerResponse);

      const auth = new AuthService();
      const result = await auth.register('name', 'email@email.com', 'password');

      expect(result.success).to.equal(true);
    });

    it('sets the token on success', async () => {
      const registerResponse = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(RegisterEndpoint, registerResponse);

      const auth = new AuthService();
      await auth.register('name', 'email@email.com', 'password');

      expect(auth.token).to.equal('fake.access.token');
    });

    it('sets user info on success', async () => {
      const userInfo: UserSummary = {
        id: 'fake.id',
        displayName: 'fake name',
        avatarUrl: 'fake://url'
      };
      const registerResponse = {
        accessToken: 'fake.access.token',
        userInfo
      };
      fetchMock.mock(RegisterEndpoint, registerResponse);

      const auth = new AuthService();
      await auth.register('name', 'email@email.com', 'password');

      expect(auth.userInfo).to.deep.equal(userInfo);
    });

    it('saves the token to localstorage', async () => {
      const registerResponse = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(RegisterEndpoint, registerResponse);

      const auth = new AuthService();
      await auth.register('name', 'email@email.com', 'password');

      expect(setItemStub.calledWith(AuthService.TokenStorageKey, 'fake.access.token')).to.be.ok;
    });
  });

  describe('login', () => {
    const LoginEndpoint = '/api/auth/login';

    it('posts to login', async () => {
      fetchMock.mock(LoginEndpoint, {});

      const auth = new AuthService();
      const result = await auth.login('email@email.com', 'password');

      const call = fetchMock.lastCall(LoginEndpoint);
      expect(call).to.be.ok;

      expect(call?.[1]?.method).to.equal('POST');

      const expectedBody = {
        email: 'email@email.com',
        password: 'password'
      };
      expect(JSON.parse(call?.[1]?.body?.toString() || '')).to.deep.equal(expectedBody);
    });

    it('sets the token on success', async () => {
      const loginRespone = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(LoginEndpoint, loginRespone);

      const auth = new AuthService();
      await auth.login('email@email.com', 'password');

      expect(auth.token).to.equal('fake.access.token');
    });

    it('sets the user info on success', async () => {
      const userInfo: UserSummary = {
        id: 'fake.id',
        displayName: 'fake name',
        avatarUrl: 'fake://url'
      };
      const loginRespone = {
        accessToken: 'fake.access.token',
        userInfo
      };
      fetchMock.mock(LoginEndpoint, loginRespone);

      const auth = new AuthService();
      await auth.login('email@email.com', 'password');

      expect(auth.userInfo).to.deep.equal(userInfo);
    });

    it('saves the token to localstorage', async () => {
      const loginRespone = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(LoginEndpoint, loginRespone);

      const auth = new AuthService();
      await auth.login('email@email.com', 'password');

      expect(setItemStub.calledWith(AuthService.TokenStorageKey, 'fake.access.token')).to.be.ok;
    });

    it('clears localstorage on logout', async () => {
      const loginRespone = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(LoginEndpoint, loginRespone);
      const removeItemSpy = sinon.stub(localStorage, 'removeItem');

      const auth = new AuthService();
      await auth.login('email@email.com', 'password');

      auth.logout();
      expect(removeItemSpy.calledWith(AuthService.TokenStorageKey)).to.be.ok;
      expect(auth.token).to.be.null;
    });

    it('calls auth changed on login', async () => {
      const authChangedMock = sinon.fake();

      const loginResponse = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(LoginEndpoint, loginResponse);

      const auth = new AuthService();
      auth.subscribeToAuthChanges(authChangedMock);
      await auth.login('email@email.com', 'password');

      expect(authChangedMock.calledWith(true)).to.be.ok;
      auth.unsubscribeToAuthChanges(authChangedMock);
    });

    it('calls auth changed on logout', async () => {
      const authChangedMock = sinon.fake();

      const loginResponse = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(LoginEndpoint, loginResponse);

      const auth = new AuthService();
      auth.subscribeToAuthChanges(authChangedMock);
      await auth.login('email@email.com', 'password');
      auth.logout();

      const changeCalls = authChangedMock.getCalls();
      expect(changeCalls.length).to.equal(2);
      expect(changeCalls[0].args[0]).to.equal(true);
      expect(changeCalls[1].args[0]).to.equal(false);
      auth.unsubscribeToAuthChanges(authChangedMock);
    });
  });

  describe('refresh', () => {
    const RefreshEndpoint = '/api/auth/refresh';

    it('fails if not authenticated', async () => {
      fetchMock.mock(RefreshEndpoint, {});
      const auth = new AuthService();

      const result = await auth.refresh();
      expect(result).to.not.be.ok;
    });

    it('calls refresh', async () => {
      fetchMock.mock(RefreshEndpoint, {});
      const fakeToken = jwt.encode(
        {
          exp: Date.now() / 1000 - 1000
        },
        'xxx'
      );
      getItemStub.withArgs(AuthService.TokenStorageKey).returns(fakeToken);

      const auth = new AuthService();
      const result = await auth.refresh();

      const call = fetchMock.lastCall(RefreshEndpoint);
      expect(call).to.be.ok;

      expect(call?.[1]?.method).to.equal('GET');
      const headers: any = call?.[1]?.headers;
      expect(headers.Authorization).to.equal(`Bearer ${fakeToken}`);
    });

    it('saves a returned token to local storage', async () => {
      const response = {
        accessToken: 'fake.access.token'
      };
      fetchMock.mock(RefreshEndpoint, response);
      const fakeToken = jwt.encode(
        {
          exp: Date.now() / 1000 - 1000
        },
        'xxx'
      );
      getItemStub.withArgs(AuthService.TokenStorageKey).returns(fakeToken);

      const auth = new AuthService();
      await auth.refresh();

      expect(setItemStub.calledWith(AuthService.TokenStorageKey, 'fake.access.token')).to.be.ok;
    });

    it('returns the provided user summery', async () => {
      const response = {
        accessToken: 'fake.access.token',
        userInfo: {
          id: 'fake-id',
          displayName: 'fake name',
          avatarUrl: 'fake://url'
        }
      };
      fetchMock.mock(RefreshEndpoint, response);
      const fakeToken = jwt.encode(
        {
          exp: Date.now() / 1000 - 1000
        },
        'xxx'
      );
      getItemStub.withArgs(AuthService.TokenStorageKey).returns(fakeToken);

      const auth = new AuthService();
      const result = await auth.refresh();

      expect(result).to.be.ok;
      expect(result).to.deep.equal(response.userInfo);
    });
  });
});
