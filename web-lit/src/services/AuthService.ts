import { EventEmitter } from 'events';
import jwt_decode from 'jwt-decode';
import { UserSummary } from 'models/UserModels';

export interface AuthResult {
  success: boolean;
  messages?: string[];
}

interface JwtDataModel {
  exp: number;
}

interface AuthEventEmitter {
  on(event: 'authChanged', listener: (isAuthenticated: boolean) => void): this;
  off(event: 'authChanged', listener: (isAuthenticated: boolean) => void): this;
}

class AuthEventEmitter extends EventEmitter {
  public emitAuthChanged(isAuthenticated: boolean): void {
    this.emit('authChanged', isAuthenticated);
  }
}

export class AuthService {
  public static readonly TokenStorageKey = 'UnpubTokenStorage';
  public static readonly UserInfoStorageKey = 'UnpubUserInfoTokenStorage';

  private _eventEmitter: AuthEventEmitter = new AuthEventEmitter();

  private _token: string | null = null;
  get token(): string | null {
    return this._token;
  }

  private _userInfo: UserSummary | null = null;
  public get userInfo(): UserSummary | null {
    return this._userInfo;
  }

  constructor() {
    const storedToken = localStorage.getItem(AuthService.TokenStorageKey);
    if (storedToken) {
      this._token = storedToken;
    }
    const storedUserInfo = localStorage.getItem(AuthService.UserInfoStorageKey);
    if (storedUserInfo) {
      try {
        this._userInfo = JSON.parse(storedUserInfo);
        // tslint:disable-next-line: no-empty
      } catch {}
    }
  }

  public isAuthenticated(): boolean {
    if (!!this._token) {
      try {
        const jwt = jwt_decode<JwtDataModel>(this._token);
        const currentTime = Date.now() / 1000;
        // @ts-ignore
        if (jwt.exp > currentTime) {
          return true;
        }
        // tslint:disable-next-line: no-empty
      } catch (error) {}
    }
    return false;
  }

  // Inform the auth service that the avatar url for a user
  // changed, it will inform any listeners
  public avatarChanged(avatarUrl: string) {
    if (this._userInfo) {
      this._userInfo.avatarUrl = avatarUrl;
      this._onAuthChanged(this._token, this._userInfo);
    }
  }

  public subscribeToAuthChanges(listener: (isAuthenticated: boolean) => void) {
    this._eventEmitter.on('authChanged', listener);
  }

  public unsubscribeToAuthChanges(listener: (isAuthenticated: boolean) => void) {
    this._eventEmitter.off('authChanged', listener);
  }

  public async login(email: string, password: string): Promise<AuthResult> {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };
    const response = await fetch('/api/auth/login', {
      body: JSON.stringify({
        email,
        password
      }),
      headers,
      method: 'POST'
    });
    if (response.ok) {
      const value = await response.json();
      this._onAuthChanged(value.accessToken, value.userInfo);
      return { success: true };
    }
    // else
    const messages = await response.json();
    return { success: false, messages };
  }

  public async register(displayName: string, email: string, password: string): Promise<AuthResult> {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };
    const response = await fetch('/api/auth/register', {
      body: JSON.stringify({
        displayName,
        email,
        password
      }),
      headers,
      method: 'POST'
    });
    if (response.ok) {
      const value = await response.json();
      this._onAuthChanged(value.accessToken, value.userInfo);
      return { success: true };
    }
    // else
    let messages: string[];
    try {
      messages = await response.json();
    } catch (e) {
      messages = ['Unexpected error. Please try again later'];
    }
    return { success: false, messages };
  }

  public async refresh(): Promise<UserSummary | null> {
    if (!this.isAuthenticated) {
      return null;
    }

    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this._token}`
    };

    const response = await fetch('/api/auth/refresh', {
      headers,
      method: 'GET'
    });

    if (response.ok) {
      const value = await response.json();
      this._onAuthChanged(value.accessToken, value.userInfo);
      return value.userInfo;
    }
    // TODO: If this fails need to report logged out

    return null;
  }

  public logout() {
    this._onAuthChanged(null, null);
  }

  public async forgotPassword(email: string): Promise<boolean> {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    const response = await fetch('/api/auth/forgotPassword', {
      headers,
      method: 'POST',
      body: JSON.stringify({
        email
      })
    });

    return response.ok;
  }

  public async resetPassword(token: string, email: string, newPassword: string): Promise<boolean> {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    const response = await fetch('/api/auth/resetPassword', {
      headers,
      method: 'POST',
      body: JSON.stringify({
        email,
        token,
        password: newPassword
      })
    });

    return response.ok;
  }

  public _onAuthChanged(accessToken: string | null, userInfo: UserSummary | null) {
    this._token = accessToken;
    this._userInfo = userInfo;
    if (accessToken) {
      localStorage.setItem(AuthService.TokenStorageKey, accessToken);
    } else {
      localStorage.removeItem(AuthService.TokenStorageKey);
    }
    if (userInfo) {
      localStorage.setItem(AuthService.UserInfoStorageKey, JSON.stringify(userInfo));
    } else {
      localStorage.removeItem(AuthService.UserInfoStorageKey);
    }

    this._eventEmitter.emitAuthChanged(!!accessToken);
  }
}
