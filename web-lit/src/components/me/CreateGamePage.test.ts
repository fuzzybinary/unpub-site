import { expect, fixture, nextFrame, html } from '@open-wc/testing';
import { Router } from '@vaadin/router';
import { GameCreateInfo, GameDetails } from 'models/Game';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import { ApiResult, UnpubService } from 'services/UnpubService';
import sinon, { SinonStubbedInstance } from 'sinon';

import './CreateGamePage';
import { EditGameControl } from './EditGameControl';

describe('CreateGamePage', () => {
  describe('rendering', () => {
    let el: HTMLElement;

    beforeEach(async () => {
      el = await fixture(html`
        <unpub-create-game></unpub-create-game>
      `);
    });

    it('should have an edit control', () => {
      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control');
      expect(edit).to.be.ok;
    });
  });

  describe('interactions', () => {
    let authServiceMock: SinonStubbedInstance<AuthService> | null = null;
    let unpubServiceMock: SinonStubbedInstance<UnpubService> | null = null;

    const provideMock = (event: Event) => {
      const ce = event as CustomEvent<RequestDependencyEventDetail>;
      if (ce.detail.type.name === 'UnpubService') {
        ce.detail.instance = unpubServiceMock;
      } else if (ce.detail.type.name === 'AuthService') {
        ce.detail.instance = authServiceMock;
      }
    };

    beforeEach(() => {
      authServiceMock = sinon.createStubInstance(AuthService);
      unpubServiceMock = sinon.createStubInstance(UnpubService);
      document.addEventListener(DependencyEventName, provideMock);
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideMock);
      sinon.restore();
    });

    it('redirects if not logged in', () => {
      authServiceMock?.isAuthenticated.returns(false);
      const goStub = sinon.stub(Router, 'go');

      const location = {
        pathname: '/me/add_game'
      };
      const el = fixture(html`
        <unpub-create-game .location=${location}></unpub-create-game>
      `);

      expect(goStub.calledWith('/login?redirect=/me/add_game')).to.be.true;
    });

    it('disables form fields on submission', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolveCreatePromise: ((result: ApiResult<GameDetails>) => void) | null = null;
      unpubServiceMock?.createGame.returns(
        new Promise(resolve => (resolveCreatePromise = resolve))
      );

      const gameCreateInfo: GameCreateInfo = {
        name: 'TestGame',
        time: '60',
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'This is my very long description'
      };

      const el = await fixture(html`
        <unpub-create-game></unpub-create-game>
      `);
      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      edit.value = gameCreateInfo;

      edit.dispatchEvent(new Event('submit'));
      await nextFrame();

      expect(edit.disabled).to.be.true;
    });

    it('calls create game with correct parameters', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolveCreatePromise: ((result: ApiResult<GameDetails>) => void) | null = null;
      unpubServiceMock?.createGame.returns(
        new Promise(resolve => (resolveCreatePromise = resolve))
      );

      const gameCreateInfo: GameCreateInfo = {
        name: 'TestGame',
        time: '60',
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'This is my very long description'
      };

      const el = await fixture(html`
        <unpub-create-game></unpub-create-game>
      `);
      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      edit.value = gameCreateInfo;

      edit.dispatchEvent(new Event('submit'));
      await nextFrame();

      expect(unpubServiceMock?.createGame.calledWith(gameCreateInfo)).to.be.true;
    });

    it('enables fields on failure', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolveCreatePromise: ((result: ApiResult<GameDetails>) => void) | null = null;
      unpubServiceMock?.createGame.returns(
        new Promise(resolve => (resolveCreatePromise = resolve))
      );

      const gameCreateInfo: GameCreateInfo = {
        name: 'TestGame',
        time: '60',
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'This is my very long description'
      };

      const el = await fixture(html`
        <unpub-create-game></unpub-create-game>
      `);
      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      edit.value = gameCreateInfo;

      edit.dispatchEvent(new Event('submit'));
      await nextFrame();

      resolveCreatePromise!({
        status: 'failed',
        data: null,
        messages: []
      });
      await nextFrame();

      expect(edit.disabled).to.be.false;
    });

    it('redirects on success', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolveCreatePromise: ((result: ApiResult<GameDetails>) => void) | null = null;
      unpubServiceMock?.createGame.returns(
        new Promise(resolve => (resolveCreatePromise = resolve))
      );
      const goStub = sinon.stub(Router, 'go');

      const gameCreateInfo: GameCreateInfo = {
        name: 'TestGame',
        time: '60',
        minAge: 8,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'This is my very long description'
      };

      const el = await fixture(html`
        <unpub-create-game></unpub-create-game>
      `);
      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      edit.value = gameCreateInfo;

      edit.dispatchEvent(new Event('submit'));
      await nextFrame();

      resolveCreatePromise!({
        status: 'ok',
        data: {
          id: 'fake-id',
          ...gameCreateInfo
        },
        messages: []
      });
      await nextFrame();

      expect(goStub.calledWith('/me/games/fake-id')).to.be.true;
    });
  });
});
