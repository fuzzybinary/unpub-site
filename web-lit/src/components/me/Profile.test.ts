import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { Router } from '@vaadin/router';
import { UserProfileDetail } from 'models/UserModels';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';
import sinon, { SinonStubbedInstance } from 'sinon';

import './Profile';

describe('Profile Home', () => {
  describe('rendering', () => {
    let mockAuth: SinonStubbedInstance<AuthService> | null = null;
    let mockService: SinonStubbedInstance<UnpubService> | null = null;

    const provideMock = (event: Event) => {
      const ce = event as CustomEvent<RequestDependencyEventDetail>;
      if (ce.detail.type.name === 'UnpubService') {
        ce.detail.instance = mockService;
      } else if (ce.detail.type.name === 'AuthService') {
        ce.detail.instance = mockAuth;
      }
    };

    beforeEach(async () => {
      mockAuth = sinon.createStubInstance(AuthService);
      mockService = sinon.createStubInstance(UnpubService);
      sinon.stub(mockService, 'config').get(() => {
        return {
          maxAvatarSizeBytes: 500000,
          maxPictureSizeBytes: 4000000
        };
      });
      document.addEventListener(DependencyEventName, provideMock);
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideMock);
      sinon.restore();
    });

    const basicUser: UserProfileDetail = {
      displayName: 'Test User',
      email: '',
      id: 'fake-id',
      feedback: []
    };

    const renderElementWithProfile = async (profile: UserProfileDetail | null) => {
      mockAuth!.isAuthenticated.returns(true);
      mockService!.fetchMe.returns(Promise.resolve(profile));

      const el = await fixture(html`
        <unpub-my-profile></unpub-my-profile>
      `);
      await nextFrame();
      return el;
    };

    it('should redirect if not authed', async () => {
      mockAuth?.isAuthenticated.returns(false);
      const goStub = sinon.stub(Router, 'go');

      const location = {
        pathname: '/profile'
      };
      const el = await fixture(html`
        <unpub-my-profile .location=${location}></unpub-my-profile>
      `);

      expect(goStub.calledWith('/login?redirect=/profile')).to.be.true;
    });

    it('should render a spinner while loading', async () => {
      mockAuth?.isAuthenticated.returns(true);
      let resolvePromise: ((profile: UserProfileDetail | null) => void) | null = null;
      mockService!.fetchMe.returns(new Promise(resolve => (resolvePromise = resolve)));

      const el = await fixture(html`
        <unpub-my-profile></unpub-my-profile>
      `);

      expect(el.shadowRoot?.querySelector('sl-spinner')).to.be.ok;
      resolvePromise!(null);
    });

    it('should render a header', async () => {
      const el = await renderElementWithProfile(null);

      const header = el.shadowRoot?.querySelector('h1');
      expect(header).to.be.ok;
      expect(header?.innerText).to.equal('My Profile');
    });

    it('should render an placeholder avatar image', async () => {
      const el = await renderElementWithProfile(basicUser);

      const avatar = el.shadowRoot?.querySelector('.sidebar__avatar');
      expect(avatar).to.be.ok;
      const img = avatar?.querySelector('unpub-editable-hero');
      expect(img).to.be.ok;
      expect(img?.getAttribute('imageSrc')).to.equal('/images/avatar_placeholder.png');
    });

    it('should render actual avatar if provided', async () => {
      const el = await renderElementWithProfile({
        avatarUrl: '/images/avatar-xyz.png',
        displayName: 'Test User',
        email: '',
        id: 'fake-id',
        feedback: []
      });

      const avatar = el.shadowRoot?.querySelector('.sidebar__avatar');
      const img = avatar?.querySelector('unpub-editable-hero');
      expect(img?.getAttribute('imageSrc')).to.equal('/images/avatar-xyz.png');
    });

    it('should render display name', async () => {
      const el = await renderElementWithProfile(basicUser);

      const sidebar = el.shadowRoot?.querySelector('.sidebar');
      const header = sidebar?.querySelector('h3');
      expect(header?.innerText).to.equal('Test User');
    });

    it('should render an add games button', async () => {
      const el = await renderElementWithProfile(basicUser);

      const addButton = el.shadowRoot?.getElementById('addGameButton');
      expect(addButton).to.be.ok;
    });

    it('should redirect to add game when pressing the button', async () => {
      const el = await renderElementWithProfile(basicUser);
      const goStub = sinon.stub(Router, 'go');

      const addButton = el.shadowRoot?.getElementById('addGameButton');
      addButton?.click();

      expect(goStub.calledWith('/me/add_game/')).to.be.true;
    });

    it('should render no games if none exist', async () => {
      const el = await renderElementWithProfile(basicUser);

      const games = el.shadowRoot?.querySelector('.games');
      expect(games).to.be.ok;
      const innerText = games?.querySelector('.games__info');
      expect(innerText).to.be.ok;
      expect(innerText?.innerHTML).to.equal('No Games');
    });

    it('should render games', async () => {
      const profile: UserProfileDetail = {
        ...basicUser,
        games: [
          { id: 'game-1', name: 'Game 1' },
          { id: 'game-2', name: 'Game 2' },
          { id: 'game-3', name: 'Game 3' }
        ]
      };
      const el = await renderElementWithProfile(profile);

      const games = el.shadowRoot?.querySelector('.games');
      expect(games).to.be.ok;
      const gamesList = games?.querySelector('.games__list');
      expect(gamesList).to.be.ok;
      expect(gamesList?.children.length).to.equal(3);
      for (let i = 0; i < gamesList!.children.length; i++) {
        const displayGame = gamesList!.children[i];
        const profileGame = profile!.games![i];
        expect(displayGame.textContent?.trim()).to.equal(profileGame.name);
        expect(displayGame.tagName.toLowerCase()).to.equal('a');
        expect(displayGame.getAttribute('href')).to.equal(`/me/games/${profileGame.id}`);
      }
    });
  });
});
