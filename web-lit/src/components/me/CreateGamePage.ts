import { Router } from '@vaadin/router';
import { LitElement } from 'lit';
import { html } from 'lit-html';
import { customElement, property, query } from 'lit/decorators.js';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';
import { EditGameControl } from './EditGameControl';

import './EditGameControl';

@customElement('unpub-create-game')
class CreateGamePage extends diConsumer(LitElement) {
  @property({ attribute: false }) private _performingOperation: boolean = false;

  @query('#editGameControl') private _editGameControl!: EditGameControl;

  private _authService: AuthService | null = null;
  private _unpubService: UnpubService | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
    this._unpubService = this.resolve(UnpubService);

    if (!this._authService?.isAuthenticated()) {
      const location = (this as any).location as Router.Location;
      if (location) {
        const path = (this as any).location.pathname;
        Router.go(`/login?redirect=${path}`);
      } else {
        Router.go('/login');
      }
    }
  }

  public render() {
    return html`
      <h2>Create New Game</h2>
      <unpub-edit-game-control
        id="editGameControl"
        ?disabled=${this._performingOperation}
        @submit=${this._onSubmit}
      ></unpub-edit-game-control>
    `;
  }

  private async _onSubmit(e: Event) {
    e.preventDefault();

    this._performingOperation = true;

    const createInfo = this._editGameControl.value;
    if (createInfo) {
      if (createInfo.avatar?.startsWith('data:')) {
        createInfo.avatar = createInfo.avatar?.replace(/data:.*,/, '');
      }
      const result = await this._unpubService?.createGame(createInfo);
      if (result?.status === 'ok' && result.data?.id) {
        Router.go(`/me/games/${result.data.id}`);
      } else {
        // showSnackbar({
        //   container: document.body,
        //   template: html`
        //     <span>Failed to create game. Please try again later.</span>
        //   `,
        //   ...defaultSnackbarConfig
        // });
      }
    }

    this._performingOperation = false;
  }
}
