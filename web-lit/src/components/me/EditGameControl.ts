import { SlInput, SlTextarea } from '@shoelace-style/shoelace';
import { EditableHeroImage } from 'components/shared/EditableHeroImage';
import { Rectangle } from 'components/shared/ImageCropper';
import 'components/shared/TagEditor';
import { TagEditor } from 'components/shared/TagEditor';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { GameCreateInfo } from 'models/Game';
import { diConsumer } from 'providers';
import { UnpubService } from 'services/UnpubService';
import { clusterStyles, stackStyles } from 'styles/styles';

@customElement('unpub-edit-game-control')
export class EditGameControl extends diConsumer(LitElement) {
  static get styles() {
    return [
      stackStyles,
      clusterStyles,
      css`
        #gameHero {
          overflow: hidden;
          width: 200px;
          height: 200px;
        }

        .createGameForm__spinner {
          --progress-spinner-size: 30px;
        }
      `
    ];
  }
  @property({ type: Boolean }) public disabled: boolean = false;
  @property({ type: Boolean }) public showCancel: boolean = false;

  private _unpubService: UnpubService | null = null;

  @query('#gameHero') private _gameHeroField!: EditableHeroImage;
  @query('#nameField') private _nameField!: SlInput;
  @query('#timeField') private _timeField!: SlInput;
  @query('#ageField') private _ageField!: SlInput;
  @query('#minPlayersField') private _minPlayersField!: SlInput;
  @query('#maxPlayersField') private _maxPlayersField!: SlInput;
  @query('#mechanismsField') private _mechanismsField!: TagEditor;
  @query('#descriptionField') private _descriptionField!: SlTextarea;

  private _gameInfo: GameCreateInfo | null = null;
  @property({ attribute: false }) public get value(): GameCreateInfo | null {
    return this._gameInfo;
  }
  public set value(v: GameCreateInfo | null) {
    const oldValue = this._gameInfo;
    this._gameInfo = Object.assign({}, v);
    this._avatarImage = v?.avatar;
    this.requestUpdate('value', oldValue);
  }

  @property() private _avatarImage?: string;

  public connectedCallback() {
    super.connectedCallback();

    this._unpubService = this.resolve(UnpubService);
  }

  public render() {
    return html`
      <sl-form id="createGameForm" @sl-submit=${this._onSubmit}>
        <div class="stack-l">
          <label for="gameHero">Game Hero Image</label>
          <canvas id="scratchCanvas" style="display: none;"></canvas>
          <unpub-editable-hero
            id="gameHero"
            name="gameHero"
            imageSrc=${this._avatarImage ?? '/images/game_placeholder.jpg'}
            maxSize=${this._unpubService!.config.maxAvatarSizeBytes}
            .updateImage=${this._updateImage.bind(this)}
          ></unpub-editable-hero>
          <sl-input
            id="nameField"
            name="gameName"
            required
            label="Game Name"
            ?disabled=${this.disabled}
            value=${this._gameInfo?.name ?? ''}
            @sl-change=${this._onFieldChange}
          ></sl-input>
          <sl-input
            id="timeField"
            name="gameTime"
            required
            label="Game Time (in mintues)"
            ?disabled=${this.disabled}
            value=${this._gameInfo?.time ?? ''}
            @sl-change=${this._onFieldChange}
          >
            <sl-icon slot="prefix" name="clock"></sl-icon>
          </sl-input>
          <sl-input
            id="ageField"
            name="minAge"
            required
            label="Min Age"
            type="number"
            ?disabled=${this.disabled}
            value=${this._gameInfo?.minAge ?? ''}
            @sl-change=${this._onFieldChange}
          >
            <img width="24" slot="prefix" src="/images/noun_family.png" />
          </sl-input>
          <sl-input
            id="minPlayersField"
            name="minPlayers"
            required
            label="Min Players"
            type="number"
            ?disabled=${this.disabled}
            value=${this._gameInfo?.minPlayers ?? ''}
            @sl-change=${this._onFieldChange}
          >
            <sl-icon slot="prefix" name="person-dash-fill"></sl-icon>
          </sl-input>
          <sl-input
            id="maxPlayersField"
            name="maxPlayers"
            required
            label="Max Players"
            type="number"
            ?disabled=${this.disabled}
            value=${this._gameInfo?.maxPlayers ?? ''}
            @sl-change=${this._onFieldChange}
          >
            <sl-icon slot="prefix" name="person-plus-fill"></sl-icon>
          </sl-input>
          <unpub-tag-editor
            id="mechanismsField"
            label="Mechanisms"
            .value=${this._gameInfo?.mechanisms ?? []}
            ?disabled=${this.disabled}
            @sl-change=${this._onFieldChange}
          >
          </unpub-tag-editor>
          <sl-textarea
            id="descriptionField"
            name="description"
            required
            label="Game Description"
            resize="auto"
            ?disabled=${this.disabled}
            value=${this._gameInfo?.description ?? ''}
            @sl-change=${this._onFieldChange}
          >
          </sl-textarea>
          <div class="cluster-l">
            <div>
              <sl-button type="primary" submit ?loading=${this.disabled}>Submit</sl-button>
              ${this.showCancel
                ? html`
                    <sl-button name="cancel" ?disabled=${this.disabled} @click=${this._onCancel}>
                      Cancel
                    </sl-button>
                  `
                : ''}
            </div>
          </div>
        </div>
      </sl-form>
    `;
  }

  private _onFieldChange() {
    try {
      this._gameInfo = {
        name: this._nameField.value,
        avatar: this._avatarImage,
        avatarSrcRect: this._avatarImage ? { top: 0, left: 0, width: 200, height: 200 } : undefined,
        time: this._timeField.value,
        minAge: Number(this._ageField.value),
        minPlayers: Number(this._minPlayersField.value),
        maxPlayers: Number(this._maxPlayersField.value),
        mechanisms: this._mechanismsField.value,
        description: this._descriptionField.value
      };
    } catch {
      this._gameInfo = null;
    }
  }

  // tslint:disable-next-line: prettier
  private async _updateImage(
    cropRect: Rectangle,
    image: File,
    buffer: string
  ): Promise<string | undefined> {
    const canvas = this.shadowRoot?.getElementById('scratchCanvas') as HTMLCanvasElement;
    const imageElement = document.createElement('img');
    imageElement.src = buffer;

    canvas.width = this._gameHeroField.clientWidth;
    canvas.height = this._gameHeroField.clientHeight;
    canvas
      .getContext('2d')
      ?.drawImage(
        imageElement,
        cropRect.left,
        cropRect.top,
        cropRect.width,
        cropRect.height,
        0,
        0,
        canvas.width,
        canvas.height
      );
    this._avatarImage = canvas.toDataURL('image/jpeg');
    this.requestUpdate('gameInfo');
    return;
  }

  private _onSubmit(e: Event) {
    e.preventDefault();

    if (this._avatarImage) {
      this._gameInfo!.avatar = this._avatarImage;
      this._gameInfo!.avatarSrcRect = { top: 0, left: 0, width: 200, height: 200 };
    }
    this.dispatchEvent(new Event('submit'));
  }

  private _onCancel(e: Event) {
    this.dispatchEvent(new Event('cancelediting'));
  }
}
