import { expect, fixture, nextFrame } from '@open-wc/testing';
import { Router } from '@vaadin/router';
import { ConfirmDialog } from 'components/shared/ConfirmDialog';
import { GameInfo } from 'components/shared/GameInfo';
import { html } from 'lit/static-html.js';
import { GameCreateInfo, GameDetails, GameDetailsPrivate } from 'models/Game';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import { ApiResult, UnpubService } from 'services/UnpubService';
import sinon, { SinonStubbedInstance } from 'sinon';
import { EditGameControl } from './EditGameControl';

import './PrivateGameDetails';

describe('PrivateGameDetails', () => {
  let authServiceMock: SinonStubbedInstance<AuthService> | null = null;
  let unpubServiceMock: SinonStubbedInstance<UnpubService> | null = null;

  const provideMock = (event: Event) => {
    const ce = event as CustomEvent<RequestDependencyEventDetail>;
    if (ce.detail.type.name === 'UnpubService') {
      ce.detail.instance = unpubServiceMock;
    } else if (ce.detail.type.name === 'AuthService') {
      ce.detail.instance = authServiceMock;
    }
  };

  beforeEach(() => {
    authServiceMock = sinon.createStubInstance(AuthService);
    unpubServiceMock = sinon.createStubInstance(UnpubService);
    document.addEventListener(DependencyEventName, provideMock);
  });

  afterEach(() => {
    document.removeEventListener(DependencyEventName, provideMock);
    sinon.restore();
  });

  describe('Forbidden behaviors', () => {
    it('should redirect if not authed', async () => {
      authServiceMock?.isAuthenticated.returns(false);
      const goStub = sinon.stub(Router, 'go');

      const location = {
        pathname: '/me/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-private-game-details .location=${location}></unpub-private-game-details>
      `);

      expect(goStub.calledWith('/login?redirect=/me/fake-id')).to.be.true;
    });

    it('should display a spinner while loading', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolvePromise: ((result: ApiResult<GameDetailsPrivate>) => void) | null = null;
      unpubServiceMock?.fetchPrivateGame.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const location = {
        pathname: '/me/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-private-game-details .location=${location}></unpub-private-game-details>
      `);
      const spinner = el.shadowRoot?.querySelector('sl-spinner');
      expect(spinner).to.be.ok;
    });

    it('should display 404 if not authorized', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolvePromise: ((result: ApiResult<GameDetailsPrivate>) => void) | null = null;
      unpubServiceMock?.fetchPrivateGame.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const location = {
        pathname: '/me/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-private-game-details .location=${location}></unpub-private-game-details>
      `);

      resolvePromise!({
        status: 'forbidden',
        data: null,
        messages: []
      });
      await nextFrame();

      const fourOhfour = el.shadowRoot?.querySelector('unpub-four-oh-four');
      expect(fourOhfour).to.be.ok;
    });
  });

  describe('Rendering', () => {
    let el: Element;
    const gameDetails: GameDetailsPrivate = {
      id: 'fake-id',
      name: 'My Awesome Game',
      time: '20-30',
      minAge: 8,
      minPlayers: 3,
      maxPlayers: 6,
      mechanisms: [],
      description: 'The description of the game that is awesome.',
      feedback: []
    };

    beforeEach(async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolveFetchPromise: ((game: ApiResult<GameDetailsPrivate>) => void) | null = null;
      unpubServiceMock?.fetchPrivateGame.returns(
        new Promise(resolve => (resolveFetchPromise = resolve))
      );

      const location = {
        params: {
          id: 'fake-id'
        }
      };
      el = await fixture(
        html`
          <!-- @ts-ignore -->
          <unpub-private-game-details .location=${location}></unpub-private-game-details>
        `
      );

      resolveFetchPromise!({
        status: 'ok',
        data: gameDetails,
        messages: []
      });
      await nextFrame();
    });

    it('Renders the game title as the header', async () => {
      const header = el.shadowRoot?.querySelector('h1');
      expect(header).to.be.ok;
      expect(header?.innerText.trim()).to.equal('My Awesome Game');
    });

    it('renders game info', () => {
      const info = el.shadowRoot?.querySelector('unpub-game-info') as GameInfo;
      expect(info).to.be.ok;
      expect(info.gameInfo).to.equal(gameDetails);
    });

    it('Renders actions', async () => {
      const deleteButton = el.shadowRoot?.querySelector('[name="delete"]');
      expect(deleteButton).to.be.ok;

      const editButton = el.shadowRoot?.querySelector('[name="edit"]');
      expect(editButton).to.be.ok;
    });

    it('goes to editing hash on edit clicked', async () => {
      const goStub = sinon.stub(Router, 'go');
      const editButton = el.shadowRoot?.querySelector('[name="edit"]') as HTMLElement;

      editButton?.click();
      await nextFrame();

      expect(goStub.calledOnce).to.be.true;
      const locationArg = goStub.getCall(0).args[0] as Location;
      expect(locationArg.hash).to.equal('#editing');
    });

    it('displays a confirm dialog on clicking delete', async () => {
      const deleteButton = el.shadowRoot?.querySelector('[name="delete"]') as HTMLElement;

      deleteButton?.click();
      await nextFrame();

      const dialog = el.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
      expect(dialog).to.be.ok;
      expect(dialog.open).to.be.true;
    });

    it('clicking ok calls to delete the game', async () => {
      let resolvePromise: ((result: ApiResult<void>) => void) | null = null;
      unpubServiceMock?.deleteGame.returns(new Promise(resolve => (resolvePromise = resolve)));

      const actionBar = el.shadowRoot?.querySelector('.actionBar');
      const deleteButton = actionBar?.querySelector('[name="delete"]') as HTMLElement;

      deleteButton?.click();
      await nextFrame();

      const dialog = el.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
      dialog.okButton.click();
      await nextFrame();

      expect(unpubServiceMock?.deleteGame.called).to.be.true;
    });

    it('success on delete redirects back', async () => {
      let resolvePromise: ((result: ApiResult<void>) => void) | null = null;
      unpubServiceMock?.deleteGame.returns(new Promise(resolve => (resolvePromise = resolve)));
      const backStub = sinon.stub(history, 'back');

      const actionBar = el.shadowRoot?.querySelector('.actionBar');
      const deleteButton = actionBar?.querySelector('[name="delete"]') as HTMLElement;

      deleteButton?.click();
      await nextFrame();

      const dialog = el.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
      dialog.okButton.click();
      resolvePromise!({
        status: 'ok',
        data: null,
        messages: []
      });
      await nextFrame();

      expect(backStub.called).to.be.true;
    });
  });

  describe('editing', () => {
    let el: Element;
    const gameDetails: GameDetailsPrivate = {
      id: 'fake-id',
      name: 'My Awesome Game',
      time: '20-30',
      minAge: 8,
      minPlayers: 3,
      maxPlayers: 6,
      mechanisms: [],
      description: 'The description of the game that is awesome.',
      feedback: []
    };

    beforeEach(async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolveFetchPromise: ((game: ApiResult<GameDetailsPrivate>) => void) | null = null;
      unpubServiceMock?.fetchPrivateGame.returns(
        new Promise(resolve => (resolveFetchPromise = resolve))
      );

      const location = {
        params: {
          id: 'fake-id'
        },
        hash: '#editing'
      };
      el = await fixture(
        html`
          <!-- @ts-ignore -->
          <unpub-private-game-details .location=${location}></unpub-private-game-details>
        `
      );

      resolveFetchPromise!({
        status: 'ok',
        data: gameDetails,
        messages: []
      });
      await nextFrame();
    });

    afterEach(() => {
      sinon.reset();
    });

    it('shows the editing control on editing', () => {
      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      expect(edit).to.be.ok;
      expect(edit.value).to.deep.equal(gameDetails);
    });

    it('calls update with value on submit', async () => {
      let resolveEditPromise: ((game: ApiResult<GameDetails>) => void) | null = null;
      unpubServiceMock?.updateGame.returns(new Promise(resolve => (resolveEditPromise = resolve)));

      let edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      const newValue: GameCreateInfo = {
        name: 'Modified Name',
        time: '100-120',
        minAge: 3,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'My description'
      };
      edit.value = newValue;
      edit.dispatchEvent(new Event('submit'));
      await nextFrame();

      edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;

      expect(edit.disabled).to.be.true;
      expect(unpubServiceMock?.updateGame.called).to.be.true;
      const call = unpubServiceMock?.updateGame.firstCall;
      expect(call?.args[0]).to.equal(gameDetails.id);
      expect(call?.args[1]).to.deep.equal(newValue);
    });

    it('shows errors on update error', async () => {
      let resolveEditPromise: ((game: ApiResult<GameDetails>) => void) | null = null;
      unpubServiceMock?.updateGame.returns(new Promise(resolve => (resolveEditPromise = resolve)));

      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      const newValue: GameCreateInfo = {
        name: 'Modified Name',
        time: '100-120',
        minAge: 3,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'My description'
      };
      edit.value = newValue;
      edit.dispatchEvent(new Event('submit'));
      await nextFrame();
      resolveEditPromise!({
        status: 'failed',
        data: null,
        messages: ['Failure 1', 'Failure 2']
      });
      await nextFrame();

      expect(edit.disabled).to.be.false;
      const errors = el.shadowRoot?.querySelector('.errorMessages');
      expect(errors).to.be.ok;
      expect(errors?.childElementCount).to.equal(2);
      expect(errors?.children[0].textContent).to.equal('Failure 1');
      expect(errors?.children[1].textContent).to.equal('Failure 2');
    });

    it('stops editing on submit', async () => {
      const goStub = sinon.stub(Router, 'go');

      let resolveEditPromise: ((game: ApiResult<GameDetails>) => void) | null = null;
      unpubServiceMock?.updateGame.returns(new Promise(resolve => (resolveEditPromise = resolve)));

      const edit = el.shadowRoot?.querySelector('unpub-edit-game-control') as EditGameControl;
      const newValue: GameCreateInfo = {
        name: 'Modified Name',
        time: '100-120',
        minAge: 3,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'My description'
      };
      edit.value = newValue;
      edit.dispatchEvent(new Event('submit'));
      await nextFrame();
      resolveEditPromise!({
        status: 'ok',
        data: null,
        messages: []
      });
      await nextFrame();

      expect(goStub.called).to.be.true;
      const locationArg = goStub.getCall(0).args[0] as Location;
      expect(locationArg.hash).to.not.be.ok;
    });
  });
});
