import { Router } from '@vaadin/router';
import { ConfirmDialog } from 'components/shared/ConfirmDialog';
import 'components/shared/FeedbackSummaryControl';
import 'components/shared/GameInfo';
import { LoadResult, unpubLoad } from 'components/shared/Loader';
import { LitElement } from 'lit';
import { html } from 'lit-html';
import { customElement, query, state } from 'lit/decorators.js';
import { GameDetailsPrivate } from 'models/Game';
import { diConsumer, withLocation } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';
import { clusterStyles, stackStyles } from 'styles/styles';
import { EditGameControl } from './EditGameControl';

import styles from './PrivateGameDetails.scss';

@customElement('unpub-private-game-details')
class PrivateGameDetails extends withLocation(diConsumer(LitElement)) {
  static get styles() {
    return [stackStyles, clusterStyles, styles];
  }
  private _unpubService: UnpubService | null = null;
  private _authService: AuthService | null = null;
  private _id: string | null = null;

  @state() private _editing = false;
  @state() private _processingOperation = false;
  @state() private _lastErrors: string[] = [];
  @state() private _game: GameDetailsPrivate | null = null;

  @query('#editingControl') private _editingControl!: EditGameControl;

  public connectedCallback() {
    super.connectedCallback();

    if (this.location?.hash === '#editing') {
      this._editing = true;
    } else {
      this._editing = false;
      this._game = null;
    }

    this._authService = this.resolve(AuthService);
    this._unpubService = this.resolve(UnpubService);
    if (!this._authService?.isAuthenticated()) {
      if (this.location) {
        Router.go(`/login?redirect=${this.location.pathname}`);
      } else {
        Router.go('/login');
      }
    }

    this._id = (this.location?.params.id as string) || null;
  }

  public render() {
    return unpubLoad(
      this._load.bind(this),
      this._editing ? this._renderEditing() : this._renderLoaded(),
      this._game === null
    );
  }

  private _renderLoaded() {
    return html`
      <div class="stack-l content">
        <div class="cluster-l" style="--cluster-margin: var(--s-2)">
          <div>
            <h1>${this._game?.name}</h1>
            <sl-button circle id="editButton" name="edit" @click=${this._onEditClicked}>
              <sl-icon name="pencil-fill"></sl-icon>
            </sl-button>
            <sl-button circle id="deleteButton" name="delete" @click=${this._onDeleteClicked}>
              <sl-icon name="trash-fill"></sl-icon>
            </sl-button>
          </div>
        </div>
        <unpub-game-info .gameInfo=${this._game}></unpub-game-info>
        <h2>Recent Feedback</h2>
          ${
            this._game?.feedback.length
              ? html`
                  <unpub-feedback-summary .feedback=${this._game.feedback}></unpub-feedback-summary>
                `
              : 'No Recent Feedback'
          }
        </table>
      </div>
      <unpub-confirm-dialog 
        id="confirmDialog"
        label="Delete ${this._game?.name}?"
        .operation=${this._deleteGame.bind(this)} 
        @confirmcomplete=${this._deleteConfirmed}
      >
        Are you sure you want to delete <b>${this._game?.name}</b> 
        and all of it's reviews and content?
      </unpub-confirm-dialog>
    `;
  }

  private _renderEditing() {
    return html`
      <h1>Editing ${this._game?.name}</h1>
      <unpub-edit-game-control
        id="editingControl"
        .value=${this._game}
        showCancel
        ?disabled=${this._processingOperation}
        @cancelediting=${this._onCancelEditing}
        @submit=${this._onEditingSubmit}
      ></unpub-edit-game-control>
      ${this._lastErrors
        ? html`
            <div class="errorMessages">
              ${this._lastErrors.map(x => {
                return html`
                  <span>${x}</span>
                `;
              })}
            </div>
          `
        : ''}
    `;
  }

  private async _load() {
    if (!this._id) {
      return LoadResult.noData;
    }

    const data = await this._unpubService?.fetchPrivateGame(this._id);
    if (data?.status === 'ok') {
      this._game = data?.data ?? null;
    }

    if (this._game) {
      return LoadResult.ok;
    }

    return LoadResult.noData;
  }

  private _onDeleteClicked() {
    const dialog = this.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
    dialog.show();
  }

  private _onEditClicked() {
    Router.go({
      pathname: this.location!.pathname,
      hash: '#editing'
    });
  }

  private async _onEditingSubmit() {
    this._processingOperation = true;
    const value = this._editingControl.value;
    if (value) {
      if (value.avatar?.startsWith('data:')) {
        value.avatar = value.avatar?.replace(/data:.*,/, '');
      } else {
        // Didn't change or remains empty
        delete value.avatar;
      }
      const result = await this._unpubService!.updateGame(this._game!.id, value);
      if (result.status === 'ok') {
        Object.assign(this._game, result.data);
        Router.go({
          pathname: this.location!.pathname
        });
      } else {
        this._lastErrors = result.messages;
      }
    }
    this._processingOperation = false;
  }

  private _onCancelEditing() {
    Router.go({
      pathname: this.location!.pathname
    });
  }

  private async _deleteGame() {
    const result = await this._unpubService!.deleteGame(this._game!.id);
    if (result.status === 'ok') {
      return null;
    }

    return result.messages;
  }

  private _deleteConfirmed() {
    history.back();
  }
}
