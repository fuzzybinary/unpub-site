import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { SlButton, SlInput, SlTextarea } from '@shoelace-style/shoelace';
import { EditableHeroImage } from 'components/shared/EditableHeroImage';
import { TagEditor } from 'components/shared/TagEditor';
import { GameCreateInfo } from 'models/Game';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';
import sinon, { SinonStubbedInstance } from 'sinon';

import { EditGameControl } from './EditGameControl';

describe('EditGameControl', () => {
  let authServiceMock: SinonStubbedInstance<AuthService> | null = null;
  let unpubServiceMock: SinonStubbedInstance<UnpubService> | null = null;

  const provideMock = (event: Event) => {
    const ce = event as CustomEvent<RequestDependencyEventDetail>;
    if (ce.detail.type.name === 'UnpubService') {
      ce.detail.instance = unpubServiceMock;
    } else if (ce.detail.type.name === 'AuthService') {
      ce.detail.instance = authServiceMock;
    }
  };

  beforeEach(() => {
    authServiceMock = sinon.createStubInstance(AuthService);
    unpubServiceMock = sinon.createStubInstance(UnpubService);
    sinon.stub(unpubServiceMock, 'config').get(() => {
      return {
        maxAvatarSizeBytes: 500000,
        maxPictureSizeBytes: 4000000
      };
    });
    document.addEventListener(DependencyEventName, provideMock);
  });

  afterEach(() => {
    document.removeEventListener(DependencyEventName, provideMock);
    sinon.restore();
  });

  describe('rendering', () => {
    let el: HTMLElement;

    beforeEach(async () => {
      el = await fixture(html`
        <unpub-edit-game-control></unpub-edit-game-control>
      `);
    });

    it('should have a form', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      expect(form).to.be.ok;
    });

    it('should render a name field', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const nameField = form?.querySelector('[name="gameName"]') as SlInput;

      expect(nameField).to.be.ok;
      expect(nameField.required).to.be.true;
    });

    it('should render an editable hero', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const heroField = form?.querySelector(
        'unpub-editable-hero[name="gameHero"]'
      ) as EditableHeroImage;

      expect(heroField).to.be.ok;
    });

    it('should render a time field', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const timeField = form?.querySelector('[name="gameTime"]') as SlInput;

      expect(timeField).to.be.ok;
      expect(timeField.required).to.be.true;
    });

    it('should render a min age field', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const ageField = form?.querySelector('[name="minAge"]') as SlInput;

      expect(ageField).to.be.ok;
      expect(ageField.required).to.be.true;
    });

    it('should render a min / max players fields', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const minPlayers = form?.querySelector('[name="minPlayers"]') as SlInput;
      const maxPlayers = form?.querySelector('[name="maxPlayers"]') as SlInput;

      expect(minPlayers).to.be.ok;
      expect(minPlayers.required).to.be.true;
      expect(minPlayers.type).to.equal('number');

      expect(maxPlayers).to.be.ok;
      expect(maxPlayers.required).to.be.true;
      expect(maxPlayers.type).to.equal('number');
    });

    it('should render a mechanisms field', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const mechanismsField = form?.querySelector('unpub-tag-editor') as TagEditor;

      expect(mechanismsField).to.be.ok;
    });

    it('should render a description field', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const descriptionField = form?.querySelector('[name="description"]') as SlTextarea;

      expect(descriptionField).to.be.ok;
      expect(descriptionField.required).to.be.true;
    });

    it('should render a submit button', () => {
      const form = el.shadowRoot?.getElementById('createGameForm');
      const submitButton = form?.querySelector('sl-button[type="primary"]') as SlButton;

      expect(submitButton).to.be.ok;
      expect(submitButton.submit).to.be.true;
    });
  });

  describe('interactions', () => {
    it('disables form fields on disabled', async () => {
      const el = await fixture(html`
        <unpub-edit-game-control disabled></unpub-edit-game-control>
      `);

      const form = el.shadowRoot?.getElementById('createGameForm') as HTMLElement;
      const nameField = form?.querySelector('[name="gameName"]') as SlInput;
      expect(nameField.disabled).to.be.true;

      const timeField = form?.querySelector('[name="gameTime"]') as SlInput;
      expect(timeField.disabled).to.be.true;

      const ageField = form?.querySelector('[name="minAge"]') as SlInput;
      expect(ageField.disabled).to.be.true;

      const minPlayersField = form?.querySelector('[name="minPlayers"]') as SlInput;
      expect(minPlayersField.disabled).to.be.true;

      const maxPlayersField = form?.querySelector('[name="maxPlayers"]') as SlInput;
      expect(maxPlayersField.disabled).to.be.true;

      const mechanismsField = form?.querySelector('unpub-tag-editor') as TagEditor;
      expect(mechanismsField.disabled).to.be.true;

      const descriptionField = form?.querySelector('[name="description"]') as SlInput;
      expect(descriptionField.disabled).to.be.true;

      const button = form.querySelector('sl-button[type="primary"]') as SlButton;
      expect(button.loading).to.be.true;
    });

    it('fills in fields with supplied game', async () => {
      const gameInfo: GameCreateInfo = {
        name: 'The name of the game',
        time: '20 min',
        minAge: 5,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: ['Mechanism 1', 'Mechanism 2'],
        description: 'A description of the game that is very long and you should love it.'
      };
      const el = await fixture(html`
        <unpub-edit-game-control .value=${gameInfo}></unpub-edit-game-control>
      `);

      const form = el.shadowRoot?.getElementById('createGameForm') as HTMLElement;
      const nameField = form?.querySelector('[name="gameName"]') as SlInput;
      expect(nameField.value).to.equal(gameInfo.name);

      const timeField = form?.querySelector('[name="gameTime"]') as SlInput;
      expect(timeField.value).to.equal(gameInfo.time);

      const ageField = form?.querySelector('[name="minAge"]') as SlInput;
      expect(ageField.value).to.equal(gameInfo.minAge.toString());

      const minPlayersField = form?.querySelector('[name="minPlayers"]') as SlInput;
      expect(minPlayersField.value).to.equal(gameInfo.minPlayers.toString());

      const maxPlayersField = form?.querySelector('[name="maxPlayers"]') as SlInput;
      expect(maxPlayersField.value).to.equal(gameInfo.maxPlayers.toString());

      const mechanismsField = form?.querySelector('unpub-tag-editor') as TagEditor;
      expect(mechanismsField.value).to.equal(gameInfo.mechanisms);

      const descriptionField = form?.querySelector('[name="description"]') as SlInput;
      expect(descriptionField.value).to.equal(gameInfo.description);
    });

    it('value reflects modified values', async () => {
      const gameInfo: GameCreateInfo = {
        name: 'The name of the game',
        time: '20 min',
        minAge: 5,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'A description of the game that is very long and you should love it.'
      };
      const el = (await fixture(html`
        <unpub-edit-game-control .value=${gameInfo}></unpub-edit-game-control>
      `)) as EditGameControl;

      const form = el.shadowRoot?.getElementById('createGameForm') as HTMLElement;
      const nameField = form?.querySelector('[name="gameName"]') as SlInput;
      nameField.value = 'A changed name';
      nameField.dispatchEvent(new Event('sl-change'));

      expect(el.value?.name).to.equal('A changed name');
    });

    it('sends submit event when submitted', async () => {
      const onSubmitStub = sinon.stub();
      const gameInfo: GameCreateInfo = {
        name: 'The name of the game',
        time: '20 min',
        minAge: 5,
        minPlayers: 2,
        maxPlayers: 10,
        mechanisms: [],
        description: 'A description of the game that is very long and you should love it.'
      };
      const el = (await fixture(html`
        <!-- @ts-ignore -->
        <unpub-edit-game-control
          .value=${gameInfo}
          @submit=${onSubmitStub}
        ></unpub-edit-game-control>
      `)) as EditGameControl;

      const submitButton = el.shadowRoot?.querySelector('[type="primary"]') as SlButton;
      submitButton?.click();
      await nextFrame();

      expect(onSubmitStub?.called).to.be.true;
    });
  });
});
