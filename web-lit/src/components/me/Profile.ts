import { Router } from '@vaadin/router';
import { css, html, LitElement } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { UserProfileDetail } from 'models/UserModels';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';

import 'components/shared/EditableHeroImage';
import { SourceChange } from 'components/shared/EditableHeroImage';
import 'components/shared/FeedbackSummaryControl';
import { Rectangle } from 'components/shared/ImageCropper';
import { LoadResult, unpubLoad } from 'components/shared/Loader';
import { clusterStyles, sidebarStyles, stackStyles } from 'styles/styles';

@customElement('unpub-my-profile')
export class Profile extends diConsumer(LitElement) {
  static get styles() {
    return [
      clusterStyles,
      sidebarStyles,
      stackStyles,
      css`
        .content {
          padding: var(--s-1);
        }

        .sidebar__avatar {
          overflow: hidden;
          background-color: #284b8d;
          width: 200px;
          height: 200px;
        }

        .games__list {
          list-style-type: none;
          padding-inline-start: 0px;
        }

        .games__list li {
          list-style: none;
          margin-top: var(--s-2);
          padding: var(--s-3);
          background: var(--unpub-shaded);
          border: thin solid var(--unpub-shaded-border);
        }

        .games__list li:hover {
          background: whitesmoke;
        }

        .actionButton {
          --button-fab-size: 2.8ch;
        }
      `
    ];
  }

  private _authService: AuthService | null = null;
  private _unpubService: UnpubService | null = null;

  @state() private _me: UserProfileDetail | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
    this._unpubService = this.resolve(UnpubService);

    if (!this._authService?.isAuthenticated()) {
      const location = (this as any).location as Router.Location;
      Router.go(`/login?redirect=${location.pathname}`);
    } else {
      this.load();
    }
  }

  public async load() {
    this._me = await this._unpubService!.fetchMe();

    return this._me ? LoadResult.ok : LoadResult.error;
  }

  public render() {
    return html`
      <h1>My Profile</h1>
      ${unpubLoad(this.load.bind(this), this._me && this.renderProfile())}
    `;
  }

  private get avatarUrl(): string {
    return this._me?.avatarUrl ? this._me.avatarUrl : '/images/avatar_placeholder.png';
  }

  private _onAddGameClick(e: Event) {
    Router.go('/me/add_game/');
  }

  private _avatarImageChanged(e: CustomEvent<SourceChange>) {
    const oldProp = this.avatarUrl;
    this._me!.avatarUrl = e.detail.newUrl;
    this.requestUpdate('avatarUrl', oldProp);
  }

  private async _updateImage(
    rect: Rectangle,
    image: File,
    buffer: string
  ): Promise<string | undefined> {
    const newUrl = await this._unpubService?.postAvatar(rect, image);
    return newUrl;
  }

  private renderProfile() {
    return html`
      <div class="with-sidebar-l content" style="--sidebar-gutter: var(--s0)">
        <div>
          <div class="sidebar sidebar-intrinsic-l">
            <div class="sidebar__avatar">
              <unpub-editable-hero
                imageSrc=${this.avatarUrl}
                maxSize=${this._unpubService!.config.maxAvatarSizeBytes}
                @imageChanged=${this._avatarImageChanged}
                .updateImage=${this._updateImage.bind(this)}
              ></unpub-editable-hero>
            </div>
            <h3>${this._me?.displayName}</h3>
          </div>
          <div class="not-sidebar-l stack-l">
            <div class="content__header">
              <div class="cluster-l" style="--cluster-margin: var(--s-2)">
                <div>
                  <h2>My Games</h2>
                  <sl-icon-button
                    id="addGameButton"
                    name="plus-circle-fill"
                    @click=${this._onAddGameClick}
                  >
                  </sl-icon-button>
                </div>
              </div>
              <div class="games">
                <div>
                  ${this.renderGames()}
                </div>
              </div>
            </div>
            <div>
              <h2>My Recent Feedback</h2>
              <div>
                <unpub-feedback-summary
                  .feedback=${this._me!.feedback}
                  showGame
                ></unpub-feedback-summary>
              </div>
            </div>
          </div>
        </div>
      </div>
    `;
  }

  private renderGames() {
    return this._me?.games?.length
      ? html`
          <ul class="games__list">
            ${this._me.games.map(g => {
              return html`
                <a href="/me/games/${g.id}">
                  <li>${g.name}</li>
                </a>
              `;
            })}
          </ul>
        `
      : html`
          <span class="games__info">No Games</span>
        `;
  }
}
