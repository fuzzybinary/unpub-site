import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { SlButton } from '@shoelace-style/shoelace';
import { Router } from '@vaadin/router';
import { ConfirmDialog } from 'components/shared/ConfirmDialog';
import { FeedbackDetails } from 'models/Game';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import { ApiResult, UnpubService } from 'services/UnpubService';
import sinon from 'sinon';
import { SinonStubbedInstance } from 'sinon';

import './FeedbackDetails';

describe('FeedbackDetails', () => {
  let authServiceMock: SinonStubbedInstance<AuthService> | null = null;
  let unpubServiceMock: SinonStubbedInstance<UnpubService> | null = null;

  const provideMock = (event: Event) => {
    const ce = event as CustomEvent<RequestDependencyEventDetail>;
    if (ce.detail.type.name === 'UnpubService') {
      ce.detail.instance = unpubServiceMock;
    } else if (ce.detail.type.name === 'AuthService') {
      ce.detail.instance = authServiceMock;
    }
  };

  beforeEach(() => {
    authServiceMock = sinon.createStubInstance(AuthService);
    unpubServiceMock = sinon.createStubInstance(UnpubService);
    document.addEventListener(DependencyEventName, provideMock);
  });

  afterEach(() => {
    document.removeEventListener(DependencyEventName, provideMock);
    sinon.restore();
  });

  describe('Forbidden behaviors', () => {
    it('should redirect if not authed', async () => {
      authServiceMock?.isAuthenticated.returns(false);
      const goStub = sinon.stub(Router, 'go');

      const location = {
        pathname: '/me/feedback/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-feedback-details .location=${location}></unpub-feedback-details>
      `);
      expect(goStub.calledWith('/login?redirect=/me/feedback/fake-id')).to.be.true;
    });

    it('should display a spinner while loading', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolvePromise: ((result: ApiResult<FeedbackDetails>) => void) | null = null;
      unpubServiceMock?.fetchFeedbackDetails.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const location = {
        pathname: '/me/feedback/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-feedback-details .location=${location}></unpub-feedback-details>
      `);
      const spinner = el.shadowRoot?.querySelector('sl-spinner');
      expect(spinner).to.be.ok;
    });

    it('should display 404 if not authorized', async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolvePromise: ((result: ApiResult<FeedbackDetails>) => void) | null = null;
      unpubServiceMock?.fetchFeedbackDetails.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const location = {
        pathname: '/me/feedback/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-feedback-details .location=${location}></unpub-feedback-details>
      `);

      resolvePromise!({
        status: 'forbidden',
        data: null,
        messages: []
      });
      await nextFrame();

      const fourOhfour = el.shadowRoot?.querySelector('unpub-four-oh-four');
      expect(fourOhfour).to.be.ok;
    });
  });

  describe('Allowed operations', () => {
    const feedback: FeedbackDetails = {
      id: 'fake-id',
      game: {
        id: 'fake-game-id',
        name: 'My Fake Game',
        owner: {
          id: 'fake-owner-id',
          displayName: 'The Designer'
        }
      },
      created: '2020-07-08T19:46:16.888Z',
      time: 120,
      players: 3,
      firstPlayerScore: 10,
      lastPlayerScore: 10,
      gameLength: 1,
      easeOfLearning: 2,
      downTime: 3,
      decisions: 4,
      interactivity: 1,
      originality: 5,
      fun: 5,
      predictable: 'Yes',
      predictableWhy: 'Predictability comments.',
      playAgain: 'Maybe',
      buy: 'No',
      changeOneThing: 'One thing to change comments',
      favoritePart: 'Favorite part comments',
      comments: 'My generalized comments on the game.',
      isAnonymous: false,
      feedbackMeta: {
        fromEmail: 'myfakeemail',
        fromName: 'My Fake Name'
      }
    };
    let el: Element;

    beforeEach(async () => {
      authServiceMock?.isAuthenticated.returns(true);
      let resolvePromise: ((result: ApiResult<FeedbackDetails>) => void) | null = null;
      unpubServiceMock?.fetchFeedbackDetails.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const location = {
        pathname: '/me/feedback/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-feedback-details .location=${location}></unpub-feedback-details>
      `);

      resolvePromise!({
        status: 'ok',
        messages: [],
        data: feedback
      });
      await nextFrame();
    });

    it('should render all feedback fields', () => {
      const checkField = (fieldId: string, label: string, content: string) => {
        const field = el.shadowRoot?.getElementById(fieldId);
        expect(field).to.be.ok;

        const labelElement = field?.querySelector('.label') as HTMLElement;
        expect(labelElement).to.be.ok;
        expect(labelElement.textContent?.trim()).to.equal(label);

        const valueElement = field?.querySelector('.value') as HTMLElement;
        expect(valueElement).to.be.ok;
        expect(valueElement.textContent?.trim()).to.equal(content);
      };

      checkField('time', 'Time (in minutes)', feedback.time.toString());
      checkField('players', 'Number of Players', feedback.players.toString());
      checkField('firstPlayerScore', 'First Player Score', feedback.firstPlayerScore.toString());
      checkField('lastPlayerScore', 'Last Player Score', feedback.lastPlayerScore.toString());
      checkField('gameLength', 'Game Length', feedback.gameLength.toString());
      checkField('easeOfLearning', 'Ease of Learning', feedback.easeOfLearning.toString());
      checkField('downTime', 'Down Time', feedback.downTime.toString());
      checkField('decisions', 'Decisions', feedback.decisions.toString());
      checkField('interactivity', 'Interactivity', feedback.interactivity.toString());
      checkField('originality', 'Originality', feedback.originality.toString());
      checkField('fun', 'Fun / Enjoyable', feedback.fun.toString());
      checkField(
        'predictable',
        'Was the end of the game predictable?',
        feedback.predictable.toString()
      );
      checkField('predictableWhy', 'If so, why?', feedback.predictableWhy);
      checkField('playAgain', 'Would you play again?', feedback.playAgain.toString());
      checkField('buy', 'Would you buy this?', feedback.buy.toString());
      checkField('oneChange', 'What is one thing you would change?', feedback.changeOneThing);
      checkField(
        'favoritePart',
        'What was your favorite part of this game?',
        feedback.favoritePart
      );
      checkField('additionalComments', 'Any additional comments?', feedback.comments);
    });

    it('should render a delete button', () => {
      const deleteButton = el.shadowRoot?.getElementById('deleteButton');
      expect(deleteButton).to.be.ok;
    });

    it('clicking delete displays confirm dialog', async () => {
      const deleteButton = el.shadowRoot?.getElementById('deleteButton') as SlButton;

      deleteButton.click();
      await nextFrame();

      const dialog = el.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
      expect(dialog).to.be.ok;
      expect(dialog.open).to.be.true;
    });

    it('clicking ok calls to delete the game', async () => {
      let resolvePromise: ((result: ApiResult<void>) => void) | null = null;
      unpubServiceMock?.deleteFeedback.returns(new Promise(resolve => (resolvePromise = resolve)));

      const deleteButton = el.shadowRoot?.getElementById('deleteButton') as SlButton;

      deleteButton.click();
      await nextFrame();

      const dialog = el.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
      dialog.okButton.click();
      await nextFrame();

      expect(unpubServiceMock?.deleteFeedback.called).to.be.true;
    });

    it('success on delete redirects back', async () => {
      let resolvePromise: ((result: ApiResult<void>) => void) | null = null;
      unpubServiceMock?.deleteFeedback.returns(new Promise(resolve => (resolvePromise = resolve)));
      const backStub = sinon.stub(history, 'back');

      const deleteButton = el.shadowRoot?.getElementById('deleteButton') as SlButton;

      deleteButton.click();
      await nextFrame();

      const dialog = el.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
      dialog.okButton.click();
      resolvePromise!({
        status: 'ok',
        data: null,
        messages: []
      });
      await nextFrame();

      expect(backStub.called).to.be.true;
    });
  });
});
