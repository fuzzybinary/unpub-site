import { Router } from '@vaadin/router';
import 'components/shared/ConfirmDialog';
import { ConfirmDialog } from 'components/shared/ConfirmDialog';
import {
  feedbackForm,
  Field,
  FieldType,
  FormSection
} from 'components/shared/DefaultFeedbackFields';
import { LoadResult, unpubLoad } from 'components/shared/Loader';
import { html, LitElement } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { FeedbackDetails } from 'models/Game';
import { diConsumer, withLocation } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';
import { clusterStyles, stackStyles } from 'styles/styles';

@customElement('unpub-feedback-details')
class FeedbackDetailsPage extends withLocation(diConsumer(LitElement)) {
  static get styles() {
    return [clusterStyles, stackStyles];
  }

  private _authService!: AuthService | null;
  private _unpubService!: UnpubService | null;

  private _id: string | null = null;

  @state() private _feedback: FeedbackDetails | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
    this._unpubService = this.resolve(UnpubService);

    if (!this._authService?.isAuthenticated()) {
      if (this.location) {
        Router.go(`/login?redirect=${this.location.pathname}`);
      } else {
        Router.go('/login');
      }
    }

    this._id = (this.location?.params.id as string) || null;
  }

  public render() {
    return unpubLoad(
      this._load.bind(this),
      this._feedback &&
        html`
          <div class="cluster-l" style="--cluster-margin: var(--s-2)">
            <div>
              <h1>Feedback Details</h1>
              <sl-button circle id="deleteButton" @click=${this._onDeleteClicked}>
                <sl-icon name="trash-fill"></sl-icon>
              </sl-button>
            </div>
          </div>
          <div>Feedback for ${this._feedback?.game.name}</div>
          <div class="stack-l">
            ${feedbackForm.map(section => this._renderSection(section))}
          </div>
          <unpub-confirm-dialog
            id="confirmDialog"
            .operation=${this._deleteFeedback.bind(this)}
            @confirmcomplete=${this._onDeleteConfirmed}
          >
            Are you sure you want to delete this feedback? You should only delete feedback that was
            left by accident or that is not helpful.
          </unpub-confirm-dialog>
        `
    );
  }

  private _renderSection(section: FormSection) {
    return html`
      <div class="sectionBox">
        <h2>${section.name}</h2>
        <div class="stack-l">
          ${section.fields.map(field => this._renderField(field))}
        </div>
      </div>
    `;
  }

  private _getFieldValue(feedback: FeedbackDetails, field: Field): any {
    const fieldName = field.jsonField ?? field.name;
    return (feedback as any)[fieldName];
  }

  private _renderField(field: Field) {
    switch (field.type) {
      case FieldType.text:
      case FieldType.number:
      case FieldType.slider:
      case FieldType.textArea:
      case FieldType.discreetAnswer:
        return html`
          <div id=${field.name}>
            <div class="label">${field.label}</div>
            <div class="value">${this._getFieldValue(this._feedback!, field)}</div>
          </div>
        `;
    }
  }

  private async _load() {
    if (!this._id) {
      return LoadResult.noData;
    }

    const data = await this._unpubService?.fetchFeedbackDetails(this._id);

    if (data?.status === 'ok') {
      this._feedback = data?.data ?? null;
      return this._feedback ? LoadResult.ok : LoadResult.error;
    }

    return LoadResult.noData;
  }

  private _onDeleteClicked() {
    const dialog = this.shadowRoot?.getElementById('confirmDialog') as ConfirmDialog;
    dialog.show();
  }

  private async _deleteFeedback() {
    const result = await this._unpubService!.deleteFeedback(this._feedback!.id);
    if (result.status === 'ok') {
      return null;
    }

    return result.messages;
  }

  private _onDeleteConfirmed() {
    history.back();
  }
}
