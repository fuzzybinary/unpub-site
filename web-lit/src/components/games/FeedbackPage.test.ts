import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';
import { SinonStubbedInstance } from 'sinon';
import sinon from 'sinon';

import { FeedbackSubmissionResult, GameDetailsPublic, GameFeedbackSubmission } from 'models/Game';
import { FeedbackControl } from './FeedbackControl';
import './FeedbackPage';

describe('FeedbackPage', () => {
  let authServiceMock: SinonStubbedInstance<AuthService> | null = null;
  let unpubServiceMock: SinonStubbedInstance<UnpubService> | null = null;

  const provideMock = (event: Event) => {
    const ce = event as CustomEvent<RequestDependencyEventDetail>;
    if (ce.detail.type.name === 'UnpubService') {
      ce.detail.instance = unpubServiceMock;
    } else if (ce.detail.type.name === 'AuthService') {
      ce.detail.instance = authServiceMock;
    }
  };

  beforeEach(() => {
    authServiceMock = sinon.createStubInstance(AuthService);
    unpubServiceMock = sinon.createStubInstance(UnpubService);
    document.addEventListener(DependencyEventName, provideMock);
  });

  afterEach(() => {
    document.removeEventListener(DependencyEventName, provideMock);
    sinon.restore();
  });

  describe('rendering', () => {
    it('should render a loading spinner', async () => {
      const el = await fixture(html`
        <unpub-feedback-page></unpub-feedback-page>
      `);

      const spinner = el.shadowRoot?.querySelector('sl-spinner');
      expect(spinner).to.be.ok;
      el.remove();
    });

    it('should request game info for the location', async () => {
      let resolvePromise: ((result: GameDetailsPublic) => void) | null = null;
      const fetchStub = unpubServiceMock?.fetchGame.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const location = {
        pathname: '/me/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-feedback-page .location=${location}></unpub-feedback-page>
      `);
      expect(fetchStub?.called).to.be.true;
      expect(fetchStub?.firstCall.args[0]).to.equal('fake-id');

      el.remove();
    });

    it('should render the feedback control on successful load', async () => {
      let resolvePromise: ((result: GameDetailsPublic) => void) | null = null;
      unpubServiceMock?.fetchGame.returns(new Promise(resolve => (resolvePromise = resolve)));

      const location = {
        pathname: '/me/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-feedback-page .location=${location}></unpub-feedback-page>
      `);
      resolvePromise!({
        id: 'fake-id',
        name: 'name',
        description: 'description',
        minPlayers: 0,
        maxPlayers: 100,
        minAge: 12,
        time: '30',
        mechanisms: [],
        owner: {
          id: 'fake-user-id',
          displayName: 'Fake Designer'
        }
      });
      await nextFrame();

      const feedbackForm = el.shadowRoot?.querySelector('unpub-feedback-control');
      expect(feedbackForm).to.be.ok;

      el.remove();
    });
  });

  describe('submission', () => {
    const feedback: GameFeedbackSubmission = {
      time: 30,
      players: 5,
      firstPlayerScore: 30,
      lastPlayerScore: 10,
      gameLength: 1,
      easeOfLearning: 3,
      downTime: 5,
      decisions: 1,
      interactivity: 2,
      originality: 2,
      fun: 5,
      predictable: 'No',
      predictableWhy: 'Because it was',
      playAgain: 'Maybe',
      buy: 'Yes',
      changeOneThing: 'One thing to change',
      favoritePart: 'My ffavorite part',
      comments: 'Comments',
      isAnonymous: true,
      shareEmailOptIn: false
    };
    let el: Element;

    beforeEach(async () => {
      const location = {
        pathname: '/me/fake-id',
        params: {
          id: 'fake-id'
        }
      };
      el = await fixture(html`
        <!-- @ts-ignore -->
        <unpub-feedback-page .location=${location}></unpub-feedback-page>
      `);
    });

    afterEach(() => {
      el.remove();
    });

    it('should disable the control while submitting', async () => {
      let resolvePromise: ((result: FeedbackSubmissionResult | null) => void) | null = null;
      unpubServiceMock?.submitFeedback.returns(new Promise(resolve => (resolvePromise = resolve)));

      const feedbackControl = el.shadowRoot?.querySelector(
        'unpub-feedback-control'
      ) as FeedbackControl;
      feedbackControl?.dispatchEvent(
        new CustomEvent<GameFeedbackSubmission>('submit-feedback', {
          detail: feedback
        })
      );
      await nextFrame();

      expect(feedbackControl?.disabled).to.be.true;
    });

    it('should submit feedback', async () => {
      let resolvePromise: ((result: FeedbackSubmissionResult | null) => void) | null = null;
      const submitStub = unpubServiceMock?.submitFeedback.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const feedbackControl = el.shadowRoot?.querySelector(
        'unpub-feedback-control'
      ) as FeedbackControl;
      feedbackControl?.dispatchEvent(
        new CustomEvent<GameFeedbackSubmission>('submit-feedback', {
          detail: feedback
        })
      );
      await nextFrame();

      expect(submitStub?.called).to.be.true;
      expect(submitStub?.firstCall.args[0]).to.equal('fake-id');
      expect(submitStub?.firstCall.args[1]).to.deep.equal(feedback);
    });

    it('should re-enable if submission fails', async () => {
      let resolvePromise: ((result: FeedbackSubmissionResult | null) => void) | null = null;
      const submitStub = unpubServiceMock?.submitFeedback.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      const feedbackControl = el.shadowRoot?.querySelector(
        'unpub-feedback-control'
      ) as FeedbackControl;
      feedbackControl?.dispatchEvent(
        new CustomEvent<GameFeedbackSubmission>('submit-feedback', {
          detail: feedback
        })
      );
      await nextFrame();

      resolvePromise!(null);
      await nextFrame();

      expect(feedbackControl.disabled).to.be.false;
    });

    it('should clear if successful', async () => {
      let resolvePromise: ((result: FeedbackSubmissionResult | null) => void) | null = null;
      const submitStub = unpubServiceMock?.submitFeedback.returns(
        new Promise(resolve => (resolvePromise = resolve))
      );

      let feedbackControl = el.shadowRoot?.querySelector(
        'unpub-feedback-control'
      ) as FeedbackControl;
      feedbackControl?.dispatchEvent(
        new CustomEvent<GameFeedbackSubmission>('submit-feedback', {
          detail: feedback
        })
      );
      await nextFrame();

      resolvePromise!({
        id: 'fake-feedback-id',
        gameId: 'fake=id',
        created: 'fake-time'
      });
      await nextFrame();

      feedbackControl = el.shadowRoot?.querySelector('unpub-feedback-control') as FeedbackControl;
      expect(feedbackControl).to.not.be.ok;
    });
  });
});
