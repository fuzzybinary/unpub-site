import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import sinon, { SinonStubbedInstance } from 'sinon';

import { GameSummary } from 'models/Game';
import { UnpubService } from 'services/UnpubService';
import './GamesList';

describe('GamesList', () => {
  describe('rendering', () => {
    let unpubServiceMock: SinonStubbedInstance<UnpubService> | null = null;

    const provideMock = (event: Event) => {
      const ce = event as CustomEvent<RequestDependencyEventDetail>;
      if (ce.detail.type.name === 'UnpubService') {
        ce.detail.instance = unpubServiceMock;
      }
    };

    beforeEach(async () => {
      unpubServiceMock = sinon.createStubInstance(UnpubService);
      document.addEventListener(DependencyEventName, provideMock);
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideMock);
    });

    it('Renders a spinner by default', async () => {
      let resolveFetchPromise: ((games: GameSummary[]) => void) | null = null;
      unpubServiceMock?.fetchGames.returns(new Promise(resolve => (resolveFetchPromise = resolve)));

      const el = await fixture(
        html`
          <unpub-games-list></unpub-games-list>
        `
      );
      const spinner = el?.shadowRoot?.querySelector('sl-spinner');
      expect(spinner).to.be.ok;

      resolveFetchPromise!([]);
    });

    const renderWithGames = async (games: GameSummary[]) => {
      let resolveFetchPromise: ((games: GameSummary[]) => void) | null = null;
      unpubServiceMock?.fetchGames.returns(new Promise(resolve => (resolveFetchPromise = resolve)));

      const el = await fixture(
        html`
          <unpub-games-list></unpub-games-list>
        `
      );

      resolveFetchPromise!(games);
      await nextFrame();

      return el;
    };

    it('displays no games after load for empty list', async () => {
      const el = await renderWithGames([]);

      const spinner = el?.shadowRoot?.querySelector('sl-spinner');
      expect(spinner).to.not.be.ok;
      const gamesList = el?.shadowRoot?.getElementById('gamesList');
      expect(gamesList).to.be.ok;
      expect(gamesList?.innerText.toLowerCase()).to.equal('no games');
    });

    it('displays games after load', async () => {
      const gameData: GameSummary[] = [
        { id: 'gameA', name: 'Game 1' },
        { id: 'gameB', name: 'Game 2' },
        { id: 'gameC', name: 'Game 3' }
      ];
      const el = await renderWithGames(gameData);

      const gameList = el?.shadowRoot?.getElementById('gamesList');
      const games = gameList?.querySelectorAll('.gamesList__game');
      expect(games?.length).to.equal(3);
      gameData.forEach((data, index) => {
        expect(games![index].textContent).to.equal(data.name);
      });
    });
  });
});
