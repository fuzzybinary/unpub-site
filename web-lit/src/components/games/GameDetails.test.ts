import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { GameInfo } from 'components/shared/GameInfo';
import sinon, { SinonStubbedInstance } from 'sinon';
import { GameDetailsPublic } from '../../models/Game';
import { DependencyEventName, RequestDependencyEventDetail } from '../../providers';
import { UnpubService } from '../../services/UnpubService';

import './GameDetails';

describe('GameDetails', () => {
  describe('Rendering', () => {
    let unpubServiceMock: SinonStubbedInstance<UnpubService> | null = null;

    const provideMock = (event: Event) => {
      const ce = event as CustomEvent<RequestDependencyEventDetail>;
      if (ce.detail.type.name === 'UnpubService') {
        ce.detail.instance = unpubServiceMock;
      }
    };

    beforeEach(() => {
      unpubServiceMock = sinon.createStubInstance(UnpubService);
      document.addEventListener(DependencyEventName, provideMock);
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideMock);
      sinon.restore();
    });

    it('shows a loading indicator by default', async () => {
      let resolveFetchPromise: ((game: GameDetailsPublic | null) => void) | null = null;
      unpubServiceMock?.fetchGame.returns(new Promise(resolve => (resolveFetchPromise = resolve)));

      const location = {
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(
        html`
          <!-- @ts-ignore -->
          <unpub-game-details .location=${location}></unpub-game-details>
        `
      );

      const spinner = el?.shadowRoot?.querySelector('sl-spinner');
      expect(spinner).to.be.ok;

      resolveFetchPromise!(null);
    });

    it('fetches a game by id', async () => {
      let resolveFetchPromise: ((game: GameDetailsPublic | null) => void) | null = null;
      unpubServiceMock?.fetchGame.returns(new Promise(resolve => (resolveFetchPromise = resolve)));

      const location = {
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(
        html`
          <!-- @ts-ignore -->
          <unpub-game-details .location=${location}></unpub-game-details>
        `
      );

      expect(unpubServiceMock?.fetchGame.calledWith('fake-id')).to.be.true;

      resolveFetchPromise!(null);
    });

    it('removes the spinner post fetch', async () => {
      let resolveFetchPromise: ((game: GameDetailsPublic | null) => void) | null = null;
      unpubServiceMock?.fetchGame.returns(new Promise(resolve => (resolveFetchPromise = resolve)));

      const location = {
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(
        html`
          <!-- @ts-ignore -->
          <unpub-game-details .location=${location}></unpub-game-details>
        `
      );
      resolveFetchPromise!(null);
      await nextFrame();

      expect(el.shadowRoot?.querySelector('sl-spinner')).to.not.be.ok;
    });

    it('shows 404 on failed load', async () => {
      let resolveFetchPromise: ((game: GameDetailsPublic | null) => void) | null = null;
      unpubServiceMock?.fetchGame.returns(new Promise(resolve => (resolveFetchPromise = resolve)));

      const location = {
        params: {
          id: 'fake-id'
        }
      };
      const el = await fixture(
        html`
          <!-- @ts-ignore -->
          <unpub-game-details .location=${location}></unpub-game-details>
        `
      );
      resolveFetchPromise!(null);
      await nextFrame();

      expect(el.shadowRoot?.querySelector('unpub-four-oh-four')).to.be.ok;
    });

    describe('loaded game rendering', () => {
      let el: Element;
      let game: GameDetailsPublic;

      beforeEach(async () => {
        game = {
          id: 'fake-id',
          name: 'My Awesome Game',
          time: '20-30',
          minAge: 8,
          minPlayers: 3,
          maxPlayers: 6,
          mechanisms: ['Mechanism 1', 'Mechanism 2'],
          description: 'The description of the game that is awesome.',
          owner: {
            id: 'fake-id',
            displayName: 'Designer Name',
            avatarUrl: '/images/fake-image.png'
          }
        };

        let resolveFetchPromise: ((game: GameDetailsPublic | null) => void) | null = null;
        unpubServiceMock?.fetchGame.returns(
          new Promise(resolve => (resolveFetchPromise = resolve))
        );

        const location = {
          params: {
            id: 'fake-id'
          }
        };
        el = await fixture(
          html`
            <!-- @ts-ignore -->
            <unpub-game-details .location=${location}></unpub-game-details>
          `
        );

        resolveFetchPromise!(game);
        await nextFrame();

        return el;
      });

      it('renders the game title as the header', () => {
        const header = el.shadowRoot?.querySelector('h1');
        expect(header).to.be.ok;
        expect(header?.innerText.trim()).to.equal('My Awesome Game');
      });

      it('renders the designer', () => {
        const designer = el.shadowRoot?.querySelector('.gameDetails__designer');
        expect(designer).to.be.ok;
        expect(designer?.textContent?.trim().toLowerCase()).to.equal('designed by: designer name');
      });

      it('renders game info', () => {
        const info = el.shadowRoot?.querySelector('unpub-game-info') as GameInfo;
        expect(info).to.be.ok;
        expect(info.gameInfo).to.equal(game);
      });

      it('renders a give feedback button', async () => {
        const feedbackButton = el.shadowRoot?.getElementById('submitFeedback');
        expect(feedbackButton).to.be.ok;
      });
    });
  });
});
