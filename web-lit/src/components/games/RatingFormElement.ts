import { SlRating } from '@shoelace-style/shoelace';
import { renderFormControl } from '@shoelace-style/shoelace/dist/internal/form-control';
import { css, html, LitElement, PropertyValues } from 'lit';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import { customElement, property, query } from 'lit/decorators.js';

@customElement('unpub-rating')
export class UnpubRating extends LitElement {
  @property() public label?: string;

  @property() public name?: string;

  @property({ type: Boolean, reflect: true })
  public required: boolean = false;

  @property({ type: Boolean, reflect: true })
  public disabled: boolean = false;

  @property()
  public value = '';

  public render() {
    let intValue: number | undefined;

    if (this.value) {
      intValue = parseInt(this.value, 10);
      if (isNaN(intValue)) {
        intValue = undefined;
      }
    }

    return html`
      <div class="stack-l">
        <div>
          <label for=${ifDefined(this.name)}>${this.label}</label>
        </div>
        <div>
          <slot></slot>
        </div>
        <div class="ratingContent">
          <input
            tabindex="-1"
            class="input__control"
            name=${ifDefined(this.name)}
            value=${this.value}
            ?required=${this.required}
            type="number"
            style="width: 0; height: 0; opacity: 0; padding: 0; margin: 0;"
          />
          <sl-rating
            ?disabled=${this.disabled}
            value=${ifDefined(intValue)}
            @sl-change=${this._handleChange}
          ></sl-rating>
        </div>
      </div>
    `;
  }

  protected firstUpdated(props: PropertyValues) {
    super.firstUpdated(props);

    const cluster = this.shadowRoot?.querySelector('.ratingContent');
    if (cluster) {
      this.appendChild(cluster);
    }
  }

  private _handleChange(e: Event) {
    const rating = e.currentTarget as SlRating;
    this.value = `${rating.value}`;
    const event = new CustomEvent('sl-change', {
      bubbles: true,
      cancelable: false,
      composed: true,
      detail: {}
    });
    this.dispatchEvent(event);
  }
}
