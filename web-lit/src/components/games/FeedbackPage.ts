import { SlAlert } from '@shoelace-style/shoelace';
import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { GameDetailsPublic, GameFeedbackSubmission } from 'models/Game';
import { diConsumer, withLocation } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';

import './FeedbackControl';

enum PageState {
  Loading,
  Normal,
  PerformingSubmit,
  SuccessfulSubmit
}

@customElement('unpub-feedback-page')
class FeedbackPage extends withLocation(diConsumer(LitElement)) {
  private _unpubService: UnpubService | null = null;
  private _authService: AuthService | null = null;
  private _id: string | null = null;

  @property({ attribute: false }) private _pageState = PageState.Loading;
  @property({ attribute: false }) private _gameDetails: GameDetailsPublic | null = null;

  @property({ attribute: false }) get _authenticated() {
    return this._authService?.isAuthenticated() ?? false;
  }

  public connectedCallback() {
    super.connectedCallback();

    this._unpubService = this.resolve(UnpubService);
    this._authService = this.resolve(AuthService);
    this._authService?.subscribeToAuthChanges(this._authHandler);

    this._id = (this.location?.params.id as string) || null;
    this._load();
  }

  public disconnectedCallback() {
    super.disconnectedCallback();

    this._authService?.unsubscribeToAuthChanges(this._authHandler);
  }

  public render() {
    switch (this._pageState) {
      case PageState.Loading:
        return html`
          <sl-spinner></sl-spinner>
        `;
      case PageState.Normal:
      case PageState.PerformingSubmit:
        return this._renderFeedbackForm();
      case PageState.SuccessfulSubmit:
        return this._renderThankYou();
    }
  }

  private _renderFeedbackForm() {
    return html`
      <h1>Feedback for ${this._gameDetails?.name}</h1>
      <unpub-feedback-control
        ?disabled=${this._pageState === PageState.PerformingSubmit}
        ?authenticated=${this._authenticated}
        @submit-feedback=${this._onSubmitFeedback}
      ></unpub-feedback-control>
      <sl-alert id="errorAlert" type="warning" closable>
        <sl-icon slot="icon" name="exclamation-triangle"></sl-icon>
        <strong>An error occured submitting your feedback</strong>
      </sl-alert>
    `;
  }

  private _renderThankYou() {
    return html`
      <h1>Feedback for ${this._gameDetails?.name}</h1>
      <h2>Thank you for your feedback!</h2>
      <a href="/games/${this._id}">Return to game detils</a>
    `;
  }

  private async _onSubmitFeedback(e: CustomEvent<GameFeedbackSubmission>) {
    let error = true;
    this._pageState = PageState.PerformingSubmit;
    if (e.detail) {
      const result = await this._unpubService?.submitFeedback(this._id!, e.detail);
      if (result) {
        error = false;
      }
    }
    if (error) {
      this._pageState = PageState.Normal;
      const alert = this.shadowRoot?.querySelector('#errorAlert') as SlAlert;
      alert.show();
    } else {
      this._pageState = PageState.SuccessfulSubmit;
    }
  }

  private async _load() {
    if (this._id) {
      this._gameDetails = await this._unpubService!.fetchGame(this._id);
      this._pageState = PageState.Normal;
    }
  }

  private _authHandler = (isAuthenticated: boolean) => {
    this.requestUpdate('_authenticated');
  };
}
