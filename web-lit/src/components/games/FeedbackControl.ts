import { SlCheckbox } from '@shoelace-style/shoelace';
import {
  feedbackForm,
  Field,
  FieldType,
  FormSection
} from 'components/shared/DefaultFeedbackFields';
import 'components/shared/InfoButton';
import { css, html, LitElement } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { GameFeedbackSubmission } from 'models/Game';
import { clusterStyles, gridStyles, stackStyles } from 'styles/styles';
import { DiscreetAnswerElement } from './DiscreetAnswer';

import './DiscreetAnswer';
import './RatingFormElement';

export interface FormField extends LitElement {
  value: string;
  required: boolean;
  disabled: boolean;
}

@customElement('unpub-feedback-control')
export class FeedbackControl extends LitElement {
  static get styles() {
    return [
      clusterStyles,
      stackStyles,
      gridStyles,
      DiscreetAnswerElement.externalStyles,
      css`
        :host {
          --textarea-min-height: 10rem;
        }

        .sectionBox {
          padding: var(--s-1);
        }

        .stack-l > * + * {
          margin-top: var(--s-2);
        }
      `
    ];
  }

  @property({ type: Boolean }) public authenticated: boolean = false;
  @property({ type: Boolean }) public disabled: boolean = false;

  @query('#postAnonymous')
  private _postAnonymous!: SlCheckbox;

  @query('#shareEmail')
  private _shareEmail!: SlCheckbox;

  @property({ attribute: false })
  public get feedbackValue(): GameFeedbackSubmission {
    const value = feedbackForm.reduce((prevSection, section, index) => {
      return section.fields.reduce((prev, field) => {
        const htmlField = this.shadowRoot?.querySelector(`[name="${field.name}"]`) as FormField;
        let fieldValue: any = htmlField?.value ?? null;
        if (field.type === FieldType.number || field.type === FieldType.slider) {
          fieldValue = Number(fieldValue);
        }
        const fieldName = field.jsonField ?? field.name;
        return Object.assign(prev, {
          [fieldName]: fieldValue
        });
      }, prevSection);
    }, {});
    const returnValue = value as GameFeedbackSubmission;
    returnValue.isAnonymous = this._postAnonymous.checked;
    returnValue.shareEmailOptIn = this._shareEmail.checked;
    if (!this.authenticated) {
      // prevent blanks from being put into this form. If it's not filled in it should
      // stay as undefined.
      const nameValue = (this.shadowRoot?.getElementById('fromName') as FormField)?.value;
      if (!!nameValue) {
        returnValue.fromName = nameValue;
      }
      const emailValue = (this.shadowRoot?.getElementById('email') as FormField)?.value;
      if (!!emailValue) {
        returnValue.fromEmail = emailValue;
      }
    }
    return value as GameFeedbackSubmission;
  }

  public render() {
    return html`
      <sl-form id="formControl" @sl-submit=${this._onSubmit}>
        <div class="stack-l">
          ${feedbackForm.map(section => this._renderSection(section))}
          <div class="sectionBox">
            <h2>Your Info</h2>
            <div class="stack-l">
              ${this.authenticated ? '' : this._renderAnonymousInfoFields()}
              ${this._renderInfoShareCheckboxes()}
            </div>
          </div>
          <div>
            <sl-button style="display: inline" type="primary" submit>Submit</sl-button>
          </div>
        </div>
      </sl-form>
    `;
  }

  private _onSubmit(e: Event) {
    e.stopPropagation();
    e.preventDefault();
    this.dispatchEvent(
      new CustomEvent<GameFeedbackSubmission | null>('submit-feedback', {
        detail: this.feedbackValue
      })
    );
  }

  private _renderSection(section: FormSection) {
    const sectionLayoutClass = section.layout === 'grid' ? 'grid-l' : 'stack-l';
    return html`
      <div class="sectionBox">
        <h2>${section.name}</h2>
        <div class=${sectionLayoutClass}>
          ${section.fields.map(field => this._renderField(field))}
        </div>
      </div>
    `;
  }

  private _renderField(field: Field) {
    switch (field.type) {
      case FieldType.text:
      case FieldType.number:
        return html`
          <sl-input
            id=${field.name}
            name=${field.name}
            label=${field.label}
            ?required=${!!field.required}
            ?disabled=${this.disabled}
            type=${field.type === FieldType.number ? 'number' : 'text'}
          ></sl-input>
        `;
      case FieldType.textArea:
        return html`
          <sl-textarea
            id=${field.name}
            name=${field.name}
            label=${field.label}
            ?required=${!!field.required}
            ?disabled=${this.disabled}
            outlined
          >
          </sl-textarea>
        `;
      case FieldType.slider:
        return html`
          <div class="stack-l">
            <unpub-rating
              label=${field.label}
              id=${field.name}
              name=${field.name}
              ?required=${!!field.required}
              ?disabled=${this.disabled}
            >
            </unpub-rating>
          </div>
        `;
      case FieldType.discreetAnswer:
        return html`
          <unpub-discreet-answer
            id=${field.name}
            name=${field.name}
            label=${field.label}
            ?required=${!!field.required}
            ?disabled=${this.disabled}
          >
          </unpub-discreet-answer>
        `;
    }
  }

  private _renderInfoShareCheckboxes() {
    return html`
      <span style="display: inline;">
        <sl-checkbox id="postAnonymous" name="postAnonymous" ?disabled=${this.disabled}>
          Post Feedback Anonymously
        </sl-checkbox>
        <unpub-info-button>
          <div>
            Leaving anonymous feedback hides your name and email from the designer for this
            feedback. Your email and name (if provided) or your user id (if you are logged in) will
            still be tracked on this feedback so that you can refer back to it, and for quality
            assurance and moderation purposes.
          </div>
        </unpub-info-button>
      </span>
      <span style="display: inline;">
        <sl-checkbox id="shareEmail" name="shareEmail" ?disabled=${this.disabled}>
          Share your email with the designer?
        </sl-checkbox>
        <unpub-info-button>
          <div>
            This opts you in sharing your email with the designer, allowing them to supply you with
            updates on the game's design and development. Your email is kept separate from your
            feedback in cases where you wish to share feedback anonymously.
          </div>
        </unpub-info-button>
      </span>
    `;
  }

  private _renderAnonymousInfoFields() {
    return html`
      <sl-input
        id="fromName"
        name="fromName"
        label="Your name (optional)"
        ?disabled=${this.disabled}
      ></sl-input>
      <sl-input
        id="email"
        name="email"
        label="Your email (optional)"
        ?disabled=${this.disabled}
      ></sl-input>
    `;
  }
}
