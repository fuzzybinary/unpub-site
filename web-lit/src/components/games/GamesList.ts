import { LoadResult, unpubLoad } from 'components/shared/Loader';
import { css, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { html } from 'lit-html';
import { GameSummary } from 'models/Game';
import { diConsumer } from 'providers';
import { UnpubService } from 'services/UnpubService';

@customElement('unpub-games-list')
class UnpubGames extends diConsumer(LitElement) {
  static get styles() {
    return css`
      #gamesList ul {
        list-style-type: none;
        padding-inline-start: 0px;
      }

      .gamesList__game {
        list-style: none;
        display: block;
        margin-top: 5px;
        margin-bottom: 5px;
        padding: 5px;
        background-color: whitesmoke;
      }
    `;
  }

  private _unpubService: UnpubService | null = null;
  @property() private _gamesList: GameSummary[] | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._unpubService = this.resolve(UnpubService);
  }

  public render() {
    return html`
      <div class="gameListContent">
        <h1>Games</h1>
        ${unpubLoad(
          this._load.bind(this),
          html`
            <div id="gamesList">
              ${this._gamesList?.length
                ? html`
                    <ul>
                      ${this._gamesList.map(game => {
                        return html`
                          <a href="/games/${game.id}">
                            <li class="gamesList__game">${game.name}</li>
                          </a>
                        `;
                      })}
                    </ul>
                  `
                : 'No Games'}
            </div>
          `
        )}
      </div>
    `;
  }

  private async _load() {
    this._gamesList = await this._unpubService!.fetchGames();
    return LoadResult.ok;
  }
}
