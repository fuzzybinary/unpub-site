import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { SlCheckbox, SlForm, SlInput } from '@shoelace-style/shoelace';
import { GameFeedbackSubmission } from 'models/Game';
import { SinonStub } from 'sinon';
import sinon from 'sinon';

import './FeedbackControl';
import { FormField } from './FeedbackControl';

describe('FeedbackControl', () => {
  const fillField = (form: SlForm, fieldName: string, value: string) => {
    const field = form.querySelector(`[name="${fieldName}"]`) as FormField;
    field.value = value;
  };

  const fillCheckbox = (form: SlForm, fieldName: string, value: boolean) => {
    const field = form.querySelector(`[name="${fieldName}"]`) as SlCheckbox;
    field.checked = value;
  };

  const fillBasicForm = (form: SlForm): GameFeedbackSubmission => {
    const expected: GameFeedbackSubmission = {
      time: 30,
      players: 5,
      firstPlayerScore: 30,
      lastPlayerScore: 10,
      gameLength: 1,
      easeOfLearning: 3,
      downTime: 5,
      decisions: 1,
      interactivity: 2,
      originality: 2,
      fun: 5,
      predictable: 'No',
      predictableWhy: 'Because it was',
      playAgain: 'Maybe',
      buy: 'Yes',
      changeOneThing: 'One thing to change',
      favoritePart: 'My ffavorite part',
      comments: 'Comments',
      isAnonymous: true,
      shareEmailOptIn: false
    };

    fillField(form, 'time', expected.time.toString());
    fillField(form, 'players', expected.players.toString());
    fillField(form, 'firstPlayerScore', expected.firstPlayerScore.toString());
    fillField(form, 'lastPlayerScore', expected.lastPlayerScore.toString());
    fillField(form, 'gameLength', expected.gameLength.toString());
    fillField(form, 'easeOfLearning', expected.easeOfLearning.toString());
    fillField(form, 'downTime', expected.downTime.toString());
    fillField(form, 'decisions', expected.decisions.toString());
    fillField(form, 'interactivity', expected.interactivity.toString());
    fillField(form, 'originality', expected.originality.toString());
    fillField(form, 'fun', expected.fun.toString());
    fillField(form, 'predictable', expected.predictable);
    fillField(form, 'predictableWhy', expected.predictableWhy);
    fillField(form, 'playAgain', expected.playAgain);
    fillField(form, 'buy', expected.buy);
    fillField(form, 'oneChange', expected.changeOneThing);
    fillField(form, 'favoritePart', expected.favoritePart);
    fillField(form, 'additionalComments', expected.comments);
    fillCheckbox(form, 'postAnonymous', expected.isAnonymous);
    fillCheckbox(form, 'shareEmail', expected.shareEmailOptIn);

    return expected;
  };

  describe('default rendering', () => {
    let el: Element | null = null;
    let onSubmitStub: SinonStub | null;

    beforeEach(async () => {
      onSubmitStub = sinon.stub();
      el = await fixture(
        html`
          <!-- @ts-ignore -->
          <unpub-feedback-control @submit-feedback=${onSubmitStub}></unpub-feedback-control>
        `
      );
    });

    it('renders a form', () => {
      const form = el?.shadowRoot?.querySelector('sl-form');
      expect(form).to.be.ok;
    });

    it('renders all expected fields', () => {
      const form = el?.shadowRoot?.querySelector('sl-form')!;

      const verifyField = (
        tag: string,
        name: string,
        extra?: { type?: string; required?: boolean }
      ) => {
        const element = form.querySelector(`${tag}[name="${name}"]`) as FormField;
        expect(element).to.be.ok;
        if (extra?.required) {
          expect(element.required).to.be.true;
        }

        if (extra?.type) {
          expect((element as SlInput).type).to.equal(extra.type);
        }
      };

      verifyField('sl-input', 'time', { required: true, type: 'number' });
      verifyField('sl-input', 'players', { required: true, type: 'number' });
      verifyField('sl-input', 'firstPlayerScore', { required: true, type: 'number' });
      verifyField('sl-input', 'lastPlayerScore', { required: true, type: 'number' });
      verifyField('unpub-rating', 'gameLength');
      verifyField('unpub-rating', 'easeOfLearning');
      verifyField('unpub-rating', 'downTime');
      verifyField('unpub-rating', 'decisions');
      verifyField('unpub-rating', 'interactivity');
      verifyField('unpub-rating', 'originality');
      verifyField('unpub-rating', 'fun');
      verifyField('unpub-discreet-answer', 'predictable', { required: true });
      verifyField('sl-textarea', 'predictableWhy');
      verifyField('unpub-discreet-answer', 'playAgain', { required: true });
      verifyField('unpub-discreet-answer', 'buy', { required: true });
      verifyField('sl-textarea', 'oneChange');
      verifyField('sl-textarea', 'favoritePart');
      verifyField('sl-textarea', 'additionalComments');
      verifyField('sl-checkbox', 'postAnonymous');
      verifyField('sl-checkbox', 'shareEmail');
    });

    it('calls onSubmit on submission', async () => {
      const form = el?.shadowRoot?.querySelector('sl-form')!;

      const expected = fillBasicForm(form);

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      expect(onSubmitStub?.called).to.be.true;
      const arg = onSubmitStub?.getCalls()[0].args[0] as CustomEvent<GameFeedbackSubmission>;
      expect(arg.detail).to.deep.equal(expected);
    });
  });

  it('Shows name and email field when not authenticated', async () => {
    const el = await fixture(
      html`
        <unpub-feedback-control ?authenticated=${false}></unpub-feedback-control>
      `
    );

    const form = el?.shadowRoot?.querySelector('sl-form')!;
    const fromName = form.querySelector('[name="fromName"]');
    const email = form.querySelector('[name="email"]');

    expect(fromName).to.be.ok;
    expect(email).to.be.ok;
  });

  it('Fills in name and email field when not authenticated', async () => {
    const onSubmitStub = sinon.stub();

    const el = await fixture(
      html`
        <!-- @ts-ignore -->
        <unpub-feedback-control
          ?authenticated=${false}
          @submit-feedback=${onSubmitStub}
        ></unpub-feedback-control>
      `
    );

    const form = el?.shadowRoot?.querySelector('sl-form')!;

    fillBasicForm(form);
    fillField(form, 'fromName', 'My Fake Name');
    fillField(form, 'email', 'me@fuzzybinary.com');
    form?.dispatchEvent(new Event('sl-submit'));
    await nextFrame();

    expect(onSubmitStub?.called).to.be.true;
    const arg = onSubmitStub?.getCalls()[0].args[0] as CustomEvent<GameFeedbackSubmission>;
    expect(arg.detail.fromName).to.equal('My Fake Name');
    expect(arg.detail.fromEmail).to.equal('me@fuzzybinary.com');
  });

  it('Does not shows name and email field when not authenticated', async () => {
    const el = await fixture(
      html`
        <unpub-feedback-control ?authenticated=${true}></unpub-feedback-control>
      `
    );

    const form = el?.shadowRoot?.querySelector('sl-form')!;
    const fromName = form.querySelector('[name="fromName"]');
    const email = form.querySelector('[name="email"]');

    expect(fromName).to.not.be.ok;
    expect(email).to.not.be.ok;
  });

  it('Disables all child controls when disabled', async () => {
    const el = await fixture(
      html`
        <unpub-feedback-control ?disabled=${true}></unpub-feedback-control>
      `
    );

    const form = el?.shadowRoot?.querySelector('sl-form')!;

    const verifyDisabled = (
      tag: string,
      name: string,
      extra?: { type?: string; required?: boolean }
    ) => {
      const element = form.querySelector(`${tag}[name="${name}"]`) as FormField;
      expect(element.disabled).to.be.true;
    };

    verifyDisabled('sl-input', 'time');
    verifyDisabled('sl-input', 'players');
    verifyDisabled('sl-input', 'firstPlayerScore');
    verifyDisabled('sl-input', 'lastPlayerScore');
    verifyDisabled('unpub-rating', 'gameLength');
    verifyDisabled('unpub-rating', 'easeOfLearning');
    verifyDisabled('unpub-rating', 'downTime');
    verifyDisabled('unpub-rating', 'decisions');
    verifyDisabled('unpub-rating', 'interactivity');
    verifyDisabled('unpub-rating', 'originality');
    verifyDisabled('unpub-rating', 'fun');
    verifyDisabled('unpub-discreet-answer', 'predictable');
    verifyDisabled('sl-textarea', 'predictableWhy');
    verifyDisabled('unpub-discreet-answer', 'playAgain');
    verifyDisabled('unpub-discreet-answer', 'buy');
    verifyDisabled('sl-textarea', 'oneChange');
    verifyDisabled('sl-textarea', 'favoritePart');
    verifyDisabled('sl-textarea', 'additionalComments');
    verifyDisabled('sl-checkbox', 'postAnonymous');
    verifyDisabled('sl-checkbox', 'shareEmail');
  });
});
