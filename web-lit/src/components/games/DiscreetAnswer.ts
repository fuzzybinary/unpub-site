import { css, html, LitElement, PropertyValues } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { DiscreetAnswer } from 'models/Game';
import { stackStyles } from 'styles/styles';

import styles from './DiscreetAnswer.scss';

@customElement('unpub-discreet-answer')
export class DiscreetAnswerElement extends LitElement {
  static get externalStyles() {
    return [styles];
  }

  static get styles() {
    return [
      stackStyles,
      css`
        #label {
          font-size: var(--input-label-font-size, 0.75rem);
          top: var(--input-padding-top-bottom, 0.5rem);
          transform: translateY(0px);
        }

        :host {
          --stack-margin: var(--s-1);
        }
      `
    ];
  }

  @property({ type: Boolean }) public required: boolean = false;
  @property({ type: Boolean }) public disabled: boolean = false;
  @property() public label: string = '';
  @property() public name: string = '';

  private _value?: DiscreetAnswer;
  @property() public get value(): DiscreetAnswer | undefined {
    return this._value;
  }

  public set value(value: DiscreetAnswer | undefined) {
    const oldValue = this.value;
    this._value = value;
    switch (value) {
      case 'Yes':
        const rdYes = this.querySelector(`#${this.name}_rdYes`) as HTMLInputElement;
        rdYes.checked = true;
        break;
      case 'Maybe':
        const rdMaybe = this.querySelector(`#${this.name}_rdMaybe`) as HTMLInputElement;
        rdMaybe.checked = true;
        break;
      case 'No':
        const rdNo = this.querySelector(`#${this.name}_rdNo`) as HTMLInputElement;
        rdNo.checked = true;
        break;
      default:
        this.querySelectorAll('.discreetAnswerRadio').forEach(input => {
          const rd = input as HTMLInputElement;
          rd.checked = false;
        });
        this._value = undefined;
    }
    this.requestUpdate('value', oldValue);
  }

  public render() {
    return html`
      <div class="stack-l">
        <div id="label">${this.label}</div>
        <div>
          <slot></slot>
        </div>
        <div class="cluster-l answerContent">
          <div>
            <input
              id="${this.name}_rdYes"
              class="discreetAnswerRadio"
              type="radio"
              value="Yes"
              name=${this.name}
              ?required=${this.required}
              ?disabled=${this.disabled}
              @change=${this._onChange}
            />
            <label for="${this.name}_rdYes">Yes</label>
            <input
              id="${this.name}_rdMaybe"
              type="radio"
              value="Maybe"
              name=${this.name}
              ?required=${this.required}
              ?disabled=${this.disabled}
              class="discreetAnswerRadio"
              @change=${this._onChange}
            />
            <label for="${this.name}_rdMaybe">
              Maybe
            </label>
            <input
              id="${this.name}_rdNo"
              type="radio"
              value="No"
              name=${this.name}
              ?required=${this.required}
              ?disabled=${this.disabled}
              @change=${this._onChange}
              class="discreetAnswerRadio"
            />
            <label for="${this.name}_rdNo">
              No
            </label>
          </div>
        </div>
      </div>
    `;
  }

  protected firstUpdated(props: PropertyValues) {
    super.firstUpdated(props);

    const cluster = this.shadowRoot?.querySelector('.answerContent');
    if (cluster) {
      this.appendChild(cluster);
    }
  }

  private _onChange(e: Event) {
    this.value = (e.currentTarget as HTMLInputElement).value as DiscreetAnswer;
  }
}
