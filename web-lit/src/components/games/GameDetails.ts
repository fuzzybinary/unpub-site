import 'components/shared/GameInfo';
import { LoadResult, unpubLoad } from 'components/shared/Loader';
import 'components/shared/Loader';
import { html, LitElement } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { GameDetailsPublic } from 'models/Game';
import { diConsumer, withLocation } from 'providers';
import { UnpubService } from 'services/UnpubService';
import { clusterStyles, linkButton, stackStyles } from 'styles/styles';

@customElement('unpub-game-details')
class GameDetails extends withLocation(diConsumer(LitElement)) {
  static get styles() {
    return [clusterStyles, stackStyles, linkButton];
  }
  private _unpubService: UnpubService | null = null;
  private _id: string | null = null;

  @state() private _gameDetails: GameDetailsPublic | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._unpubService = this.resolve(UnpubService);
    this._id = (this.location?.params.id as string) || null;
  }

  public render() {
    return unpubLoad(
      this._load.bind(this),
      html`
        <div id="gameDetails">
          <div id="gameDetails__gameInfo" class="stack-l">
            <div class="cluster-l" styles="--cluster-margin: var(--s-2);">
              <div>
                <h1>${this._gameDetails?.name}</h1>
                <sl-button
                  link
                  circle
                  id="submitFeedback"
                  href="/games/${this._gameDetails?.id}/feedback"
                >
                  <sl-icon name="chat-text-fill"></sl-icon>
                </sl-button>
              </div>
            </div>
            <div class="gameDetails__designer">
              Designed By: ${this._gameDetails?.owner.displayName}
            </div>
            <unpub-game-info .gameInfo=${this._gameDetails}></unpub-game-info>
          </div>
        </div>
      `
    );
  }

  private async _load() {
    if (this._id) {
      this._gameDetails = await this._unpubService!.fetchGame(this._id);
    }

    if (this._gameDetails) {
      return LoadResult.ok;
    }

    return LoadResult.noData;
  }
}
