import { Router } from '@vaadin/router';
import { html, LitElement } from 'lit';
import { customElement, state } from 'lit/decorators.js';

import { diProvider } from 'providers';
import { AuthService } from 'services/AuthService';
import { UnpubService } from 'services/UnpubService';

import 'components/auth/AuthDialog';
import 'components/auth/Login';
import 'components/auth/Register';
import 'components/auth/ResetPassword';
import 'components/FourOhFour';
import 'components/UnpubNav';

@customElement('unpub-app')
export default class UnpubApp extends diProvider(LitElement) {
  @state() private _isInitialized = false;

  private _authService!: AuthService;
  private _unpubService!: UnpubService;
  private _router: Router | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = new AuthService();
    this.register(AuthService, this._authService);

    this._unpubService = new UnpubService(this._authService);
    this.register(UnpubService, this._unpubService);
    this._initConfig();
  }

  public updated() {
    // Once things have updated and everythings initialized, setup the router
    if (this._isInitialized && !this._router) {
      const gameDetailsElement = document.createElement('unpub-private-game-details');

      const outlet = this.querySelector('outlet');
      this._router = new Router(outlet);
      this._router.setRoutes([
        {
          path: '/news(/.*)?',
          action: (ctx, commands) => {
            if (window.location.pathname !== ctx.pathname) {
              window.location.pathname = ctx.pathname;
            }
          },
          component: 'sl-spinner'
        },
        { path: '/', component: 'unpub-home' },
        { path: '/login', component: 'unpub-login-page' },
        {
          path: '/games',
          children: [
            { path: '/', component: 'unpub-games-list' },
            { path: '/:id/feedback', component: 'unpub-feedback-page' },
            { path: '/:id', component: 'unpub-game-details' }
          ]
        },
        { path: '/events', component: 'unpub-games' },
        { path: '/resetPassword', component: 'unpub-reset-password' },
        {
          path: '/me',
          children: [
            { path: '/', component: 'unpub-my-profile' },
            { path: '/games/:id', action: () => gameDetailsElement },
            { path: '/add_game', component: 'unpub-create-game' },
            { path: '/feedback/:id', component: 'unpub-feedback-details' }
          ]
        },
        { path: '(.*)', component: 'unpub-four-oh-four' }
      ]);
    }
  }

  public createRenderRoot() {
    return this;
  }

  public render() {
    return html`
      <unpub-nav></unpub-nav>
      ${this._isInitialized
        ? html`
            <div class="content">
              <outlet></outlet>
            </div>
          `
        : html`
            <sl-spinner></sl-spinner>
          `}
    `;
  }

  private async _initConfig() {
    await this._unpubService!.initConfig();
    this._isInitialized = true;
  }
}
