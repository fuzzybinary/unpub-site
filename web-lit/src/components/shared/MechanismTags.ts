export interface MechanismCategory {
  name: string;
  abrev: string;
  mechanisms: string[];
}

export const Mechanisms: MechanismCategory[] = [
  {
    name: 'Game Structure',
    abrev: 'STR',
    mechanisms: [
      'Competitive',
      'Cooperative',
      'Team-Based',
      'Solo',
      'Semi-Cooperative',
      'Single Loser',
      'Traitor',
      'Scenario/Mission/Campaign',
      'Score-and-Reset',
      'Legacy'
    ]
  },

  {
    name: 'Turn Order',
    abrev: 'TRN',
    mechanisms: [
      'Fixed Turn Order',
      'Stat Turn Order',
      'Bid Turn Order',
      'Progressive Turn Order',
      'Pass Order',
      'Real-Time',
      'Punctuated Real-Time',
      'Simultaneous Action Selection',
      'Role Order',
      'Random Turn Order',
      'Action Timer',
      'Time Track',
      'Passed Action Token',
      'Interleaved vs. Sequential Phases',
      'Lose a Turn',
      'Interrupts'
    ]
  },

  {
    name: 'Actions',
    abrev: 'ACT',
    mechanisms: [
      'Action Points',
      'Action Drafting',
      'Action Retrieval',
      'Action/Event',
      'Command Cards',
      'Action Queue',
      'Shared Action Queue',
      'Follow',
      'Order Counters',
      'Rondel',
      'Action Selection Restrictions',
      'Variable Player Powers',
      'Once-Per-Game Abilities',
      'Advantage Token',
      'Gating and Unlocking',
      'Tech Trees/Tech Tracks/Track Bonusus',
      'Events',
      'Narrative Choice'
    ]
  },

  {
    name: 'Resolution',
    abrev: 'RES',
    mechanisms: [
      'High Number',
      'Stat Check',
      'Critical Hits and Failures',
      'Ratio/Combat Results Table',
      'Die Icons',
      'Card Play',
      'Rock, Paper, Scissors',
      "Prisoner's Dilemma",
      'Alternate Removal',
      'Physical Action',
      'Static Capture',
      'Enclosure',
      'Minimap',
      'Force Commitment',
      'Voting',
      'Player Judge',
      'Targeted Clues',
      'Tie-Breakers',
      'Dice Selection',
      'Action Speed',
      'Rerolling and Lcoking',
      'Kill Steal'
    ]
  },

  {
    name: 'Game End and Victory',
    abrev: 'VIC',
    mechanisms: [
      'Victory Points from Game State',
      'Victory Points from Player Actions',
      'Temporary and Permanent Victory Points',
      'Victory Points as a Resource',
      'Hidden and Exposed Victory Points',
      'End-Game Bonuses',
      'Race',
      'Player Elimination',
      'Fixed Number of Rounds',
      'Exhausting Resources',
      'Completing Targets',
      'Fixed Number of Events',
      'Elapsed Real Time',
      'Connecitons',
      'Circuit Breaker/Sudden Death',
      'Finale',
      'King of the Hill',
      'Catch the Leader',
      'Tug of War',
      'Highest Lowest'
    ]
  },

  {
    name: 'Uncertainty',
    abrev: 'UNC',
    mechanisms: [
      'Betting and Bluffing',
      'Push-Your-Luck',
      'Memory',
      'Hidden Roles',
      'Roles with Asymmetric Information',
      'Communicaiton Limits',
      'Unknown Information',
      'Hidden Information',
      'Probability Management',
      'Variable Setup',
      'Hidden Control'
    ]
  },

  {
    name: 'Economics',
    abrev: 'ECO',
    mechanisms: [
      'Exchanging',
      'Trading',
      'Market',
      'Delayed Purchase',
      'Income',
      'Automatic Resource Growth',
      'Loans',
      'Always Available Purchases',
      'I Cut, You Choose',
      'Discounts',
      'Upgrades',
      'Random Production',
      'Investment',
      'Ownership',
      'Contracts',
      'Bribery',
      'Increase Value of Unchosen Resources',
      'Negotiation',
      'Alliances'
    ]
  },

  {
    name: 'Auctions',
    abrev: 'AUC',
    mechanisms: [
      'Open Auction',
      'English Auction',
      'Turn Order Until Pass Auction',
      'Sealed-Bid Auction',
      'Sealed Bid with Cancellation',
      'Constrained Bidding',
      'Once-Around Auction',
      'Dutch Auction',
      'Second-Bid Auction',
      'Selection Order Bid',
      'Multiple-Lot Auction',
      'Closed-Economy Auction',
      'Reverse Auction',
      'Dexterity Auction',
      'Fixed-Placement Auction',
      'Dutch Priority Auction'
    ]
  },

  {
    name: 'Worker Placement',
    abrev: 'WPL',
    mechanisms: [
      'Standard Worker Placement',
      'Workers of Differing Types',
      'Acquiring and Losing Workers',
      'Workers-As-Dice',
      'Adding and Blocking Buildings',
      'Single Workers',
      'Building Actions and Rewards',
      'Turn Order and Resolution Order'
    ]
  },

  {
    name: 'Movement',
    abrev: 'MOV',
    mechanisms: [
      'Tessellation',
      'Roll and Move',
      'Pattern Movement',
      'Movement Points',
      'Resource to Move',
      'Measurement',
      'Different Dice',
      'Drift',
      'Impulse',
      'Programmed Movement',
      'Relative Position',
      'Mancala',
      'Chaining',
      'Bias',
      'Moving Multiple Units',
      'Map Addition',
      'Map Reduction',
      'Map Deformation',
      'Move Through Deck',
      'Movement Template',
      'Pieces as Map',
      'Multiple Maps',
      'Shortcuts',
      'Hidden Movement'
    ]
  },

  {
    name: 'Area Control',
    abrev: 'ARC',
    mechanisms: [
      'Absolute Control',
      'Area Majority/Influence',
      'Troop Types',
      'Territories and Regions',
      'Area Parameters',
      'Force Projection',
      'Zone of Control',
      'Line of Sight'
    ]
  },

  {
    name: 'Set Collection',
    abrev: 'SET',
    mechanisms: [
      'Set Valuation',
      'Tile-Laying',
      'Grid Coverage',
      'Network Building',
      'Combo Abilities'
    ]
  },

  {
    name: 'Card Mechanisms',
    abrev: 'CAR',
    mechanisms: [
      'Trick Taking',
      'Ladder Climbing',
      'Melding and Splaying',
      'Card Draw, Limits and Deck Exhaustion',
      'Deck Building',
      'Drafting'
    ]
  }
];
