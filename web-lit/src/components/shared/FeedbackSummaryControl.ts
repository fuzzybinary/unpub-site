import { html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { discreetAnswerIcon, FeedbackItemSummary } from 'models/Game';

import styles from './FeedbackSummaryControl.scss';

@customElement('unpub-feedback-summary')
class ElementName extends LitElement {
  static get styles() {
    return [styles];
  }

  @property({ type: Boolean }) public showGame: boolean = false;
  @property({ attribute: false }) public feedback: FeedbackItemSummary[] | null = null;

  public render() {
    const formatter = new Intl.DateTimeFormat('default', {
      weekday: 'long',
      month: 'numeric',
      day: 'numeric',
      year: 'numeric',
      hour: 'numeric',
      minute: 'numeric'
    });
    const formatCreatedDate = (dateString: string) => {
      const date = new Date(dateString);
      return formatter.format(date);
    };

    return html`
      <table style="width: 100%">
        <thead>
          <tr>
            <th>Date</th>
            ${this.showGame
              ? html`
                  <th>Game</th>
                `
              : ''}
            <th class="data">Len</th>
            <th class="data">EoL</th>
            <th class="data">DT</th>
            <th class="data">Dec</th>
            <th class="data">Int</th>
            <th class="data">Org</th>
            <th class="data">Fun</th>
            <th class="data">Agn</th>
            <th class="data">
              <sl-icon library="material" name="attach_money"></sl-icon>
            </th>
          </tr>
        </thead>
        ${this.feedback?.map(fb => {
          return html`
            <tbody class="spacer">
              <tr>
                <td><div style="height: var(--s-1)"></div></td>
              </tr>
            </tbody>
            <tbody class="feedbackRow">
              <tr>
                <td>
                  <a href="/me/feedback/${fb.id}">${formatCreatedDate(fb.created)}</a>
                </td>
                ${this.showGame
                  ? html`
                      <td>
                        <a href="/games/${fb.gameId}">${fb.gameName}</a>
                      </td>
                    `
                  : ''}
                <td class="data">${fb.gameLength}</td>
                <td class="data">${fb.easeOfLearning}</td>
                <td class="data">${fb.downTime}</td>
                <td class="data">${fb.decisions}</td>
                <td class="data">${fb.interactivity}</td>
                <td class="data">${fb.originality}</td>
                <td class="data">${fb.fun}</td>
                <td class="data">
                  ${discreetAnswerIcon(fb.playAgain)}
                </td>
                <td class="data">
                  ${discreetAnswerIcon(fb.buy)}
                </td>
              </tr>
              <tr>
                <td colspan="11">
                  <div class="comments">
                    <div class="comments__title">Comments</div>
                    <div class="comments__body">${fb.comments}</div>
                  </div>
                </td>
              </tr>
            </tbody>
          `;
        })}
      </table>
    `;
  }
}
