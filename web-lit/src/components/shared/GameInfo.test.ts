import { expect, fixture, html } from '@open-wc/testing';
import { GameDetails } from 'models/Game';

import './GameInfo';

describe('GameInfo', () => {
  let el: Element;

  beforeEach(async () => {
    const info: GameDetails = {
      id: 'fake-id',
      name: 'My Awesome Game',
      time: '20-30',
      minAge: 8,
      minPlayers: 3,
      maxPlayers: 6,
      mechanisms: ['Mechanism 1', 'Mechanism 2'],
      description: 'The description of the game that is awesome.'
    };

    el = await fixture(html`
      <unpub-game-info .gameInfo=${info}></unpub-game-info>
    `);
  });

  describe('rendering', () => {
    it('renders default avatar', () => {
      const avatar = el.shadowRoot?.querySelector('.sidebar__avatar');
      const img = avatar?.querySelector('img');
      expect(img).to.be.ok;
      expect(img?.getAttribute('src')).to.equal('/images/game_placeholder.jpg');
    });

    it('renders mechanisms', () => {
      const mechanisms = el.shadowRoot?.querySelector('.sidebar__mechanisms');
      expect(mechanisms).to.be.ok;

      const listElement = mechanisms?.querySelector('ul');
      expect(listElement).to.be.ok;
      expect(listElement?.children.length).to.equal(2);
      expect(listElement?.children[0].textContent?.trim()).to.equal('Mechanism 1');
      expect(listElement?.children[1].textContent?.trim()).to.equal('Mechanism 2');
    });

    it('renders an info bar with info', async () => {
      const infoBar = el.shadowRoot?.querySelector('.infoBar');
      expect(infoBar).to.be.ok;

      const checkInfo = (id: string, label: string, content: string) => {
        const item = infoBar!.querySelector(`#${id}`);
        const labelElement = item!.querySelector('.infoBar__label') as HTMLElement;
        expect(labelElement.innerText.toLowerCase()).to.equal(label.toLowerCase());

        const contentElement = item!.querySelector('.infoBar__property') as HTMLElement;
        expect(contentElement.innerText).to.equal(content);
      };

      checkInfo('playersInfo', 'Players:', '3-6');
      checkInfo('agesInfo', 'Ages:', '8+');
      checkInfo('timeInfo', 'Time:', '20-30');
    });

    it('renders the game description', async () => {
      const description = el.shadowRoot?.querySelector('.description');
      expect(description).to.be.ok;
      expect(description?.textContent?.trim()).to.equal('The description of the game that is awesome.');
    });
  });
});
