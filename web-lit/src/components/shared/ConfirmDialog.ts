import { SlButton, SlDialog } from '@shoelace-style/shoelace';
import { html, LitElement } from 'lit';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import { customElement, property, query, state } from 'lit/decorators.js';

@customElement('unpub-confirm-dialog')
export class ConfirmDialog extends LitElement {
  // Public queries for testing purposes
  @query('#okButton') public okButton!: SlButton;
  @query('#cancelButton') public cancelButton!: SlButton;

  @property({ attribute: false }) public operation: (() => Promise<string[] | null>) | null = null;
  @property() public label: string | undefined;

  @state() private _performingOperation: boolean = false;
  @state() private _lastErrors: string[] = [];

  @query('#internalDialog') private _internalDialog!: SlDialog;

  public get open() {
    return this._internalDialog.open;
  }

  public show() {
    this._internalDialog.show();
  }

  public render() {
    return html`
      <sl-dialog
        id="internalDialog"
        label=${ifDefined(this.label)}
        ?no-header=${this.label === undefined}
      >
        <slot></slot>
        <div class="errorMessages">
          ${this._lastErrors.length > 0
            ? `${this._lastErrors.reduce((p, c) => `${p}${c} `, '')}`
            : ''}
        </div>
        <div slot="footer" class="cluster-l">
          <div>
            <sl-button
              id="okButton"
              ?loading=${this._performingOperation}
              @click="${this._onOk}"
              type="primary"
            >
              Okay
            </sl-button>
            <sl-button
              id="cancelButton"
              ?disabled=${this._performingOperation}
              @click="${this._onCancel}"
            >
              Cancel
            </sl-button>
          </div>
        </div>
      </sl-dialog>
    `;
  }

  private _onCancel() {
    this._lastErrors = [];
    this._internalDialog.hide();
  }

  private async _onOk() {
    this._lastErrors = [];
    let result: string[] | null = null;
    this._performingOperation = true;
    if (this.operation) {
      result = await this.operation();
    }
    this._performingOperation = false;
    if (!result) {
      this.dispatchEvent(new Event('confirmcomplete'));
      this._internalDialog.hide();
    } else {
      this._lastErrors = result;
    }
  }
}
