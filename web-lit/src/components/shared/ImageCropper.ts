import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';

export interface Rectangle {
  top: number;
  left: number;
  width: number;
  height: number;
}

@customElement('unpub-image-cropper')
export class ImageCropper extends LitElement {
  public static get styles() {
    return css`
      :host {
        width: 400px;
        margin-left: auto;
        margin-right: auto;
        border: 2px solid blue;
        display: flex;
        flex-direction: column;
      }

      .cropWrapper {
        position: relative;
      }

      .cropContainer {
        user-select: none;
        -ms-user-select: none;
        -moz-user-select: none;
        -webkit-user-select: none;
        position: absolute;
        overflow: hidden;
        z-index: 1;
        top: 0;
        width: 100%;
        height: 100%;
        box-sizing: border-box;
      }

      .cropContainer img {
        display: block;
      }

      .handle {
        position: absolute;
        display: block;
        z-index: 999;
      }

      .handle:before {
        position: absolute;
        padding: 4px;
        transform: translate(-50%, -50%);
        content: ' ';
        background: rgba(222, 60, 80, 0.9);
        border: 1px solid #767676;
      }

      .nw {
        top: 0;
        left: 0;
        cursor: nwse-resize;
      }

      .sw {
        bottom: 0;
        left: 0;
        cursor: nesw-resize;
      }

      .ne {
        top: 0;
        right: 0;
        cursor: nesw-resize;
      }

      .se {
        bottom: 0;
        right: 0;
        cursor: nwse-resize;
      }

      .cropBox {
        position: absolute;
        border: 1px dashed #fff;
        box-sizing: border-box;
        cursor: move;
      }

      .cropOutline {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        outline: 600px solid rgba(0, 0, 0, 0.3);
      }
    `;
  }

  @property() public imgSrc: string = '';
  public get cropRect() {
    const image = this.shadowRoot?.getElementById('cropImage') as HTMLImageElement;
    const scale = image.naturalHeight / image.clientHeight;
    return {
      top: Math.floor(this._top * scale),
      left: Math.floor(this._left * scale),
      width: Math.floor(this._width * scale),
      height: Math.floor(this._height * scale)
    };
  }

  @property() private _top: number = 0;
  @property() private _left: number = 0;
  @property() private _width: number = 400;
  @property() private _height: number = 400;

  private _currentResizeHandle: string | null = null;
  private _moveStartPoints = {
    startPoint: { left: 0, top: 0 },
    mouseDown: { x: 0, y: 0 }
  };
  private _mouseMoveFunc: any;
  private _mouseUpFunc: any;

  public render() {
    return html`
      <div class="cropWrapper">
        <img id="cropImage" width="100%" src=${this.imgSrc} @load=${this._loaded} />
        <div class="cropContainer">
          <div
            class="cropBox"
            style="top: ${this._top}px; left: ${this._left}px; 
              width: ${this._width}px; height: ${this._height}px;"
            @mousedown="${this._onCropBoxDown}"
          >
            <div class="cropOutline"></div>
            <span class="handle nw" data-handle="nw" @mousedown=${this._onResizeHandleDown}></span>
            <span class="handle ne" data-handle="ne" @mousedown=${this._onResizeHandleDown}></span>
            <span class="handle se" data-handle="se" @mousedown=${this._onResizeHandleDown}></span>
            <span class="handle sw" data-handle="sw" @mousedown=${this._onResizeHandleDown}></span>
          </div>
        </div>
      </div>
    `;
  }

  private _loaded(e: Event) {
    const image = this.shadowRoot?.getElementById('cropImage') as HTMLImageElement;
    const scale = image.naturalHeight / image.clientHeight;
    const minSize = Math.min(image.naturalWidth / scale, image.naturalHeight / scale);
    this._top = 0;
    this._left = 0;
    this._width = minSize;
    this._height = minSize;
  }

  private _onCropBoxDown(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();

    const container = this.shadowRoot?.querySelector('.cropBox')!;

    const point = {
      x: e.clientX || e.pageX,
      y: e.clientY || e.pageY
    };
    const clientRect = container.getBoundingClientRect();
    this._moveStartPoints = {
      startPoint: {
        left: this._left,
        top: this._top
      },
      mouseDown: {
        x: point.x,
        y: point.y
      }
    };

    this._mouseMoveFunc = this._onMove.bind(this);
    this._mouseUpFunc = this._onEndMove.bind(this);

    document.addEventListener('mousemove', this._mouseMoveFunc);
    document.addEventListener('mouseup', this._mouseUpFunc);
  }

  private _onEndMove(e: MouseEvent) {
    e.stopPropagation();

    document.removeEventListener('mousemove', this._mouseMoveFunc);
    document.removeEventListener('mouseup', this._mouseUpFunc);
  }

  private _onMove(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();

    const point = {
      x: Math.floor(e.clientX || e.pageX),
      y: Math.floor(e.clientY || e.pageY)
    };

    this._left = Math.floor(
      this._moveStartPoints.startPoint.left + (point.x - this._moveStartPoints.mouseDown.x)
    );
    this._top = Math.floor(
      this._moveStartPoints.startPoint.top + (point.y - this._moveStartPoints.mouseDown.y)
    );

    // Don't let the box go beyond the containr
    if (this._left < 0) {
      this._left = 0;
    }
    if (this._top < 0) {
      this._top = 0;
    }

    const container = this.shadowRoot?.querySelector('.cropContainer');
    if (this._left + this._width > container!.clientWidth) {
      this._left = container!.clientWidth - this._width;
    }

    if (this._top + this._height > container!.clientHeight) {
      this._top = container!.clientHeight - this._height;
    }
  }

  private _onResizeHandleDown(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();

    this._currentResizeHandle = (e.currentTarget as HTMLElement).dataset.handle ?? null;

    this._mouseMoveFunc = this._onResize.bind(this);
    this._mouseUpFunc = this._onEndResize.bind(this);

    document.addEventListener('mousemove', this._mouseMoveFunc);
    document.addEventListener('mouseup', this._mouseUpFunc);
  }

  private _onEndResize(e: MouseEvent) {
    e.stopPropagation();

    document.removeEventListener('mousemove', this._mouseMoveFunc);
    document.removeEventListener('mouseup', this._mouseUpFunc);
  }

  private _onResize(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();

    const container = this.shadowRoot?.querySelector('.cropContainer')!;

    let point = {
      x: e.clientX || e.pageX,
      y: e.clientY || e.pageY
    };
    const clientRect = container.getBoundingClientRect();
    point = {
      x: Math.round(point.x - clientRect.left),
      y: Math.round(point.y - clientRect.top)
    };

    const boxRect = {
      top: this._top,
      left: this._left,
      bottom: this._top + this._height,
      right: this._left + this._width
    };

    let size: number = 0;
    switch (this._currentResizeHandle) {
      case 'nw':
        boxRect.top = point.y > 0 ? point.y : 0;
        boxRect.left = point.x > 0 ? point.x : 0;
        size = Math.min(boxRect.right - boxRect.left, boxRect.bottom - boxRect.top);
        boxRect.top = boxRect.bottom - size;
        boxRect.left = boxRect.right - size;
        break;
      case 'ne':
        boxRect.top = point.y > 0 ? point.y : 0;
        boxRect.right = point.x < clientRect.width ? point.x : clientRect.width;
        size = Math.min(boxRect.right - boxRect.left, boxRect.bottom - boxRect.top);
        boxRect.top = boxRect.bottom - size;
        boxRect.right = boxRect.left + size;
        break;
      case 'sw':
        boxRect.bottom = point.y < clientRect.height ? point.y : clientRect.height;
        boxRect.left = point.x < 0 ? point.x : 0;
        size = Math.min(boxRect.right - boxRect.left, boxRect.bottom - boxRect.top);
        boxRect.bottom = boxRect.top + size;
        boxRect.left = boxRect.right - size;
        break;
      case 'se':
        boxRect.bottom = point.y < clientRect.height ? point.y : clientRect.height;
        boxRect.right = point.x < clientRect.width ? point.x : clientRect.width;
        size = Math.min(boxRect.right - boxRect.left, boxRect.bottom - boxRect.top);
        boxRect.bottom = boxRect.top + size;
        boxRect.right = boxRect.left + size;
        break;
    }
    this._left = Math.round(boxRect.left);
    this._top = Math.round(boxRect.top);
    this._width = Math.round(boxRect.right - boxRect.left);
    this._height = Math.round(boxRect.bottom - boxRect.top);
  }
}
