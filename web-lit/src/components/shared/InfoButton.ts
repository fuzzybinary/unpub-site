import { SlDialog } from '@shoelace-style/shoelace';
import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('unpub-info-button')
class InfoButton extends LitElement {
  public render() {
    return html`
      <sl-icon-button
        @click=${this._onClick}
        size="small"
        class="infoButton"
        label="info"
        name="info-circle-fill"
      >
      </sl-icon-button>
      <sl-dialog id="infoDialog" no-header>
        <slot></slot>
      </sl-dialog>
    `;
  }

  private _onClick() {
    const dialog = this.shadowRoot?.getElementById('infoDialog') as SlDialog;
    dialog.show();
  }
}
