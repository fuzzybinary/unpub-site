import { html } from 'lit';
import { TemplateResult } from 'lit-html';
import { directive } from 'lit-html/directive.js';
import { AsyncDirective } from 'lit/async-directive.js';

export enum LoadResult {
  ok,
  noData,
  error
}

const renderResult = (result: LoadResult | null, value: unknown, go: () => TemplateResult) => {
  let renderValue;
  switch (result) {
    case LoadResult.ok:
      renderValue = value;
      break;
    case LoadResult.error:
      renderValue = html`
        <div class="errorMessages">
          There was an issue loading this content.
        </div>
        <sl-button
          name="retry"
          type="primary"
          @click=${() => {
            go();
          }}
        >
          Retry
        </sl-button>
      `;
      break;
    case LoadResult.noData:
      renderValue = html`
        <unpub-four-oh-four></unpub-four-oh-four>
      `;
      break;
  }
  return renderValue;
};

class UnpubLoadDirective extends AsyncDirective {
  private loadResult: LoadResult | null | undefined;

  public render(loader: () => Promise<LoadResult>, value: unknown, needsForceRefresh?: boolean) {
    const go = () => {
      if (this.loadResult) {
        this.setValue(html`
          <sl-spinner></sl-spinner>
        `);
      }
      this.loadResult = null;

      const promise = loader();
      Promise.resolve(promise).then(result => {
        this.loadResult = result;
        this.setValue(renderResult(result, value, go));
      });

      return html`
        <sl-spinner></sl-spinner>
      `;
    };

    if (this.loadResult == undefined || needsForceRefresh) {
      return go();
    }

    return renderResult(this.loadResult, value, go);
  }
}

export const unpubLoad = directive(UnpubLoadDirective);
