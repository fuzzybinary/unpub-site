import { expect, fixture, nextFrame, waitUntil } from '@open-wc/testing';
import { SlButton, SlDialog } from '@shoelace-style/shoelace';
import { html } from 'lit/static-html.js';
import sinon from 'sinon';

import './ConfirmDialog';
import { ConfirmDialog } from './ConfirmDialog';

describe('ConfirmDialog', () => {
  afterEach(() => {
    sinon.reset();
  });

  it('should have an internal dialog', async () => {
    const el = await fixture(html`
      <unpub-confirm-dialog></unpub-confirm-dialog>
    `);

    const internalDialog = el.shadowRoot?.getElementById('internalDialog') as SlDialog;
    expect(internalDialog).to.be.ok;
    expect(internalDialog.open).to.be.false;
  });

  it('calling show opens dialog', async () => {
    const el = (await fixture(html`
      <unpub-confirm-dialog></unpub-confirm-dialog>
    `)) as ConfirmDialog;

    el.show();
    await nextFrame();

    const internalDialog = el.shadowRoot?.getElementById('internalDialog') as SlDialog;
    expect(internalDialog.open).to.be.true;
  });

  it('open property is passed from dialog', async () => {
    const el = (await fixture(html`
      <unpub-confirm-dialog></unpub-confirm-dialog>
    `)) as ConfirmDialog;

    expect(el.open).to.be.false;

    el.show();
    await nextFrame();

    expect(el.open).to.be.true;
  });

  it('should display okay and cancel buttons', async () => {
    const el = (await fixture(html`
      <unpub-confirm-dialog></unpub-confirm-dialog>
    `)) as ConfirmDialog;
    el.show();
    await nextFrame();

    const okayButton = el.shadowRoot?.getElementById('okButton');
    expect(okayButton).to.be.ok;

    const cancelButton = el.shadowRoot?.getElementById('cancelButton');
    expect(cancelButton).to.be.ok;
  });

  it('should hide on cancel', async () => {
    const el = (await fixture(html`
      <unpub-confirm-dialog></unpub-confirm-dialog>
    `)) as ConfirmDialog;
    el.show();
    await nextFrame();

    const cancelButton = el.shadowRoot?.getElementById('cancelButton');
    cancelButton?.click();
    await waitUntil(() => !el.open, 'Waiting for dialog to close', { timeout: 500 });

    expect(el.open).to.be.false;
  });

  it('should perform operation on okay', async () => {
    let resolvePromise;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = (await fixture(html`
      <!-- @ts-ignore -->
      <unpub-confirm-dialog .operation=${fake}></unpub-confirm-dialog>
    `)) as ConfirmDialog;
    el.show();
    await nextFrame();

    const okButton = el.shadowRoot?.getElementById('okButton');
    okButton?.click();
    await nextFrame();

    expect(fake.called).to.be.true;
  });

  it('should disable while perorming the operation', async () => {
    let resolvePromise!: (result: string[] | null) => void;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = (await fixture(html`
      <!-- @ts-ignore -->
      <unpub-confirm-dialog .operation=${fake}></unpub-confirm-dialog>
    `)) as ConfirmDialog;
    el.show();
    await nextFrame();

    const okButton = el.shadowRoot?.getElementById('okButton') as SlButton;
    const cancelButton = el.shadowRoot?.getElementById('cancelButton') as SlButton;

    okButton?.click();
    await nextFrame();

    expect(okButton.loading).to.be.true;
    expect(cancelButton.disabled).to.be.true;
  });

  it('successful resolve closes dialog, posts event', async () => {
    let resolvePromise!: (result: string[] | null) => void;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));
    const fakeComplete = sinon.fake();

    const el = (await fixture(html`
      <!-- @ts-ignore -->
      <unpub-confirm-dialog
        .operation=${fake}
        @confirmcomplete=${fakeComplete}
      ></unpub-confirm-dialog>
    `)) as ConfirmDialog;
    el.show();
    await nextFrame();

    const okButton = el.shadowRoot?.getElementById('okButton') as SlButton;

    okButton?.click();
    await nextFrame();

    resolvePromise(null);
    await waitUntil(() => !el.open, 'Waiting for dialog to close', { timeout: 500 });

    expect(el.open).to.be.false;
    expect(fakeComplete.called).to.be.true;
  });

  it('error on shows messages and renables buttons', async () => {
    let resolvePromise: ((result: string[] | null) => void) | null = null;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = (await fixture(html`
      <!-- @ts-ignore -->
      <unpub-confirm-dialog .operation=${fake}></unpub-confirm-dialog>
    `)) as ConfirmDialog;
    el.show();
    await nextFrame();

    const okButton = el.shadowRoot?.getElementById('okButton') as SlButton;
    const cancelButton = el.shadowRoot?.getElementById('cancelButton') as SlButton;

    okButton?.click();
    resolvePromise!(['Error Message']);
    await nextFrame();

    expect(okButton.disabled).to.be.false;
    expect(cancelButton.disabled).to.be.false;
    const messages = el.shadowRoot?.querySelector('.errorMessages');
    expect(messages?.textContent?.trim()).to.equal('Error Message');
  });
});
