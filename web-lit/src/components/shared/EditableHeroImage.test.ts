import { expect, fixture, html, nextFrame, waitUntil } from '@open-wc/testing';
import { SlAlert, SlDialog } from '@shoelace-style/shoelace';
import sinon from 'sinon';

import './EditableHeroImage';
import { EditableHeroImage } from './EditableHeroImage';

describe('EditableHeroImage', () => {
  describe('rendering', () => {
    it('renders an image', async () => {
      const el = await fixture(html`
        <unpub-editable-hero imageSrc="/avatar/url"></unpub-editable-hero>
      `);

      const img = el.shadowRoot?.querySelector('img') as HTMLImageElement;
      expect(img).to.be.ok;
      expect(img.getAttribute('src')).to.equal('/avatar/url');
    });

    it('renders a hidden file input', async () => {
      const el = await fixture(
        html`
          <unpub-editable-hero></unpub-editable-hero>
        `
      );

      const input = el.shadowRoot?.querySelector('input[type="file"]') as HTMLInputElement;
      expect(input).to.be.ok;
      expect(input.hidden).to.be.true;
    });

    it('renders a crop dialog', async () => {
      const el = await fixture(html`
        <unpub-editable-hero></unpub-editable-hero>
      `);

      const dialog = el.shadowRoot?.querySelector('sl-dialog');
      expect(dialog).to.be.ok;
      const cropper = dialog?.querySelector('unpub-image-cropper');
      expect(cropper).to.be.ok;
    });
  });

  describe('behaviors', async () => {
    let el: Element;

    beforeEach(async () => {
      const maxSize = 100 * 1024;
      el = await fixture(
        html`
          <unpub-editable-hero maxSize=${maxSize} imageSrc="/src/avatar.jpg"></unpub-editable-hero>
        `
      );
    });

    afterEach(() => {
      sinon.reset();
    });

    it('opens the crop dialog on file change', async () => {
      const fileInput = el.shadowRoot?.querySelector('input[type="file"]') as HTMLInputElement;

      const list = new DataTransfer();
      list.items.add(new File(['fake content'], 'test.jpg'));
      fileInput.files = list.files;
      fileInput.dispatchEvent(new Event('change'));

      const dialog = el.shadowRoot?.querySelector('sl-dialog') as SlDialog;
      await waitUntil(() => dialog.open, 'Waiting for dialog to open', { timeout: 500 });

      expect(dialog.open).is.true;
    });

    it('shows error for file that is too large', async () => {
      const fileInput = el.shadowRoot?.querySelector('input[type="file"]') as HTMLInputElement;
      const snackbar = el.shadowRoot?.querySelector('sl-alert') as SlAlert;

      const list = new DataTransfer();
      const fakeFile = new File(['fake content'], 'test.jpg');
      Object.defineProperty(fakeFile, 'size', { value: 200 * 1024 });
      list.items.add(fakeFile);
      fileInput.files = list.files;
      fileInput.dispatchEvent(new Event('change'));

      await nextFrame();

      expect(snackbar.open).to.be.true;
      expect(fileInput.files.length).to.equal(0);
    });

    it('too large file size is based on the component', async () => {
      const component = el as EditableHeroImage;
      component.maxSize = 500 * 1024;

      const fileInput = el.shadowRoot?.querySelector('input[type="file"]') as HTMLInputElement;

      const list = new DataTransfer();
      const fakeFile = new File(['fake content'], 'test.jpg');
      Object.defineProperty(fakeFile, 'size', { value: 200 * 1024 });
      list.items.add(fakeFile);
      fileInput.files = list.files;
      fileInput.dispatchEvent(new Event('change'));

      await nextFrame();

      const snackbar = el.shadowRoot?.querySelector('sl-alert') as SlAlert;
      expect(snackbar.open).to.be.false;
    });
  });
});
