import { SlAlert, SlDialog } from '@shoelace-style/shoelace';
import { css, html, LitElement } from 'lit';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import { customElement, property, query } from 'lit/decorators.js';
import { ImageCropper, Rectangle } from './ImageCropper';

export interface SourceChange {
  newUrl: string;
}

/**
 * @fires imageChanged - Fires with new file when the editor would like to change images
 */
@customElement('unpub-editable-hero')
export class EditableHeroImage extends LitElement {
  static get styles() {
    return css`
      .overlay {
        position: relative;
        width: 100%;
        height: 100%;
      }

      .overlay:hover img {
        opacity: 0.5;
        transition: opacity 0.1s linear;
      }

      #image {
        width: 100%;
        height: 100%;
      }

      .overlay .instructionText {
        display: none;
        position: absolute;
        top: 50%;
        width: 100%;
        text-align: center;
        color: white;
      }

      .overlay:hover .instructionText {
        display: block;
      }
    `;
  }

  @property() public imageSrc?: string = '';
  @property({ attribute: false }) public updateImage:
    | ((rect: Rectangle, image: File, buffer: string) => Promise<string | undefined>)
    | null = null;
  @property({ type: Number }) public maxSize: number = 5 * 1024 * 1024;

  @property() private _cropImage: string = '';
  @property() private _errorMessage: string = '';

  @query('#cropDialog') private _cropDialog!: SlDialog;
  @query('#errorSnackbar') private _errorSnackbar!: SlAlert;
  @query('#fileInput') private _fileInput!: HTMLInputElement;

  public get maxSizeString() {
    const kbSize = this.maxSize / 1024;
    if (kbSize > 1024) {
      return `${kbSize / 1024}MB`;
    }
    return `${kbSize}KB`;
  }

  public render() {
    return html`
      <div @dragover=${this._onDragEnter} @drop=${this._onDrop}>
        <div class="overlay" @click=${this._onUploadClick}>
          <img id="image" src=${ifDefined(this.imageSrc)} />
          <div class="instructionText">Upload</div>
        </div>
        <input id="fileInput" type="file" hidden @change=${this._onFileChanged} />
      </div>
      <sl-dialog id="cropDialog" label="Crop Image">
        <unpub-image-cropper id="cropper" imgSrc=${this._cropImage}></unpub-image-cropper>
        <div slot="footer">
          <sl-button id="okButton" @click="${this._onCropOkayClicked}" type="primary">
            Okay
          </sl-button>
          <div>${this._errorMessage}</div>
        </div>
      </sl-dialog>
      <sl-alert type="warning" id="errorSnackbar" duration="3000" closable>
        <sl-icon slot="icon" name="exclamation-triangle"></sl-icon>
        <strong>Image is too large</strong><br />
        Max size is ${this.maxSizeString}
      </sl-alert>
    `;
  }

  private _onUploadClick(e: Event) {
    this._fileInput.click();
  }

  private _onDragEnter(e: Event) {
    e.preventDefault();
    e.stopPropagation();
    const fileEvent = e as DragEvent;
    fileEvent.dataTransfer!.dropEffect = 'copy';
  }

  private _onDrop(e: Event) {
    e.preventDefault();
    e.stopPropagation();
    const fileEvent = e as DragEvent;
    const fileList = fileEvent.dataTransfer?.files;
    if (fileList?.length) {
      this._fileInput.files = fileList;
      this._onFileChanged(e);
    }
  }

  private _onFileChanged(e: Event) {
    const file = this._fileInput.files?.[0];
    if (file) {
      if (file.size > this.maxSize) {
        this._errorSnackbar.toast();
        this._fileInput.value = '';
        return;
      }

      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        if (reader.result) {
          const result: string =
            typeof reader.result === 'string'
              ? reader.result
              : Buffer.from(reader.result).toString();
          this._cropImage = result;
          this._cropDialog.show();
        }
      };
    }
  }

  private async _onCropOkayClicked(e: Event) {
    this._errorMessage = '';
    const cropper = this.shadowRoot?.getElementById('cropper') as ImageCropper;
    const cropRect = cropper.cropRect;

    const file = this._fileInput.files![0];
    try {
      const newUrl = await this.updateImage?.(cropRect, file, this._cropImage);
      this._cropDialog.hide();
      if (newUrl) {
        this.dispatchEvent(
          new CustomEvent<SourceChange>('imageChanged', { detail: { newUrl } })
        );
      }
    } catch (e) {
      this._errorMessage = (e as any).toString();
    }
  }
}
