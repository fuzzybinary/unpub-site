import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { SlButton } from '@shoelace-style/shoelace';
import sinon from 'sinon';

import './Loader';
import { LoadResult, unpubLoad } from './Loader';

describe('Loader', () => {
  afterEach(() => {
    sinon.reset();
  });

  it('should display a loading spinner', async () => {
    let resolvePromise!: (result: LoadResult) => void;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = await fixture(html`
      <div>
        ${unpubLoad(
          fake,
          html`
            <div id="slottedContent">
              I am slotted content
            </div>
          `
        )}
      </div>
    `);

    const spinner = el.querySelector('sl-spinner');
    expect(spinner).to.be.ok;
  });

  it('should call back to the loader', async () => {
    let resolvePromise!: (result: LoadResult) => void;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = await fixture(html`
      <div>
        ${unpubLoad(
          fake,
          html`
            <div id="slottedContent">
              I am slotted content
            </div>
          `
        )}
      </div>
    `);
    expect(fake.called).to.be.true;
    resolvePromise(LoadResult.ok);
  });

  it('should not display slotted content while loading', async () => {
    let resolvePromise!: (result: LoadResult) => void;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = await fixture(html`
      <div>
        ${unpubLoad(
          fake,
          html`
            <div id="slottedContent">
              I am slotted content
            </div>
          `
        )}
      </div>
    `);

    const slotted = el.querySelector('#slottedContent');
    expect(slotted).to.not.be.ok;
    resolvePromise(LoadResult.ok);
  });

  it('should display slotted content on success', async () => {
    let resolvePromise!: (result: LoadResult) => void;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = await fixture(html`
      <div>
        ${unpubLoad(
          fake,
          html`
            <div id="slottedContent">
              I am slotted content
            </div>
          `
        )}
      </div>
    `);

    resolvePromise(LoadResult.ok);
    await nextFrame();

    const slotted = el.querySelector('#slottedContent');
    expect(slotted).to.be.ok;
  });

  it('errors show on error with retry button', async () => {
    let resolvePromise!: (result: LoadResult) => void;
    const fake = sinon.fake.returns(new Promise(resolve => (resolvePromise = resolve)));

    const el = await fixture(html`
      <div>
        ${unpubLoad(
          fake,
          html`
            <div id="slottedContent">
              I am slotted content
            </div>
          `
        )}
      </div>
    `);

    resolvePromise(LoadResult.error);
    await nextFrame();

    const slotted = el.querySelector('#slottedContent');
    expect(slotted).to.not.be.ok;

    const errors = el.querySelector('.errorMessages');
    expect(errors).to.be.ok;
    const retryButton = el.querySelector('sl-button[name="retry"]');
    expect(retryButton).to.be.ok;
  });

  it('click retry button calls again, shows loading spinner', async () => {
    let resolvePromise!: (result: LoadResult) => void;
    const fake = sinon.fake.call(null, () => {
      return new Promise(resolve => (resolvePromise = resolve));
    });

    const el = await fixture(html`
      <div>
        ${unpubLoad(
          fake,
          html`
            <div id="slottedContent">
              I am slotted content
            </div>
          `
        )}
      </div>
    `);

    resolvePromise(LoadResult.error);
    await nextFrame();

    const retryButton = el.querySelector('sl-button[name="retry"]') as SlButton;
    retryButton.click();
    await nextFrame();

    expect(fake.callCount).to.equal(2);

    const spinner = el.querySelector('sl-spinner');
    expect(spinner).to.be.ok;
  });
});
