import { css, html, LitElement } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { GameDetails } from 'models/Game';
import { sidebarStyles, stackStyles } from 'styles/styles';

@customElement('unpub-game-info')
export class GameInfo extends LitElement {
  static get styles() {
    return [
      css`
        .infoBar {
          display: flex;
          border: 1px solid var(--unpub-shaded-border);
          background-color: var(--unpub-shaded);
          padding: 10px 30px 10px 30px;
          justify-content: space-between;
        }

        .infoBar__label {
          font-weight: bold;
          text-transform: uppercase;
        }
      `,
      stackStyles,
      sidebarStyles
    ];
  }

  @property({ attribute: false }) public gameInfo: GameDetails | null = null;

  public render() {
    return html`
      <div class="with-sidebar-l">
        <div>
          <div class="sidebar sidebar-intrinsic-l">
            <div class="sidebar__avatar">
              <img src=${this.gameInfo?.avatar ?? '/images/game_placeholder.jpg'} />
            </div>
            <div class="sidebar__mechanisms">
              <h3>Mechanisms</h3>
              <ul>
                ${this.gameInfo?.mechanisms.map(m => {
                  return html`
                    <li>${m}</li>
                  `;
                })}
              </ul>
            </div>
          </div>
          <div class="not-sidebar-l stack-l">
            <div class="infoBar">
              ${this.renderInfoItem(
                'playersInfo',
                'Players',
                `${this.gameInfo?.minPlayers}-${this.gameInfo?.maxPlayers}`
              )}
              ${this.renderInfoItem('agesInfo', 'Ages', `${this.gameInfo?.minAge}+`)}
              ${this.renderInfoItem('timeInfo', 'Time', `${this.gameInfo?.time}`)}
            </div>
            <div class="description">
              ${this.gameInfo?.description}
            </div>
          </div>
        </div>
      </div>
    `;
  }

  private renderInfoItem(id: string, label: string, content: string) {
    return html`
      <span id="${id}" class="infoBar__item">
        <span class="infoBar__label">${label}:</span>
        <span class="infoBar__property">${content}</span>
      </span>
    `;
  }
}
