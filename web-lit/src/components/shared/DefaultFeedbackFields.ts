export enum FieldType {
  text,
  textArea,
  number,
  checkbox,
  slider,
  discreetAnswer
}

export interface Field {
  name: string;
  label: string;
  type: FieldType;
  required?: boolean;
  jsonField?: string;
}

export interface FormSection {
  name: string;
  layout?: 'grid' | 'stack';
  fields: Field[];
}

export const feedbackForm: FormSection[] = [
  {
    name: 'Session Overview',
    fields: [
      {
        name: 'time',
        label: 'Time (in minutes)',
        type: FieldType.number,
        required: true
      },
      {
        name: 'players',
        label: 'Number of Players',
        type: FieldType.number,
        required: true
      },
      {
        name: 'firstPlayerScore',
        label: 'First Player Score',
        type: FieldType.number,
        required: true
      },
      {
        name: 'lastPlayerScore',
        label: 'Last Player Score',
        type: FieldType.number,
        required: true
      }
    ]
  },
  {
    name: 'Ratings',
    layout: 'grid',
    fields: [
      {
        name: 'gameLength',
        label: 'Game Length',
        required: true,
        type: FieldType.slider
      },
      {
        name: 'easeOfLearning',
        label: 'Ease of Learning',
        required: true,
        type: FieldType.slider
      },
      {
        name: 'downTime',
        label: 'Down Time',
        required: true,
        type: FieldType.slider
      },
      {
        name: 'decisions',
        label: 'Decisions',
        required: true,
        type: FieldType.slider
      },
      {
        name: 'interactivity',
        label: 'Interactivity',
        required: true,
        type: FieldType.slider
      },
      {
        name: 'originality',
        label: 'Originality',
        required: true,
        type: FieldType.slider
      },
      {
        name: 'fun',
        label: 'Fun / Enjoyable',
        required: true,
        type: FieldType.slider
      }
    ]
  },
  {
    name: 'Comments',
    fields: [
      {
        name: 'predictable',
        label: 'Was the end of the game predictable?',
        type: FieldType.discreetAnswer,
        required: true
      },
      {
        name: 'predictableWhy',
        label: 'If so, why?',
        type: FieldType.textArea
      },
      {
        name: 'playAgain',
        label: 'Would you play again?',
        type: FieldType.discreetAnswer,
        required: true
      },
      {
        name: 'buy',
        label: 'Would you buy this?',
        type: FieldType.discreetAnswer,
        required: true
      },
      {
        name: 'oneChange',
        label: 'What is one thing you would change?',
        type: FieldType.textArea,
        jsonField: 'changeOneThing'
      },
      {
        name: 'favoritePart',
        label: 'What was your favorite part of this game?',
        type: FieldType.textArea
      },
      {
        name: 'additionalComments',
        label: 'Any additional comments?',
        type: FieldType.textArea,
        jsonField: 'comments'
      }
    ]
  }
];
