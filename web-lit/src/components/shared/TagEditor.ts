import { css, html, LitElement } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { clusterStyles, stackStyles } from 'styles/styles';
import { Key } from 'ts-key-enum';
import { MechanismCategory, Mechanisms } from './MechanismTags';

import { SlInput } from '@shoelace-style/shoelace';

import styles from './TagEditor.scss';

@customElement('unpub-tag')
class TagElement extends LitElement {
  public static get styles() {
    return css`
      :host {
        display: block;
        background: var(--unpub-primary-light);
        padding: var(--s-4) var(--s-2);
        border: thin solid var(--unpub-primary-dark);
        color: white;
      }

      sl-icon {
        --icon-size: 1.5ch;
        cursor: pointer;
      }

      sl-icon:hover {
        background-color: var(--unpub-primary-dark);
      }
    `;
  }

  @property() public value!: string;

  public render() {
    return html`
      <slot></slot>
      <sl-icon @click=${this._onRemoveClick} name="x-square-fill"></sl-icon>
    `;
  }

  private _onRemoveClick() {
    this.dispatchEvent(
      new CustomEvent<string>('tagRemoved', {
        detail: this.value
      })
    );
  }
}

interface MechanismIndex {
  categoryIndex: number;
  mechanismIndex: number;
}

@customElement('unpub-tag-editor')
export class TagEditor extends LitElement {
  static get styles() {
    return [clusterStyles, stackStyles, styles];
  }

  @property() public label: string = '';
  @property({ attribute: false }) public value: string[] = [];
  @property({ type: Boolean }) public disabled: boolean = false;

  @query('.tagInput') public _tagInput!: SlInput;

  @state() private _dropdownShowing: boolean = false;
  @state() private _selectedMechanism: MechanismIndex | null = null;
  @state() private _filteredMechanisms: MechanismCategory[] = Mechanisms;

  public render() {
    return html`
      <div style="position: relative">
        <sl-input
          label=${this.label}
          class="tagInput"
          @keydown=${this._onKeyPress}
          @input=${this._onInput}
        >
          <div id="chips" slot="prefix" class="cluster-l" style="--cluster-margin: var(--s-2)">
            <div>
              ${this.value.map(tag => {
                return html`
                  <unpub-tag @tagRemoved=${this._removeMechanism} value=${tag}>${tag}</unpub-tag>
                `;
              })}
            </div>
          </div>
        </sl-input>
        <div class="mechanismListPopover" ?hidden=${!this._dropdownShowing}>
          ${this._filteredMechanisms.map((cat, catIndex) => {
            return html`
              <div>
                <div class="mechanismListPopover__category">${cat.name}</div>
                <div class="mechanismListPopover__list stack-l">
                  ${cat.mechanisms.map((mechanism, mechanismIndex) => {
                    return html`
                      <div
                        class="mechanismListPopover__item"
                        @click=${() => this._mechanismClick(mechanism)}
                        ?data-selected=${this._selectedMechanism?.categoryIndex === catIndex &&
                          this._selectedMechanism?.mechanismIndex === mechanismIndex}
                      >
                        ${mechanism}
                      </div>
                    `;
                  })}
                </div>
              </div>
            `;
          })}
        </div>
      </div>
    `;
  }

  private _addMechanism(mechanism: string) {
    if (!this.value.includes(mechanism)) {
      this.value.push(mechanism);
      this._selectedMechanism = null;
      this.requestUpdate('value');
      this._tagInput.value = '';
      this._onInput(null);
      this._tagInput.focus();
      this.dispatchEvent(new Event('change'));
    }
  }

  private _removeMechanism(event: CustomEvent<string>) {
    this.value = this.value.filter(tag => tag !== event.detail);
    this.dispatchEvent(new Event('change'));
  }

  private _onKeyPress(e: KeyboardEvent) {
    switch (e.key) {
      case Key.Backspace:
        const input = (e.target as SlInput)?.shadowRoot?.querySelector('input');
        if (input?.selectionStart === 0 && input?.selectionEnd === 0) {
          e.preventDefault();
          const lastTagValue = this.value.pop();
          if (lastTagValue) {
            this._tagInput.value = `${lastTagValue} ${this._tagInput.value}`;
            this._onInput(null);
            const caratPos = lastTagValue.length;
            input.setSelectionRange(caratPos, caratPos);
            this.requestUpdate('value');
          }
        }
        break;
      case Key.ArrowDown:
        if (this._filteredMechanisms.length > 0 && !this._dropdownShowing) {
          this._dropdownShowing = true;
        } else if (!this._selectedMechanism) {
          this._selectedMechanism = {
            categoryIndex: 0,
            mechanismIndex: 0
          };
          this.requestUpdate('_filteredMechanisms');
        } else {
          this._selectedMechanism.mechanismIndex++;
          if (
            this._filteredMechanisms[this._selectedMechanism.categoryIndex].mechanisms.length <=
            this._selectedMechanism.mechanismIndex
          ) {
            this._selectedMechanism.categoryIndex++;
            this._selectedMechanism.mechanismIndex = 0;
            if (this._filteredMechanisms.length <= this._selectedMechanism.categoryIndex) {
              this._selectedMechanism = {
                categoryIndex: 0,
                mechanismIndex: 0
              };
            }
          }
          this.requestUpdate('_filteredMechanisms');
        }
        break;
      case Key.ArrowUp:
        if (this._dropdownShowing) {
          if (!this._selectedMechanism) {
            this._selectedMechanism = {
              categoryIndex: this._filteredMechanisms.length - 1,
              mechanismIndex:
                this._filteredMechanisms[this._filteredMechanisms.length - 1].mechanisms.length - 1
            };
          } else {
            this._selectedMechanism.mechanismIndex--;
            if (this._selectedMechanism.mechanismIndex < 0) {
              this._selectedMechanism.categoryIndex--;
              this._selectedMechanism.mechanismIndex = 0;
              if (this._selectedMechanism.categoryIndex < 0) {
                this._selectedMechanism = {
                  categoryIndex: this._filteredMechanisms.length - 1,
                  mechanismIndex:
                    this._filteredMechanisms[this._filteredMechanisms.length - 1].mechanisms
                      .length - 1
                };
              }
            }
          }
          this.requestUpdate('_filteredMechanisms');
        }
        break;
      case Key.Enter:
        if (this._dropdownShowing && this._selectedMechanism) {
          const category = this._filteredMechanisms[this._selectedMechanism.categoryIndex];
          const mechanism = category.mechanisms[this._selectedMechanism.mechanismIndex];
          this._addMechanism(mechanism);
        }
        break;
    }
  }

  private _onInput(e: InputEvent | null) {
    const inputValue = this._tagInput.value.trim();
    if (inputValue && inputValue.length > 0) {
      this._filteredMechanisms = Mechanisms.reduce((memo: MechanismCategory[], category) => {
        const filteredResults = category.mechanisms.filter(mechanism =>
          mechanism.toLocaleLowerCase().includes(inputValue.toLocaleLowerCase())
        );
        if (filteredResults.length > 0) {
          memo.push({
            name: category.name,
            abrev: category.abrev,
            mechanisms: filteredResults
          });
        }
        return memo;
      }, []);

      if (this._filteredMechanisms.length > 0) {
        this._dropdownShowing = true;
      }
    } else {
      this._filteredMechanisms = Mechanisms;
      this._dropdownShowing = false;
    }
  }

  private _mechanismClick(mechanism: string) {
    this._addMechanism(mechanism);
  }
}
