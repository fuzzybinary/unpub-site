import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';

import 'components/games/FeedbackControl';
import 'components/shared/TagEditor';

@customElement('unpub-four-oh-four')
class FourOhFour extends LitElement {
  public render() {
    return html`
      <h2>404</h2>
      <div>I'm not sure what you're looking for, but it's not here.</div>
      <div style="width: 100%;">
        <unpub-tag-editor></unpub-tag-editor>
      </div>
    `;
  }
}
