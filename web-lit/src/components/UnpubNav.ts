import { SlDialog, SlMenuItem } from '@shoelace-style/shoelace';
import { Router } from '@vaadin/router';

import { html, LitElement } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { UserSummary } from 'models/UserModels';
import { diConsumer, diProvider } from 'providers';
import { AuthService } from 'services/AuthService';

@customElement('unpub-nav')
export class UnpubNav extends diProvider(diConsumer(LitElement)) {
  private _authService: AuthService | null = null;

  @state() private _authenticated: boolean = false;
  @state() private _userInfo: UserSummary | null = null;

  @query('#authDialog') private _authDialog!: SlDialog;

  private get _userAvatarUrl() {
    return this._userInfo?.avatarUrl ?? '/images/avatar_placeholder.png';
  }

  public createRenderRoot() {
    return this;
  }

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
    if (!this._authService) {
      // Okay to create this stand alone and provide to children
      this._authService = new AuthService();
      this.register(AuthService, this._authService);
    }
    this._authenticated = this._authService!.isAuthenticated();
    this._userInfo = this._authService!.userInfo;
    this._authService?.subscribeToAuthChanges(isAuthenticated =>
      this._onAuthChanged(isAuthenticated)
    );
  }

  public render() {
    const avatarUrl = this._userAvatarUrl;
    return html`
      <nav>
        <ul>
          <li>
            <a class="navItem" href="/">Home</a>
          </li>
          <li>
            <a class="navItem" href="/news">News</a>
          </li>
          <li>
            <a class="navItem" href="/games">Games</a>
          </li>
          <li>
            <a class="navItem" href="/events">Events</a>
          </li>
          ${this._authenticated
            ? html`
                <li id="profileMenuItem" style="margin-left: auto;">
                  <sl-dropdown placement="bottom-end">
                    <sl-button class="profileButton" slot="trigger" caret type="text">
                      <img id="profileImage" src=${avatarUrl} />
                    </sl-button>
                    <sl-menu @sl-select=${this._onMenuItemSelected}>
                      <sl-menu-item href="/me">
                        Profile
                      </sl-menu-item>
                      <sl-menu-item href="/news/ghost">
                        News Admin
                      </sl-menu-item>
                      <sl-menu-item id="logoutButton" @click=${this._onLogoutClicked}>
                        Logout
                      </sl-menu-item>
                    </sl-menu>
                  </sl-dropdown>
                </li>
              `
            : html`
                <li style="margin-left: auto;">
                  <sl-button id="login_button" type="primary" @click=${this._onLoginClicked}
                    >Login</sl-button
                  >
                </li>
              `}
        </ul>
      </nav>
      <sl-dialog id="authDialog" label="Login">
        <unpub-auth-dialog></unpub-auth-dialog>
      </sl-dialog>
    `;
  }

  private _onAuthChanged(isAuthenticated: boolean) {
    this._authenticated = isAuthenticated;
    this._userInfo = this._authService!.userInfo;
    this._authDialog.hide();
    this.requestUpdate('_userAvatarUrl');
  }

  private _onMenuItemSelected(e: CustomEvent<{ item: SlMenuItem }>) {
    const selectedLink = e.detail.item.getAttribute('href');
    if (selectedLink) {
      Router.go(selectedLink);
    }
  }

  private _onLogoutClicked(e: Event) {
    this._authService?.logout();
    Router.go('/');
  }

  private _onLoginClicked(e: Event) {
    this._authDialog.show();
  }
}
