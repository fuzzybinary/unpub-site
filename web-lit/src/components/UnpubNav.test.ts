import { fixture, html } from '@open-wc/testing';
import { SlButton } from '@shoelace-style/shoelace';
import { Router } from '@vaadin/router';
import { expect } from 'chai';
import sinon, { SinonStubbedInstance } from 'sinon';

// tslint:disable-next-line: prettier
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';

import './UnpubNav';

describe('UnpubNav', () => {
  describe('rendering', () => {
    let authMock: SinonStubbedInstance<AuthService> | null = null;

    const provideFake = (event: Event) => {
      const customEvent = event as CustomEvent<RequestDependencyEventDetail>;
      if (customEvent.detail.type.name === 'AuthService') {
        customEvent.detail.instance = authMock;
      }
    };

    beforeEach(() => {
      authMock = sinon.createStubInstance(AuthService);
      document.addEventListener(DependencyEventName, provideFake);
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideFake);
      sinon.restore();
    });

    it('Renders a nav element', async () => {
      authMock?.isAuthenticated.returns(false);

      const el = await fixture(html`
        <unpub-nav></unpub-nav>
      `);

      expect(el.querySelectorAll('nav').length).to.equal(1);
    });

    it('Renders a login button when not authenticated', async () => {
      authMock?.isAuthenticated.returns(false);

      const el = await fixture(html`
        <unpub-nav></unpub-nav>
      `);

      const loginList = el.querySelectorAll('#login_button');
      expect(loginList!.length).to.equal(1);
    });

    it('Renders a profile image when authenticated', async () => {
      authMock?.isAuthenticated.returns(true);

      const el = await fixture(html`
        <unpub-nav></unpub-nav>
      `);

      const profileImage = el.querySelector('#profileImage');
      expect(profileImage).to.be.ok;
    });

    it('Renders a placeholder for no avatar image', async () => {
      authMock?.isAuthenticated.returns(true);

      const el = await fixture(html`
        <unpub-nav></unpub-nav>
      `);

      const profileImage = el.querySelector('#profileImage');
      expect(profileImage?.getAttribute('src')).to.equal('/images/avatar_placeholder.png');
    });

    it('Renders a userInfo avatar url', async () => {
      authMock?.isAuthenticated.returns(true);
      (authMock as any)._userInfo = {
        avatarUrl: 'fake://url'
      };

      const el = await fixture(html`
        <unpub-nav></unpub-nav>
      `);

      const profileImage = el.querySelector('#profileImage');
      expect(profileImage?.getAttribute('src')).to.equal('fake://url');
    });

    it('Renders a profile menuitem in the dropdown', async () => {
      authMock?.isAuthenticated.returns(true);

      const el = await fixture(html`
        <unpub-nav></unpub-nav>
      `);
      const dropDown = el.querySelector('sl-dropdown');
      const profileLink = dropDown?.querySelector('[href="/me"]');

      expect(profileLink).to.be.ok;
    });

    it('Clicking logout calls logout', async () => {
      authMock?.isAuthenticated.returns(true);
      const goStub = sinon.stub(Router, 'go');

      const el = await fixture(html`
        <unpub-nav></unpub-nav>
      `);

      const dropDown = el.querySelector('sl-dropdown');
      const logoutButton = dropDown?.querySelector('#logoutButton') as SlButton;
      logoutButton.click();

      expect(authMock?.logout.calledOnce).to.be.true;
      // Why can't I stub go properly here?
      // expect(goStub.calledOnce).to.be.true;
    });
  });
});
