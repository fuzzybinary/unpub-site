import { SlInput } from '@shoelace-style/shoelace';
import { html, LitElement } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';

@customElement('unpub-forgot-password')
class ForgotPassword extends diConsumer(LitElement) {
  private _authService: AuthService | null = null;

  @state() private _isSubmitted = false;
  @state() private _isSubmitting = false;
  @state() private _errorText = '';

  @query('#emailField') private _emailField!: SlInput;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
  }

  public render() {
    return html`
      <h2>Forgot Password?</h2>
      <div>
        ${this._isSubmitted
          ? html`
              <div id="instructions">
                Check your email for instructions on how to reset your password.
              </div>
            `
          : html`
              <div>
                <div>Enter your email address and we will send you a password reset link.</div>
                <sl-form id="forgotForm" @sl-submit=${this.onSubmit}>
                  <sl-input
                    id="emailField"
                    type="email"
                    name="email"
                    label="Email"
                    ?disabled=${this._isSubmitting}
                    required
                    outlined
                  >
                  </sl-input>
                  <sl-button type="primary" submit>Submit</sl-button>
                </sl-form>
                <div id="error">${this._errorText}</div>
              </div>
            `}
      </div>
    `;
  }

  private async onSubmit(event: Event) {
    event.preventDefault();

    this._errorText = '';
    this._isSubmitting = true;
    const email = this._emailField.value;
    if (!!email) {
      this._isSubmitted = await this._authService!.forgotPassword(email);
      if (!this._isSubmitted) {
        this._errorText = 'There was an error sending a reset email. Please try again.';
      }
    }
    this._isSubmitting = false;
  }
}
