import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { SlButton, SlInput } from '@shoelace-style/shoelace';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import { SinonStubbedInstance } from 'sinon';
import sinon from 'sinon';

import './ResetPassword';

describe('ResetPassword', () => {
  describe('unsuccessful render', () => {
    afterEach(() => {
      sinon.reset();
    });

    it('does not render without a token and email.', async () => {
      const el = await fixture(
        html`
          <unpub-reset-password></unpub-reset-password>>
        `
      );

      const form = el.querySelector('form');
      expect(form).to.not.be.ok;
    });

    it('does not render if provided bad token', async () => {
      const token = 'notdecodable';

      const location = {
        search: `token=${token}`
      };

      const el = await fixture(
        html`
          <unpub-reset-password .location=${location}></unpub-reset-password>>
        `
      );

      const form = el.querySelector('form');
      expect(form).to.not.be.ok;
    });
  });

  describe('successful render', () => {
    let el: HTMLElement;
    let authMock: SinonStubbedInstance<AuthService> | null = null;

    const provideFake = (event: Event) => {
      const customEvent = event as CustomEvent<RequestDependencyEventDetail>;
      customEvent.detail.instance = authMock;
    };

    beforeEach(async () => {
      authMock = sinon.createStubInstance(AuthService);
      document.addEventListener(DependencyEventName, provideFake);

      const tokenJson = JSON.stringify({
        resetToken: 'fakeToken',
        email: 'email@fuzzybinary.com'
      });
      const token = Buffer.from(tokenJson).toString('base64');
      const location = {
        search: `token=${token}`
      };
      el = await fixture(
        html`
          <unpub-reset-password .location=${location}></unpub-reset-password>>
        `
      );
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideFake);

      sinon.reset();
    });

    it('renders a form', () => {
      const form = el.querySelector('sl-form');
      expect(form).to.be.ok;
    });

    it('renders a password reset box', () => {
      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      expect(passwordField).to.be.ok;
      expect(passwordField.type).to.equal('password');
      expect(passwordField.required).to.be.true;
    });

    it('has a submit button', () => {
      const form = el.querySelector('sl-form');
      const submitButton = form?.querySelector('sl-button[submit]');

      expect(submitButton).to.be.ok;
    });

    it('submitting disables all fields', async () => {
      let resolveResetPromise: ((value: boolean) => void) | null = null;
      authMock?.resetPassword.callsFake(() => {
        return new Promise(resolve => (resolveResetPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const submitButton = form?.querySelector('sl-button[submit]') as SlButton;

      passwordField.value = 'testpassword';

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      expect(passwordField.disabled).to.be.true;
      expect(submitButton.disabled).to.be.true;

      resolveResetPromise!(true);
    });

    it('submit failure re-enables all fields', async () => {
      let resolveResetPromise: ((value: boolean) => void) | null = null;
      authMock?.resetPassword.callsFake(() => {
        return new Promise(resolve => (resolveResetPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const submitButton = form?.querySelector('sl-button[submit]') as SlButton;

      passwordField.value = 'testpassword';

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveResetPromise!(false);
      await nextFrame();

      expect(passwordField.disabled).to.be.false;
      expect(submitButton.disabled).to.be.false;
    });

    it('submit failure shows error', async () => {
      let resolveResetPromise: ((value: boolean) => void) | null = null;
      authMock?.resetPassword.callsFake(() => {
        return new Promise(resolve => (resolveResetPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      passwordField.value = 'testpassword';

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveResetPromise!(false);
      await nextFrame();

      const errors = el.querySelector('#error') as HTMLElement;
      expect(errors?.innerText).to.not.be.empty;
    });

    it('re-submit removes error', async () => {
      let resolveResetPromise: ((value: boolean) => void) | null = null;
      authMock?.resetPassword.callsFake(() => {
        return new Promise(resolve => (resolveResetPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      passwordField.value = 'testpassword';

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveResetPromise!(false);
      await nextFrame();

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      const errors = el.querySelector('#error') as HTMLElement;
      expect(errors?.innerText).to.be.empty;
    });

    it('sends data to reset password', async () => {
      let resolveResetPromise: ((value: boolean) => void) | null = null;
      authMock?.resetPassword.callsFake(() => {
        return new Promise(resolve => (resolveResetPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      passwordField.value = 'testpassword';

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      expect(
        authMock?.resetPassword.calledOnceWith('fakeToken', 'email@fuzzybinary.com', 'testpassword')
      ).to.be.true;

      resolveResetPromise!(true);
    });

    it('shows instructions on successful reset', async () => {
      let resolveResetPromise: ((value: boolean) => void) | null = null;
      authMock?.resetPassword.callsFake(() => {
        return new Promise(resolve => (resolveResetPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      passwordField.value = 'testpassword';

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveResetPromise!(true);
      await nextFrame();

      const instructions = el.querySelector('#instructions');
      expect(instructions).to.be.ok;
    });
  });
});
