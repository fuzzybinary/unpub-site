import { Router } from '@vaadin/router';
import { html, LitElement } from 'lit';
import { customElement } from 'lit/decorators.js';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';

@customElement('unpub-login-page')
class LoginPage extends diConsumer(LitElement) {
  private _authService: AuthService | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
    if (this._authService?.isAuthenticated()) {
      // Wat?
      const location = (this as any).location as Router.Location;
      const query = location ? this.parseQuery(location.search) : null;
      if (query?.redirect) {
        Router.go(query.redirect);
      } else {
        Router.go('/');
      }
    } else {
      this._authService?.subscribeToAuthChanges(this.authHandler);
    }
  }

  public disconnectedCallback() {
    super.disconnectedCallback();

    this._authService?.unsubscribeToAuthChanges(this.authHandler);
  }

  public createRenderRoot() {
    return this;
  }

  public render() {
    return html`
      <div style="margin-left: auto; margin-right: auto">
        <unpub-login></unpub-login>
      </div>
    `;
  }

  private authHandler = (isAuthenticated: boolean) => {
    if (isAuthenticated) {
      const location = (this as any).location as Router.Location;
      const query = this.parseQuery(location.search);
      if (query.redirect) {
        Router.go(query.redirect);
      } else {
        Router.go('/');
      }
    }
  };

  private parseQuery(queryString: string) {
    const query: any = {};
    const items = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (const item of items) {
      const pair = item.split('=');
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
  }
}
