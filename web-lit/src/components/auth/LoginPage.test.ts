import { expect, fixture, nextFrame, html } from '@open-wc/testing';

import { Router } from '@vaadin/router';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import sinon from 'sinon';

import './LoginPage';

describe('LoginPage', () => {
  let authMock: AuthService;

  const provideFake = (event: Event) => {
    const customEvent = event as CustomEvent<RequestDependencyEventDetail>;
    customEvent.detail.instance = authMock;
  };

  beforeEach(async () => {
    authMock = new AuthService();
    document.addEventListener(DependencyEventName, provideFake);

    // Prevent all localstorage calls
    sinon.stub(localStorage, 'getItem');
    sinon.stub(localStorage, 'setItem');
  });

  afterEach(() => {
    document.removeEventListener(DependencyEventName, provideFake);
    sinon.restore();
  });

  it('renders the login control', async () => {
    sinon.stub(authMock, 'isAuthenticated').returns(false);

    const el = await fixture(
      html`
        <unpub-login-page></unpub-login-page>
      `
    );

    expect(el.querySelector('unpub-login')).to.be.ok;
  });

  it('redirects if authenticated', async () => {
    sinon.stub(authMock, 'isAuthenticated').returns(true);
    const goStub = sinon.stub(Router, 'go');

    const location = {
      search: 'redirect=/me'
    };
    const el = await fixture(
      html`
        <unpub-login-page .location=${location}></unpub-login-page>
      `
    );

    expect(goStub.calledOnceWith('/me')).to.be.true;
  });

  it('redirects if on auth changed successfully', async () => {
    sinon.stub(authMock, 'isAuthenticated').returns(false);
    const goStub = sinon.stub(Router, 'go');

    const location = {
      search: 'redirect=/profile'
    };
    const el = await fixture(
      html`
        <unpub-login-page .location=${location}></unpub-login-page>
      `
    );
    authMock._onAuthChanged('fake-key', null);
    await nextFrame();

    expect(goStub.calledOnceWith('/profile')).to.be.true;
  });
});
