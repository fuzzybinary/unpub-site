import { html, LitElement } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';

import './ForgotPassword';
import './Login';
import './Register';

enum LoginState {
  login,
  register,
  forgot
}

@customElement('unpub-auth-dialog')
class AuthDialog extends diConsumer(LitElement) {
  @state() private _loginState = LoginState.login;

  private _authService: AuthService | null = null;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
  }

  public createRenderRoot() {
    // All auth stuff needs to be rendered to the light dom so
    // password managers can see it and fill it in properly
    return this;
  }

  public render() {
    switch (this._loginState) {
      case LoginState.login:
        return html`
          <unpub-login></unpub-login>
          <div>
            <span>
              Forgot password?
              <sl-button id="forgotButton" type="text" @click=${this.onForgotClicked}>
                Reset Password
              </sl-button>
            </span>
          </div>
          <div>
            <span>
              New to Unpub?
              <sl-button id="registerButton" type="text" inverted @click=${this.onRegisterClicked}>
                Register
              </sl-button>
            </span>
          </div>
        `;
      case LoginState.register:
        return html`
          <unpub-register></unpub-register>
          <div>
            <span>
              Already registered?
              <sl-button id="loginButton" type="text" @click=${this.onLoginClicked}>
                Login
              </sl-button>
            </span>
          </div>
        `;
      case LoginState.forgot:
        return html`
          <unpub-forgot-password></unpub-forgot-password>
          <span>
            <sl-button id="loginButton" type="text" @click=${this.onLoginClicked}>
              Return to Login
            </sl-button>
          </span>
        `;
    }
  }

  private onRegisterClicked() {
    this._loginState = LoginState.register;
  }

  private onForgotClicked() {
    this._loginState = LoginState.forgot;
  }

  private onLoginClicked() {
    this._loginState = LoginState.login;
  }
}
