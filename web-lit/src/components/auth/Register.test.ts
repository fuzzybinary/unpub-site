import { fixture, nextFrame, html } from '@open-wc/testing';
import { SlButton, SlForm, SlInput } from '@shoelace-style/shoelace';
import { expect } from 'chai';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthResult, AuthService } from 'services/AuthService';
import sinon, { SinonStubbedInstance } from 'sinon';

import './Register';

describe('Register', () => {
  describe('rendering', () => {
    let el: Element;

    beforeEach(async () => {
      el = await fixture(html`
        <unpub-register>></unpub-register>
      `);
    });

    it('renders a form', () => {
      const form = el.querySelector('sl-form');
      expect(form).to.be.ok;
      expect(form?.id).to.equal('registerForm');
    });

    it('renders a field for Display Name', () => {
      const form = el.querySelector('sl-form');
      const nameField = form?.querySelector('[name="name"]');
      expect(nameField).to.be.ok;
      expect(nameField?.getAttribute('required')).to.not.be.null;
    });

    it('renders a field for email', () => {
      const form = el.querySelector('sl-form');
      const emailField = form?.querySelector('[name="email"]');
      expect(emailField).to.be.ok;
      expect(emailField?.getAttribute('required')).to.not.be.null;
    });

    it('renders a field for password', () => {
      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]');
      expect(passwordField).to.be.ok;
      expect(passwordField?.getAttribute('required')).to.not.be.null;
    });

    it('renders a field for confirm password', () => {
      const form = el.querySelector('sl-form');
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]');
      expect(confirmPasswordField).to.be.ok;
      expect(confirmPasswordField?.getAttribute('required')).to.not.be.null;
    });

    it('renders a submit button', () => {
      const form = el.querySelector('sl-form');
      const submit = form?.querySelector('sl-button[submit]');
      expect(submit).to.be.ok;
    });
  });

  describe('interactions', () => {
    let el: Element;
    let authMock: SinonStubbedInstance<AuthService> | null = null;

    const provideFake = (event: Event) => {
      const customEvent = event as CustomEvent<RequestDependencyEventDetail>;
      customEvent.detail.instance = authMock;
    };

    beforeEach(async () => {
      authMock = sinon.createStubInstance(AuthService);
      document.addEventListener(DependencyEventName, provideFake);

      el = await fixture(html`
        <unpub-register>></unpub-register>
      `);
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideFake);
    });

    it('warns if passwords do not match', async () => {
      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]') as SlInput;

      passwordField.value = 'password1';
      confirmPasswordField.value = 'password2';

      // Trigger input to update ui
      passwordField.dispatchEvent(new Event('input'));
      await nextFrame();

      expect(confirmPasswordField.querySelector('sl-icon[slot="suffix"]')).to.be.ok;
    });

    it('clears warning if passwords do match', async () => {
      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]') as SlInput;

      passwordField.value = 'password1';
      confirmPasswordField.value = 'password2';

      passwordField.dispatchEvent(new Event('input'));
      await nextFrame();

      confirmPasswordField.value = 'password1';
      passwordField.dispatchEvent(new Event('input'));
      await nextFrame();

      expect(confirmPasswordField.querySelector('sl-icon[slot="suffix"]')).to.not.be.ok;
    });

    it('clears warning if either password becomes empty', async () => {
      const form = el.querySelector('sl-form');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]') as SlInput;

      passwordField.value = 'password1';
      confirmPasswordField.value = 'password2';

      passwordField.dispatchEvent(new Event('input'));
      await nextFrame();

      confirmPasswordField.value = '';
      passwordField.dispatchEvent(new Event('input'));
      await nextFrame();

      expect(confirmPasswordField.querySelector('sl-icon[slot="suffox"]')).to.not.be.ok;
    });

    it('displays a warning if passwords dont match on submission', async () => {
      const form = el.querySelector('sl-form');
      const name = form?.querySelector('[name="name"]') as SlInput;
      const email = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]') as SlInput;

      name.value = 'name';
      email.value = 'test@test.com';
      passwordField.value = 'password1';
      confirmPasswordField.value = 'password2';

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      expect(confirmPasswordField.querySelector('sl-icon[slot="suffix"]')).to.be.ok;
    });

    function fillOutForm(form: SlForm) {
      const name = form?.querySelector('[name="name"]') as SlInput;
      const email = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]') as SlInput;

      name.value = 'name';
      email.value = 'test@test.com';
      passwordField.value = 'password2';
      confirmPasswordField.value = 'password2';
    }

    it('submits registration if everything looks good', async () => {
      const form = el.querySelector('sl-form');

      fillOutForm(form!);

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      expect(authMock?.register.calledOnceWith('name', 'test@test.com', 'password2')).to.be.true;
    });

    it('disables forms while loading', async () => {
      const form = el.querySelector('sl-form');

      let resolveRegisterPromise: ((value: AuthResult) => void) | null = null;
      authMock?.register.returns(new Promise(resolve => (resolveRegisterPromise = resolve)));

      fillOutForm(form!);

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      const name = form?.querySelector('[name="name"]') as SlInput;
      const email = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]') as SlInput;
      const submit = form?.querySelector('sl-button[submit]') as SlButton;

      expect(name.disabled, 'Name disabled').to.be.true;
      expect(email.disabled, 'Email disabled').to.be.true;
      expect(passwordField.disabled, 'Password disabled').to.be.true;
      expect(confirmPasswordField.disabled, 'Confirm password disabled').to.be.true;
      expect(submit.disabled, 'Submit disabled').to.be.true;

      resolveRegisterPromise!({
        success: true
      });
      await nextFrame();

      expect(name.disabled, 'Name disabled').to.be.false;
      expect(email.disabled, 'Email disabled').to.be.false;
      expect(passwordField.disabled, 'Password disabled').to.be.false;
      expect(confirmPasswordField.disabled, 'Confirm password disabled').to.be.false;
      expect(submit.disabled, 'Submit disabled').to.be.false;
    });

    it('displays errors on failure', async () => {
      const form = el.querySelector('sl-form');

      let resolveRegisterPromise: ((value: AuthResult) => void) | null = null;
      authMock?.register.returns(new Promise(resolve => (resolveRegisterPromise = resolve)));

      fillOutForm(form!);

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveRegisterPromise!({
        messages: ['error 1', 'error 2'],
        success: false
      });
      await nextFrame();

      const errorElement = el.querySelector('.registerForm__errors');
      expect(errorElement).to.be.ok;
      expect(errorElement?.childElementCount).to.equal(2);
      expect(errorElement?.children[0].textContent).to.equal('error 1');
      expect(errorElement?.children[1].textContent).to.equal('error 2');
    });

    it('clears form on success', async () => {
      const form = el.querySelector('sl-form');

      let resolveRegisterPromise: ((value: AuthResult) => void) | null = null;
      authMock?.register.callsFake(() => {
        return new Promise(resolve => (resolveRegisterPromise = resolve));
      });

      fillOutForm(form!);

      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveRegisterPromise!({
        success: true
      });
      await nextFrame();

      const name = form?.querySelector('[name="name"]') as SlInput;
      const email = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const confirmPasswordField = form?.querySelector('[name="confirmPassword"]') as SlInput;

      expect(name.value).to.be.empty;
      expect(email.value).to.be.empty;
      expect(passwordField.value).to.be.empty;
      expect(confirmPasswordField.value).to.be.empty;
    });
  });
});
