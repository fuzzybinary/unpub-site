import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { SlButton } from '@shoelace-style/shoelace';

import './AuthDialog';

describe('AuthDialog', () => {
  it('should display login by default', async () => {
    const el = await fixture(
      html`
        <unpub-auth-dialog></unpub-auth-dialog>
      `
    );

    const loginElement = el.querySelector('unpub-login');
    expect(loginElement).to.be.ok;

    const registerButton = el.querySelector('#registerButton') as SlButton;
    expect(registerButton).to.be.ok;
    expect(registerButton.innerText.toLowerCase()).to.equal('register');

    const forgotButton = el.querySelector('#forgotButton') as SlButton;
    expect(forgotButton).to.be.ok;
  });

  it('clicking register should show register', async () => {
    const el = await fixture(
      html`
        <unpub-auth-dialog></unpub-auth-dialog>
      `
    );

    const registerButton = el.querySelector('#registerButton') as SlButton;
    registerButton.click();
    await nextFrame();

    const registerElement = el.querySelector('unpub-register');
    expect(registerElement).to.be.ok;
    const loginButton = el.querySelector('#loginButton') as SlButton;
    expect(loginButton).to.be.ok;
    expect(loginButton.innerText.toLowerCase()).to.equal('login');
  });

  it('should show login clicking login after register', async () => {
    const el = await fixture(
      html`
        <unpub-auth-dialog></unpub-auth-dialog>
      `
    );

    const registerButton = el.querySelector('#registerButton') as SlButton;
    registerButton.click();
    await nextFrame();

    const loginButton = el.querySelector('#loginButton') as SlButton;
    loginButton.click();
    await nextFrame();

    const loginElement = el.querySelector('unpub-login');
    expect(loginElement).to.be.ok;
  });

  it('should show forgot password on forgot clicked', async () => {
    const el = await fixture(
      html`
        <unpub-auth-dialog></unpub-auth-dialog>
      `
    );

    const forgotButton = el.querySelector('#forgotButton') as SlButton;
    forgotButton.click();
    await nextFrame();

    const forgotElement = el.querySelector('unpub-forgot-password');
    expect(forgotElement).to.be.ok;
  });

  it('should return to login after showing forgot password', async () => {
    const el = await fixture(
      html`
        <unpub-auth-dialog></unpub-auth-dialog>
      `
    );

    const forgotButton = el.querySelector('#forgotButton') as SlButton;
    forgotButton.click();
    await nextFrame();

    const loginButton = el.querySelector('#loginButton') as SlButton;
    loginButton.click();
    await nextFrame();

    const loginElement = el.querySelector('unpub-login');
    expect(loginElement).to.be.ok;
  });
});
