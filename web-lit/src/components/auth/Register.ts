import { SlInput } from '@shoelace-style/shoelace';
import { html, LitElement } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';

@customElement('unpub-register')
class Register extends diConsumer(LitElement) {
  private _authService: AuthService | null = null;

  @state() private _confirmPasswordValid: boolean = true;

  @state() private _performingOperation: boolean = false;

  @state() private _errorMessages: string[] = [];

  @query('#nameField') private _nameField!: SlInput;
  @query('#emailField') private _emailField!: SlInput;
  @query('#passwordField') private _passwordField!: SlInput;
  @query('#confirmPasswordField') private _confirmPasswordField!: SlInput;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
  }

  public createRenderRoot() {
    // All auth stuff needs to be rendered to the light dom so
    // password managers can see it and fill it in properly
    return this;
  }

  public onPasswordInput(event: InputEvent) {
    this.checkPasswordsMatch();
  }

  public async onSubmit(event: Event) {
    // Always prevent default to avoid browser refresh
    event.preventDefault();

    this._errorMessages = [];

    this.checkPasswordsMatch();
    if (!this._confirmPasswordValid) {
      return;
    }

    this._performingOperation = true;

    const result = await this._authService?.register(
      this._nameField.value,
      this._emailField.value,
      this._passwordField.value
    );

    if (!result?.success) {
      this._errorMessages = result?.messages || [];
    } else {
      this._nameField.value = '';
      this._emailField.value = '';
      this._passwordField.value = '';
      this._confirmPasswordField.value = '';
    }

    this._performingOperation = false;
  }

  public checkPasswordsMatch() {
    const passwordValue = this._passwordField.value;
    const confirmPasswordValue = this._confirmPasswordField.value;

    if (passwordValue && confirmPasswordValue) {
      if (passwordValue !== confirmPasswordValue) {
        this._confirmPasswordValid = false;
      } else {
        this._confirmPasswordValid = true;
      }
    } else {
      this._confirmPasswordValid = true;
    }
  }

  public render() {
    return html`
      <h2>Register</h2>
      <div class="stack-l">
        <sl-form id="registerForm" @sl-submit=${this.onSubmit}>
          <div class="stack-l">
            <sl-input
              id="nameField"
              name="name"
              label="Display Name"
              required
              ?disabled=${this._performingOperation}
            >
            </sl-input>
            <sl-input
              id="emailField"
              name="email"
              label="Email"
              type="email"
              required
              ?disabled=${this._performingOperation}
            >
            </sl-input>
            <sl-input
              id="passwordField"
              name="password"
              label="Password"
              type="password"
              required
              @input=${this.onPasswordInput}
              ?disabled=${this._performingOperation}
            >
            </sl-input>
            <sl-input
              id="confirmPasswordField"
              name="confirmPassword"
              label="Confirm Password"
              type="password"
              required
              @input=${this.onPasswordInput}
              ?disabled=${this._performingOperation}
            >
              ${this._confirmPasswordValid
                ? ''
                : html`
                    <sl-icon name="exclamation-triangle-fill" slot="suffix"></sl-icon>
                  `}
            </sl-input>

            <sl-button name="register" type="primary" submit ?disabled=${this._performingOperation}>
              Submit
            </sl-button>
          </div>
        </sl-form>
        <div class="registerForm__errors">
          ${this._errorMessages.map(message => {
            return html`
              <span>${message}</span>
            `;
          })}
        </div>
      </div>
    `;
  }
}
