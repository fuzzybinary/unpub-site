import { SlInput } from '@shoelace-style/shoelace';
import { Router } from '@vaadin/router';
import { html, LitElement } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';

@customElement('unpub-reset-password')
class ResetPassword extends diConsumer(LitElement) {
  private _authService: AuthService | null = null;

  private _resetToken: string | null = null;
  private _emailAddress: string | null = null;

  @state() private _isSubmitting: boolean = false;
  @state() private _isSubmitted: boolean = false;

  @state() private _errorText: string = '';

  @query('#passwordField') private _passwordField!: SlInput;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);

    const location = (this as any).location as Router.Location;
    const queryString = location ? this.parseQuery(location.search) : null;
    try {
      if (queryString?.token) {
        const tokenObject = JSON.parse(atob(queryString.token));
        if (tokenObject && tokenObject.resetToken && tokenObject.email) {
          this._resetToken = tokenObject.resetToken;
          this._emailAddress = tokenObject.email;
        }
      }
    } catch {
      // ignore issues parsing token. Will show user an error
    }
  }

  public createRenderRoot() {
    // All auth stuff needs to be rendered to the light dom so
    // password managers can see it and fill it in properly
    return this;
  }

  public render() {
    return this._resetToken
      ? this.renderSuccess()
      : html`
          <div>Something's wrong... please try again</div>
        `;
  }

  private renderSuccess() {
    return this._isSubmitted
      ? html`
          <h1>Reset Password</h1>
          <div id="instructions">
            Your password has been successfully reset. Please try logging in again.
            <sl-button @click=${this._onReturn}>Return to Login</sl-button>
          </div>
        `
      : html`
          <h1>Reset Password</h1>
          <sl-form @sl-submit=${this._onSubmit}>
            <sl-input
              name="password"
              id="passwordField"
              type="password"
              label="New Password"
              ?disabled=${this._isSubmitting}
              required
              outlined
            >
            </sl-input>
            <sl-button type="primary" submit ?disabled=${this._isSubmitting}>Submit</sl-button>
            <div id="error">${this._errorText}</div>
          </sl-form>
        `;
  }

  private async _onSubmit(e: Event) {
    e.preventDefault();

    if (!!this._passwordField.value) {
      if (!this._isSubmitting) {
        this._errorText = '';
      }
      this._isSubmitting = true;
      this._isSubmitted = await this._authService!.resetPassword(
        this._resetToken!,
        this._emailAddress!,
        this._passwordField.value
      );
      if (!this._isSubmitted) {
        this._errorText = 'Failure resetting password, please try again.';
      }
      this._isSubmitting = false;
    }
  }

  private _onReturn() {
    Router.go('/login');
  }

  private parseQuery(queryString: string) {
    const queryParams: any = {};
    const items = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (const item of items) {
      const pair = item.split('=');
      queryParams[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return queryParams;
  }
}
