import { fixture, html, nextFrame } from '@open-wc/testing';
import { SlButton, SlInput } from '@shoelace-style/shoelace';
import { expect } from 'chai';

import sinon, { SinonStubbedInstance } from 'sinon';

import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthResult, AuthService } from 'services/AuthService';

import './Login';

describe('Login', () => {
  describe('rendering', () => {
    let el: HTMLElement;

    beforeEach(async () => {
      el = await fixture(html`
        <unpub-login></unpub-login>
      `);
    });

    it('should have a form', () => {
      const form = el.querySelector('#loginForm');
      expect(form).to.be.ok;
    });

    it('should render an email field', () => {
      const form = el.querySelector('#loginForm');
      const emailField = form?.querySelector('[name="email"]') as SlInput;

      expect(emailField).to.be.ok;
      expect(emailField.type).to.equal('email');
      expect(emailField.required).to.be.true;
    });

    it('should render a password field', () => {
      const form = el.querySelector('#loginForm');
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      expect(passwordField).to.be.ok;
      expect(passwordField.type).to.equal('password');
      expect(passwordField.required).to.be.true;
    });

    it('should render a submit button', () => {
      const form = el.querySelector('#loginForm');
      const submitButton = form?.querySelector('sl-button[submit]') as SlButton;

      expect(submitButton).to.be.ok;
    });
  });

  describe('interactions', () => {
    let el: HTMLElement;

    let authMock: SinonStubbedInstance<AuthService> | null = null;

    const provideFake = (event: Event) => {
      const customEvent = event as CustomEvent<RequestDependencyEventDetail>;
      customEvent.detail.instance = authMock;
    };

    beforeEach(async () => {
      authMock = sinon.createStubInstance(AuthService);
      document.addEventListener(DependencyEventName, provideFake);

      el = await fixture(html`
        <unpub-login>></unpub-login>
      `);
    });

    afterEach(() => {
      document.removeEventListener(DependencyEventName, provideFake);
    });

    it('disables fields on submit', async () => {
      let resolveLoginPromise: ((value: AuthResult) => void) | null = null;
      authMock?.login.callsFake(() => {
        return new Promise(resolve => (resolveLoginPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const emailField = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const submitButton = form?.querySelector('sl-button[submit]') as SlButton;

      emailField.value = 'test@test.com';
      passwordField.value = 'testing';
      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      expect(emailField.disabled).to.be.true;
      expect(passwordField.disabled).to.be.true;
      expect(submitButton.disabled).to.be.true;

      resolveLoginPromise!({
        success: true
      });
    });

    it('calls login with supplied values', async () => {
      let resolveLoginPromise: ((value: AuthResult) => void) | null = null;
      authMock?.login.callsFake(() => {
        return new Promise(resolve => (resolveLoginPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const emailField = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      emailField.value = 'test@test.com';
      passwordField.value = 'testing';
      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      expect(authMock?.login.calledOnceWith('test@test.com', 'testing')).to.be.true;

      resolveLoginPromise!({
        success: true
      });
    });

    it('re-enables fields on login completion', async () => {
      let resolveLoginPromise: ((value: AuthResult) => void) | null = null;
      authMock?.login.callsFake(() => {
        return new Promise(resolve => (resolveLoginPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const emailField = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;
      const submitButton = form?.querySelector('sl-button[submit]') as SlButton;

      emailField.value = 'test@test.com';
      passwordField.value = 'testing';
      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveLoginPromise!({
        success: true
      });
      await nextFrame();

      expect(emailField.disabled).to.be.false;
      expect(passwordField.disabled).to.be.false;
      expect(submitButton.disabled).to.be.false;
    });

    it('renders errors on failure', async () => {
      let resolveLoginPromise: ((value: AuthResult) => void) | null = null;
      authMock?.login.callsFake(() => {
        return new Promise(resolve => (resolveLoginPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const emailField = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      emailField.value = 'test@test.com';
      passwordField.value = 'testing';
      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveLoginPromise!({
        messages: ['error 1', 'error 2'],
        success: false
      });
      await nextFrame();

      const errorElement = el.querySelector('.loginForm__errors');
      expect(errorElement).to.be.ok;
      expect(errorElement?.childElementCount).to.equal(2);
      expect(errorElement?.children[0].textContent).to.equal('error 1');
      expect(errorElement?.children[1].textContent).to.equal('error 2');
    });

    it('clears form on success', async () => {
      let resolveLoginPromise: ((value: AuthResult) => void) | null = null;
      authMock?.login.callsFake(() => {
        return new Promise(resolve => (resolveLoginPromise = resolve));
      });

      const form = el.querySelector('sl-form');
      const emailField = form?.querySelector('[name="email"]') as SlInput;
      const passwordField = form?.querySelector('[name="password"]') as SlInput;

      emailField.value = 'test@test.com';
      passwordField.value = 'testing';
      form?.dispatchEvent(new Event('sl-submit'));
      await nextFrame();

      resolveLoginPromise!({
        success: true
      });
      await nextFrame();

      expect(emailField.value).to.be.empty;
      expect(passwordField.value).to.be.empty;
    });
  });
});
