import { expect, fixture, html, nextFrame } from '@open-wc/testing';
import { SlInput } from '@shoelace-style/shoelace';
import { DependencyEventName, RequestDependencyEventDetail } from 'providers';
import { AuthService } from 'services/AuthService';
import sinon from 'sinon';
import { SinonStubbedInstance } from 'sinon';

import './ForgotPassword';

describe('ForgotPassword', () => {
  let el: HTMLElement;
  let authMock: SinonStubbedInstance<AuthService> | null = null;

  const provideFake = (event: Event) => {
    const customEvent = event as CustomEvent<RequestDependencyEventDetail>;
    customEvent.detail.instance = authMock;
  };

  beforeEach(async () => {
    authMock = sinon.createStubInstance(AuthService);
    document.addEventListener(DependencyEventName, provideFake);

    el = await fixture(
      html`
        <unpub-forgot-password></unpub-forgot-password>
      `
    );
  });

  afterEach(() => {
    document.removeEventListener(DependencyEventName, provideFake);

    sinon.reset();
  });

  it('should render a form', () => {
    const form = el.shadowRoot?.querySelector('sl-form');
    expect(form).to.be.ok;
  });

  it('should have an email field', () => {
    const form = el.shadowRoot?.querySelector('sl-form');
    const emailField = form?.querySelector('[name="email"]') as SlInput;

    expect(emailField).to.be.ok;
    expect(emailField.type).to.equal('email');
    expect(emailField.required).to.be.true;
  });

  it('disables fields on submit', async () => {
    let resolvePromise: ((value: boolean) => void) | null = null;
    authMock?.forgotPassword.callsFake(() => {
      return new Promise(resolve => (resolvePromise = resolve));
    });

    const form = el.shadowRoot?.querySelector('sl-form');
    const emailField = form?.querySelector('[name="email"]') as SlInput;

    emailField.value = 'test@test.com';
    form?.dispatchEvent(new Event('sl-submit'));
    await nextFrame();

    expect(emailField.disabled).to.be.true;

    resolvePromise!(true);
  });

  it('enables fields when submit finished', async () => {
    let resolvePromise: ((value: boolean) => void) | null = null;
    authMock?.forgotPassword.callsFake(() => {
      return new Promise(resolve => (resolvePromise = resolve));
    });

    const form = el.shadowRoot?.querySelector('sl-form');
    const emailField = form?.querySelector('[name="email"]') as SlInput;

    emailField.value = 'test@test.com';
    form?.dispatchEvent(new Event('sl-submit'));
    await nextFrame();

    resolvePromise!(false);
    await nextFrame();

    expect(emailField.disabled).to.be.false;
  });

  it('switches to instructions on success', async () => {
    let resolvePromise: ((value: boolean) => void) | null = null;
    authMock?.forgotPassword.callsFake(() => {
      return new Promise(resolve => (resolvePromise = resolve));
    });

    const form = el.shadowRoot?.querySelector('sl-form');
    const emailField = form?.querySelector('[name="email"]') as SlInput;

    emailField.value = 'test@test.com';
    form?.dispatchEvent(new Event('sl-submit'));
    await nextFrame();

    resolvePromise!(true);
    await nextFrame();

    const instructions = el.shadowRoot?.getElementById('instructions');
    expect(instructions).to.be.ok;
  });

  it('shows an error on failure', async () => {
    let resolvePromise: ((value: boolean) => void) | null = null;
    authMock?.forgotPassword.callsFake(() => {
      return new Promise(resolve => (resolvePromise = resolve));
    });

    const form = el.shadowRoot?.querySelector('sl-form');
    const emailField = form?.querySelector('[name="email"]') as SlInput;

    emailField.value = 'test@test.com';
    form?.dispatchEvent(new Event('sl-submit'));
    await nextFrame();

    resolvePromise!(false);
    await nextFrame();

    const error = el.shadowRoot?.getElementById('error');
    expect(error?.innerText).to.not.be.empty;
  });
});
