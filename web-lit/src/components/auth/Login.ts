import { SlInput } from '@shoelace-style/shoelace';
import { html, LitElement } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { diConsumer } from 'providers';
import { AuthService } from 'services/AuthService';

@customElement('unpub-login')
class UnpubLogin extends diConsumer(LitElement) {
  private _authService: AuthService | null = null;

  @state() private _performingOperation: boolean = false;
  @state() private _errorMessages: string[] = [];

  @query('#emailField') private _emailField!: SlInput;
  @query('#passwordField') private _passwordField!: SlInput;

  public connectedCallback() {
    super.connectedCallback();

    this._authService = this.resolve(AuthService);
  }

  public createRenderRoot() {
    // All auth stuff needs to be rendered to the light dom so
    // password managers can see it and fill it in properly
    return this;
  }

  public render() {
    return html`
      <div class="stack-l">
        <sl-form id="loginForm" @sl-submit=${this.onSubmit}>
          <div class="stack-l">
            <sl-input
              id="emailField"
              name="email"
              label="Email"
              type="email"
              required
              ?disabled=${this._performingOperation}
            >
            </sl-input>
            <sl-input
              id="passwordField"
              name="password"
              label="Password"
              type="password"
              required
              ?disabled=${this._performingOperation}
            >
            </sl-input>

            <sl-button
              id="loginButton"
              name="login"
              type="primary"
              submit
              ?disabled=${this._performingOperation}
            >
              Login
            </sl-button>
          </div>
        </sl-form>
        <div class="loginForm__errors">
          ${this._errorMessages.map(message => {
            return html`
              <span>${message}</span>
            `;
          })}
        </div>
      </div>
    `;
  }

  private async onSubmit(event: Event) {
    event.preventDefault();

    this._errorMessages = [];
    this._performingOperation = true;

    const emailField = this._emailField;
    const passwordField = this._passwordField;

    const result = await this._authService?.login(emailField?.value, passwordField?.value);
    if (!result?.success) {
      this._errorMessages = result?.messages || ['An unknown error occured'];
    } else {
      emailField.value = '';
      passwordField.value = '';
    }

    this._performingOperation = false;
  }
}
