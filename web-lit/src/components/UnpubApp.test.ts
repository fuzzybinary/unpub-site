import { fixture, nextFrame, html } from '@open-wc/testing';
import { expect } from 'chai';
import { AppConfig } from 'models/Config';
import { UnpubService } from 'services/UnpubService';
import sinon from 'sinon';

import './UnpubApp';

describe('index', async () => {
  afterEach(() => {
    sinon.restore();
  });

  it('loads config values', async () => {
    const init = sinon.stub(UnpubService.prototype, 'initConfig');
    let resolveInitPromise!: (config: AppConfig) => void;
    init.returns(new Promise(resolve => (resolveInitPromise = resolve)));

    const el = await fixture(html`
      <unpub-app></unpub-app>
    `);
    expect(init.called).to.be.true;
  });

  it('renders an router outlet', async () => {
    const init = sinon.stub(UnpubService.prototype, 'initConfig');
    let resolveInitPromise!: (config: AppConfig) => void;
    init.returns(new Promise(resolve => (resolveInitPromise = resolve)));

    const el = await fixture(html`
      <unpub-app></unpub-app>
    `);
    resolveInitPromise({
      maxAvatarSizeBytes: 0,
      maxPictureSizeBytes: 0
    });
    await nextFrame();

    expect(el.querySelector('outlet')).to.be.ok;
  });

  it('renders a nav', async () => {
    const el = await fixture(html`
      <unpub-app></unpub-app>
    `);

    expect(el.querySelector('unpub-nav')).to.be.ok;
  });
});
