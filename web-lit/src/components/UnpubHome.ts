import { LitElement, html } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('unpub-home')
class UnpubHome extends LitElement {
  public connectedCallback() {
    super.connectedCallback();
  }

  public render() {
    return html`
      <h1>Home</h1>
    `;
  }
}
