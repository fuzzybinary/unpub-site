import { FeedbackItemSummary, GameSummary } from './Game';

export interface UserSummary {
  id: string;
  displayName: string;
  avatarUrl?: string | null;
}

export interface UserProfileDetail {
  id: string;
  displayName: string;
  email: string;
  avatarUrl?: string;
  games?: GameSummary[];
  feedback: FeedbackItemSummary[];
}
