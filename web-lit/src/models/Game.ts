import { html } from 'lit-html';
import { UserSummary } from './UserModels';

export interface GameSummary {
  id: string;
  name: string;
  owner?: UserSummary;
}

export interface GameDetails {
  id: string;
  name: string;
  avatar?: string;
  time: string;
  minAge: number;
  minPlayers: number;
  maxPlayers: number;
  mechanisms: string[];
  description: string;
}

export interface GameDetailsPublic extends GameDetails {
  owner: UserSummary;
}

export interface GameDetailsPrivate extends GameDetails {
  feedback: FeedbackItemSummary[];
}

export interface Rectangle {
  left: number;
  top: number;
  width: number;
  height: number;
}

export interface GameCreateInfo {
  name: string;
  avatar?: string;
  avatarSrcRect?: Rectangle;
  time: string;
  minAge: number;
  minPlayers: number;
  maxPlayers: number;
  mechanisms: string[];
  description: string;
}

export type DiscreetAnswer = 'Yes' | 'Maybe' | 'No';

export const discreetAnswerIcon = (answer: DiscreetAnswer) => {
  switch (answer) {
    case 'Yes':
      return html`
        <sl-icon library="material" name="check" style="color: var(--unpub-good-color)"></sl-icon>
      `;
    case 'Maybe':
      return html`
        <span>?</span>
      `;
    case 'No':
      return html`
        <sl-icon library="material" name="not_interested" style="color: var(--unpub-bad-color)">
        </sl-icon>
      `;
  }
};

export interface FeedbackItemSummary {
  id: string;
  gameId: string;
  gameName: string;
  created: string;
  gameLength: number;
  easeOfLearning: number;
  downTime: number;
  decisions: number;
  interactivity: number;
  originality: number;
  fun: number;
  playAgain: DiscreetAnswer;
  buy: DiscreetAnswer;
  comments: string;
}

interface FeedbackMeta {
  fromRegisteredUserId?: string;
  fromName: string;
  fromEmail: string;
}

export interface FeedbackDetails {
  id: string;
  game: GameSummary;
  created: string;
  time: number;
  players: number;
  firstPlayerScore: number;
  lastPlayerScore: number;
  gameLength: number;
  easeOfLearning: number;
  downTime: number;
  decisions: number;
  interactivity: number;
  originality: number;
  fun: number;
  predictable: DiscreetAnswer;
  predictableWhy: string;
  playAgain: DiscreetAnswer;
  buy: DiscreetAnswer;
  changeOneThing: string;
  favoritePart: string;
  comments: string;
  isAnonymous: boolean;
  feedbackMeta?: FeedbackMeta;
}

export interface GameFeedbackSubmission {
  time: number;
  players: number;
  firstPlayerScore: number;
  lastPlayerScore: number;
  gameLength: number;
  easeOfLearning: number;
  downTime: number;
  decisions: number;
  interactivity: number;
  originality: number;
  fun: number;
  predictable: DiscreetAnswer;
  predictableWhy: string;
  playAgain: DiscreetAnswer;
  buy: DiscreetAnswer;
  changeOneThing: string;
  favoritePart: string;
  comments: string;
  fromName?: string;
  fromEmail?: string;
  isAnonymous: boolean;
  shareEmailOptIn: boolean;
}

export interface FeedbackSubmissionResult {
  id: string;
  gameId: string;
  created: string;
}
