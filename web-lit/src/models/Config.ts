export interface AppConfig {
  maxAvatarSizeBytes: number;
  maxPictureSizeBytes: number;
}
