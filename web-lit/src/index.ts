import 'components/shared/ImageCropper';

import 'components/UnpubApp';
import 'components/UnpubHome';
import 'components/UnpubNav';

import 'components/auth/LoginPage';

import 'components/games/FeedbackPage';
import 'components/games/GameDetails';
import 'components/games/GamesList';
import 'components/me/CreateGamePage';
import 'components/me/FeedbackDetails';
import 'components/me/PrivateGameDetails';
import 'components/me/Profile';

import '@shoelace-style/shoelace';

import { registerIconLibrary } from '@shoelace-style/shoelace/dist/utilities/icon-library.js';

registerIconLibrary('material', {
  resolver: name => {
    const match = name.match(/^(.*?)(_(round|sharp))?$/);
    if (match != null) {
      return `https://cdn.jsdelivr.net/npm/@material-icons/svg@1.0.5/svg/${match[1]}/${match[3] ||
        'outline'}.svg`;
    }
    return '';
  },
  mutator: svg => svg.setAttribute('fill', 'currentColor')
});
