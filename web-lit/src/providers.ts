import { RouterLocation } from '@vaadin/router';
import { LitElement } from 'lit';

export const DependencyEventName = 'di-provide-dependency-event';

export interface RequestDependencyEventDetail {
  type: any;
  instance: any;
}

type Constructor<T> = new (...args: any[]) => T;

export function withLocation<Base extends Constructor<LitElement>>(base: Base) {
  return class extends base {
    public location: RouterLocation | null = null;
  };
}

export function diConsumer<Base extends Constructor<LitElement>>(base: Base) {
  return class extends base {
    protected resolve<T>(type: Constructor<T>): T | null {
      const event = new CustomEvent<RequestDependencyEventDetail>(DependencyEventName, {
        bubbles: true,
        cancelable: true,
        composed: true,
        detail: {
          instance: null,
          type
        }
      });
      this.dispatchEvent(event);
      return event.detail.instance;
    }
  };
}

export function diProvider<Base extends Constructor<LitElement>>(base: Base) {
  // tslint:disable-next-line: max-classes-per-file
  return class extends base {
    private _dependencyMap: Map<string, object> = new Map();

    public connectedCallback() {
      super.connectedCallback();

      this.addEventListener(DependencyEventName, this._handleProvide);
    }

    public disconnectedCallback() {
      super.disconnectedCallback();

      this.removeEventListener(DependencyEventName, this._handleProvide);
    }

    protected register<T extends object>(type: Constructor<T>, value: T) {
      const typeName = type.name;
      this._dependencyMap.set(typeName, value);
    }

    private _handleProvide(event: Event) {
      const customEvent = event as CustomEvent<RequestDependencyEventDetail>;
      // Get information about the requested dependency
      const detail: RequestDependencyEventDetail = customEvent.detail;
      const typeName = detail.type.name;
      if (this._dependencyMap.has(typeName)) {
        customEvent.detail.instance = this._dependencyMap.get(typeName);
        customEvent.preventDefault();
        customEvent.stopPropagation();
      }
    }
  };
}
