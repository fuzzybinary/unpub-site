#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "No deploy server supplied"
    exit 1
fi

ssh $1 <<ENDSSH
mkdir ~/web-lit
mkdir ~/web-lit/dist
mkdir ~/usercontent
mkdir ~/usercontent/profiles
mkdir ~/usercontent/avatars
mkdir ~/eleventy
mkdir ~/ghost
mkdir ~/ghost/content
ENDSSH
rsync -avh web-lit/dist/* $1:~/web-lit/dist/ --delete
rsync -avh --delete --exclude .env --exclude dist/ --exclude node_modules/ eleventy/ $1:~/eleventy/ 
rsync docker-compose.* $1:~/
ssh $1 <<ENDSSH
export CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE
docker-compose down
cd ~/eleventy
mkdir dist
mkdir dist/assets
yarn install
yarn build

cd ~
export CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE
docker-compose --file docker-compose.yaml --file docker-compose.prod.yaml up -d
ENDSSH